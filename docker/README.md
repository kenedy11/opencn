This folder contains a `Dockerfile` made to ease the compilation of the whole
OpenCN framework.


## Build the image

To build the Docker image, simple execute the following command (it has to be
done only once):

```bash
cd <OPENCN_HOME>
docker build -t opencn/build-env docker/
```

## Usage

Once the image has been build, simply use the scripts presents in `<OPENCN_HOME>/docker`.


## Advanced mode

The *advanced* mode lets a user to connect itself to the container through a ssh
connection. It can be used to debug OpenCN Docker image, for example.

For information on this mode, please refers to documentation of the project.

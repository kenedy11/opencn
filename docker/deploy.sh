#!/bin/bash

# -- This script deploys OpenCN components from a docker container. It re-uses
# -- 'agency/deploy.sh' script.

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
AGENCY=$SCRIPTPATH/../agency


if [ ! -f "$AGENCY/build.conf" ]; then
    echo "ERROR: build.conf file does NOT exist"
    exit 1
else
    echo "Export varialbes from '$AGENCY/build.conf' file "
    # auto-export all variables found in build.conf (POSIX compatible method)
    set -o allexport
    . $AGENCY/build.conf
    set +o allexport
fi

if [ "$PLATFORM" == "rpi4" -o "$PLATFORM" == "x86" ]; then
    echo "Specify the device name of MMC (ex: sdb or mmcblk0 or other...)"
    read DEVNAME
fi

docker run --rm -it -v $SCRIPTPATH/..:/home/reds/opencn  \
           --privileged -v /dev/$DEVNAME:/dev/$DEVNAME \
           opencn/build-env /home/reds/opencn/agency/deploy.sh $@

#!/bin/bash

# -- This script builds runs OpenCN emulation inside a docker container. It re-uses
# -- 'agency/st' script.


SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

docker run --rm -it --privileged -v $SCRIPTPATH/..:/home/reds/opencn opencn/build-env /home/reds/opencn/agency/st
#!/bin/bash

# Deploy usr apps into the agency partition (second partition)
echo Deploying usr apps into the agency partition...
cd ../filesystem

if [ "$PLATFORM" = "x86-qemu" -o "$PLATFORM" = "x86" ]; then
./mount.sh 1
else
./mount.sh 2
fi

sudo cp -r ../usr/build/deploy/* fs/root/
./umount.sh
cd -









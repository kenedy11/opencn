#pragma once

#include "camera.h"
#include "shader.h"

struct Vertex {
	float x, y, z;
	float r = 1, g = 1, b = 1;
};

class VertexBuffer
{
public:
	VertexBuffer();
	~VertexBuffer();
	void draw(GLenum mode, int first_index, int last_index);
	void set(std::vector<Vertex> vertices);
	void setColor(float r, float g, float b, int index_start, int count);
	int size() const;

private:
	GLuint VAO = 0, VBO = 0;
	std::vector<Vertex> vertices;
	bool dirty = false;
};

#include "rs274dev.h"
#include "rs274ngc_interp.hh"
#include "rs274ngc_return.hh"

#include "camera.h"
#include "shader.h"
#include "vertex_buffer.h"

#include "sinspace.h"

// const char test_file[] = "ngc_test/unit/001_exact_stop.ngc";
// const char test_file[] = "ngc_test/full/002_PieceDemoLinuxCNC_HV_V05.ngc";
//const char test_file[] = "ngc_test/full/001_anchor.ngc";
//const char test_file[] = "ngc_test/full/003_micro5_3axes_ebauche.ngc";
// const char test_file[] = "ngc/line.ngc";
//const char test_file[] = "ngc_test/unit/helix_pitch.ngc";
//const char test_file[] = "ngc_test/full/Ebauche_3axes_V01.nc";
//const char test_file[] = "ngc_test/full/004_100_RoueDentee_Face_1_V02.ngc";
const char test_file[] = "ngc_test/full/006_300_RoueDentee_Face_2_Finition_V02.ngc";

namespace
{
int win_w = 800, win_h = 600;
Interp interp;

std::vector<ocn::CurvStruct> curv_structs;
std::vector<int> curv_struct_vertex_index;
std::vector<Vertex> vertices;
double r0D[3];

Camera camera{{-11, 17, 12}, {0, 0, 1}, -50, -30};
double last_mouse_x = 0;
double last_mouse_y = 0;
bool usingCamera = false;
GLFWwindow *window{nullptr};
ocn::FeedoptContext ctx;
} // namespace

void add_curv(const ocn::CurvStruct *c) {
    curv_structs.emplace_back(*c);
    curv_struct_vertex_index.emplace_back(vertices.size());

    ocn::CurvStruct Spline;
    ctx.q_splines.get(c->sp_index, &Spline);
    const int N = 10;
    for(int i = 0; i < N; i++) {
        ocn::EvalPosition(c, &Spline, (float)i/(N - 1), r0D);
        vertices.push_back({(float)r0D[0], (float)r0D[1], (float)r0D[2]});
    }
}

//void push_curv_struct(const ocn::CurvStruct *c)
//{
    //    printf("push_curv_struct\n");
//    curv_structs.emplace_back(*c);
//    curv_struct_vertex_index.emplace_back(vertices.size());
//    double u = 0;
//    ocn::EvalPosition(c, c, u, r0D);

//    for (int i = 0; i < r0D->size[1]; i++) {
//        vertices.push_back({(float)r0D->data[0 + r0D->size[0] * i],
//                            (float)r0D->data[1 + r0D->size[0] * i],
//                            (float)r0D->data[2 + r0D->size[0] * i]});
//    }
    
    
//}

//void c_assert_(const char *msg)
//{
//    fprintf(stderr, "ASSERT: %s\n", msg);
//}

const char *curve_type_to_str(ocn::CurveType type)
{
    switch (type) {
    case ocn::CurveType_None:
        return "None";
    case ocn::CurveType_Line:
        return "Line";
    case ocn::CurveType_Helix:
        return "Helix";
    case ocn::CurveType_TransP5:
        return "TransP5";
    default:
        return "<UNKNOWN>";
    }
}

void on_mouse_move(GLFWwindow *window, double xpos, double ypos)
{
    if (usingCamera) {
        camera.ProcessMouseMovement(xpos - last_mouse_x, ypos - last_mouse_y, true);
    }
    
    last_mouse_x = xpos;
    last_mouse_y = ypos;
}

void on_mouse_enter(GLFWwindow *window, int enter)
{
    if (enter) {
        glfwGetCursorPos(window, &last_mouse_x, &last_mouse_y);
    }
}

void on_key_press(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE) {
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
}

void on_mouse_button(GLFWwindow *window, int button, int action, int mods)
{
    if (button == GLFW_MOUSE_BUTTON_RIGHT) {
        if (action == GLFW_PRESS) {
            usingCamera = true;
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        } else if (action == GLFW_RELEASE) {
            usingCamera = false;
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        }
    }
}

void process_camera(double deltaTime)
{
    if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
        deltaTime *= 5;
    }
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        camera.ProcessKeyboard(Camera_Movement::Forward, deltaTime);
    }
    
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        camera.ProcessKeyboard(Camera_Movement::Backward, deltaTime);
    }
    
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        camera.ProcessKeyboard(Camera_Movement::Left, deltaTime);
    }
    
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        camera.ProcessKeyboard(Camera_Movement::Right, deltaTime);
    }
    
    if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS) {
        camera.ProcessKeyboard(Camera_Movement::Up, deltaTime);
    }
    
    if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS) {
        camera.ProcessKeyboard(Camera_Movement::Down, deltaTime);
    }
}

void on_resize(GLFWwindow *window, int width, int height)
{
    glViewport(0, 0, width, height);
}

int main()
{
    ocn::DebugActive = false;
    ocn::FeedoptConfig cfg;
    FeedoptDefaultConfig(&cfg);
    int filename_size[2] = {1, (int)strlen(test_file)};
    ocn::ConfigSetSource(&cfg, test_file, filename_size);


    ocn::InitFeedoptPlan(cfg, &ctx);

    bool optimized = false;
    ocn::CurvStruct Curv;
    while (ctx.op != ocn::Fopt::Fopt_Finished) {
        ocn::FeedoptPlan(&ctx, &optimized, &Curv);

        if (optimized) {
            ctx.go_next = true;
            optimized = false;
        }
    }

    for(int i = 0; i < ctx.q_split.size(); i++) {
        ocn::CurvStruct tmp_curv;
        ctx.q_split.get(i + 1, &tmp_curv);
        add_curv(&tmp_curv);
    }    
    // Add final index
    curv_struct_vertex_index.emplace_back(vertices.size());
    
//    return 0;
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    window = glfwCreateWindow(win_w, win_h, "RS274 Debug", nullptr, nullptr);
    glfwMakeContextCurrent(window);
    
    glfwSetCursorEnterCallback(window, on_mouse_enter);
    glfwSetCursorPosCallback(window, on_mouse_move);
    glfwSetKeyCallback(window, on_key_press);
    glfwSetMouseButtonCallback(window, on_mouse_button);
    glfwSetFramebufferSizeCallback(window, on_resize);
    
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    
    if (!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress))) {
        printf("Failed to initialize GLAD\n");
        return -1;
    }
    
    ImGui::CreateContext();
    ImGuiStyle style;
    ImGui::StyleColorsLight();
    ImGui::PushStyleVar(ImGuiStyleVar_Alpha, 0.8);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0);
    
    ImGuiIO &io = ImGui::GetIO();
    
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 330 core");
    
    glfwMaximizeWindow(window);
    
    glfwSwapInterval(1);
    {
        Shader shader{"shaders/base.vert", "shaders/base.frag"};
        Shader line_shader{"shaders/line.vert", "shaders/line.frag"};
        
        VertexBuffer buffer;
        buffer.set(vertices);
        
        VertexBuffer hovered_buffer;
        
        glClearColor(0.1f, 0.1f, 0.1f, 1.0);
        
        double lastFrameTime = glfwGetTime();
        
        int hovered = -1;
        float limits[2] = {0, 0};
        int first_struct_index = 0, last_struct_index = curv_structs.size();
        
        while (!glfwWindowShouldClose(window)) {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glfwPollEvents();
            
            double currentFrameTime = glfwGetTime();
            double deltaTime = currentFrameTime - lastFrameTime;
            lastFrameTime = currentFrameTime;
            
            if (usingCamera) {
                process_camera(deltaTime);
            }
            glfwGetWindowSize(window, &win_w, &win_h);
            const glm::mat4 projection =
                    glm::perspective(glm::radians(45.0f),
                                     static_cast<float>(win_w) / static_cast<float>(win_h), 0.1f, 1000.0f);
            
            glEnable(GL_DEPTH_TEST);
            //		glDisable(GL_BLEND);
            line_shader.use();
            line_shader.setMat4("V", camera.GetViewMatrix());
            line_shader.setMat4("P", projection);
            line_shader.setMat4("M", glm::mat4(1));
            line_shader.setVec2("viewport", {win_w, win_h});
            glLineWidth(1.0);
            
            buffer.draw(GL_LINE_STRIP, curv_struct_vertex_index[first_struct_index],
                        curv_struct_vertex_index[last_struct_index]);
            
            if (hovered != -1) {
                hovered_buffer.set({begin(vertices) + 5 * hovered, begin(vertices) + 5 * hovered + 5});
                hovered_buffer.setColor(1, 0, 0, 0, 5);
            }
            
            glDisable(GL_DEPTH_TEST);
            //		glEnable(GL_BLEND);
            //		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            line_shader.use();
            glLineWidth(5.0);
            line_shader.setMat4("V", camera.GetViewMatrix());
            line_shader.setMat4("P", projection);
            line_shader.setMat4("M", glm::mat4(1));
            line_shader.setVec2("viewport", {win_w, win_h});
            
            hovered_buffer.draw(GL_LINE_STRIP, 0, hovered_buffer.size());
            
            ImGui_ImplOpenGL3_NewFrame();
            ImGui_ImplGlfw_NewFrame();
            ImGui::NewFrame();
            
            //		ImGui::ShowDemoWindow();
            
            ImGui::Begin("Raw GCode");
            
            auto draw = ImGui::GetWindowDrawList();
            
            ImGui::BeginChild("CurvStructs", ImVec2(0, -1));
            
            //		ImGuiListClipper clipper(curv_structs.size(), ImGui::GetTextLineHeightWithSpacing()
            //* 2);
            ImGuiListClipper clipper(last_struct_index - first_struct_index,
                                     ImGui::GetTextLineHeightWithSpacing() * 2);
            
            ImGui::Separator();
            
            for (int i = clipper.DisplayStart; i < clipper.DisplayEnd; i++) {
                const ocn::CurvStruct &c = curv_structs[i + first_struct_index];
                
                ImGui::PushID(i);
                
                ImGui::BeginGroup();
                {
                    ImVec2 frame_start = ImGui::GetCursorPos();
                    ImGui::TextColored(ImColor(255, 128, 128), "%s", curve_type_to_str(c.Type));
                    switch (c.Type) {
                    case ocn::CurveType_Line:
                        double P0[3], P1[3];
                        ocn::EvalPosition(&c, &c, 0, P0);
                        ocn::EvalPosition(&c, &c, 1, P1);
                        ImGui::Text("(%.3f, %.3f, %.3f) -> (%.3f, %.3f, %.3f)", P0[0], P0[1], P0[2],
                                P1[0], P1[1], P1[2]);
                        break;
                    case ocn::CurveType_Helix:
                        ImGui::Text("TODO INFO                               ");
                        break;
                    case ocn::CurveType_TransP5:
                        ImGui::Text("TODO INFO                               ");
                        break;
                    case ocn::CurveType_None:
                        ImGui::Text("No info");
                        break;
                    }
                    ImVec2 frame_end = ImGui::GetCursorPos();
                    frame_end.x = 100;
//                    draw->AddRect(frame_start, frame_end, ImColor(255, 255, 255));
                }
                ImGui::EndGroup();
                ImGui::PopID();

                if (ImGui::IsItemHovered()) {
                    hovered = i + first_struct_index;
                }
                
                ImGui::Separator();
            }
            clipper.End();
            
            ImGui::EndChild();
            //		ImGui::Text("Hovered = %3d", hovered);
            
            ImGui::Begin("Camera");
            ImGui::Text("X = %5.2f, Y = %5.2f, Z = %5.2f", camera.Position.x, camera.Position.y,
                        camera.Position.z);
            ImGui::Text("Pitch = %5.2f, Yaw = %5.2f", camera.Pitch, camera.Yaw);
            ImGui::Text("Hovered: %6d", hovered);
            ImGui::SliderFloat2("Limits", limits, 0, 50);
            
            ImGui::DragIntRange2("Segment range", &first_struct_index, &last_struct_index,
                                 vertices.size() / 4000.0f, 0, curv_structs.size());
            ImGui::End();
            
            ImGui::End();
            
            ImGui::Render();
            ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
            glfwSwapBuffers(window);
        }
    }
    
    glfwDestroyWindow(window);
    glfwTerminate();
}

#version 330 core
out vec4 FragColor;

in vec2 lineCenter;
in vec4 vertexColor; // the input variable from the vertex shader (same name and same type)  

uniform vec2 viewport;

void main()
{
    vec4 col = vertexColor;
//    float d = length(lineCenter - gl_FragCoord.xy);
//    float w = 1.0; // line width
//    float blendFactor = 1.5f;
//    if (d > w) {
//            col.a = 0;
//    }
//    else {
//        col.a *= pow(float((w-d)/w), blendFactor);
//    }
////    vec4 col;
////    col.r = lineCenter.x;
////    col.g = lineCenter.y;
////    col.b = 1;
////    col.a = 1;
    FragColor = col;
} 

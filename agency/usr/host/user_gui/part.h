#ifndef PART_H
#define PART_H

#include <GL/glu.h>
#include <QObject>
#include <QString>
#include <QVector3D>
#include <QtCore>


#include "sinspace.h"


class Part : public QObject
{

    Q_OBJECT
signals:
    void pointsUpdated();

public:
    Part();
    virtual ~Part();

    bool loadStreamingFile(const QString &path);

    /*!
     * To be called in the OpenGL loop ONLY
     */
    void draw();
    void loadGCodeFile(const QString& filename);
    void loadCurvStruct(const std::vector<ocn::CurvStruct>& curvStruct);

public slots:
    void offsetXChange(double value);
    void offsetYChange(double value);
    void offsetZChange(double value);
    void offsetThetaZChange(double value);

private:
    ocn::FeedoptContext m_ctx;
    std::vector<ocn::CurvStruct> _gcodeStructs;
    std::vector<QVector3D> _segmentPoints;
    std::vector<int> _segmentSourceId;

    float offsetX = 0.0, offsetY = 0.0, offsetZ = 0.0, offsetThetaZ = 0.0;
    float _xmin = 0, _xmax = 0, _ymin = 0, _ymax = 0, _zmin = 0, _zmax = 0;

    GLuint display_list_index = 0;

    static const int SubDivisionCount;
};

#endif /* PART_H */

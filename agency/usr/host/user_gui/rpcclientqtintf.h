/*
 * Copyright (C) 2019 Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

/*
 * RpcClientToQT - this class provides an interface between the rpc client and
 * QT signals & slots
 *
 */

#ifndef RPCCLIENTQTINTF_H
#define RPCCLIENTQTINTF_H

#include <QObject>

#include "rpcclient.h"

class RpcClientQTIntf : public QObject
{
    Q_OBJECT
public:
    RpcClientQTIntf(RpcClient *client);
    ~RpcClientQTIntf();

public slots:
    void startHomingSlot();
    void stopHomingSlot();

	void speedSpindleSlot(double speed);
	void activeSpindleSlot(bool mode);

	void jogXSlot(bool mode);
	void jogYSlot(bool mode);
	void jogZSlot(bool mode);

    void relJogSlot(double value);
    void plusJogSlot();
    void minusJogSlot();

    void absJogSlot(double value);
    void goJogSlot();
    void speedJogSlot(double value);
    void stopJogSlot();

    void gcodeStartSlot();

    void faultResetSlot();

    void feedoptResetSlot();

    void streamGcodeStopSlot();

private:
    RpcClient *_client;
};


#endif /* RPCCLIENTQTINTF_H */

#ifndef IPDIALOG_H
#define IPDIALOG_H

#include <QDialog>

namespace Ui {
class IPDialog;
}

class IPDialog : public QDialog
{
    Q_OBJECT

public:
    explicit IPDialog(QWidget *parent = nullptr);
    ~IPDialog();

	QString getIP();
	uint getPort();
	void setIP(QString ip);
	void setPort(uint port);


private:
    Ui::IPDialog *ui;
};

#endif // IPDIALOG_H

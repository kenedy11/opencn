#include "mainwindow.h"
#include <QApplication>
#include <QCommandLineParser>
#include <QDebug>
#include <QSettings>

#include <arpa/inet.h>
#include <capnp/ez-rpc.h>

#include "ipdialog.h"
#include "rpcclient.h"


#define DEFAULT_IP_ADDR     "192.168.53.15"
#define DEFAULT_IP_PORT     7001

// ================= DUMMY IMPLEMENTATION =================
int feedopt_read_gcode(int)
{
    return 1;
}

// ================= DUMMY IMPLEMENTATION =================


static bool isValidIpAddress(QString ipAddress)
{
    struct sockaddr_in sa;
    int result = inet_pton(AF_INET, ipAddress.toStdString().c_str(), &(sa.sin_addr));
    return result != 0;
}


static void showIPGui(QString &ip, uint &port)
{
    IPDialog d;

    d.setIP(ip);
    d.setPort(port);

    int result = d.exec();

    if (result != QDialog::Accepted)
        qFatal("[ERROR] User pressed 'cancel' - application stopped");

    ip = d.getIP();
    if (!isValidIpAddress(ip))
        qFatal("[ERROR] IP address is not valid");

    port = d.getPort();
}


static void getParameters(QApplication &a, QString &ip, uint &port)
{
    QCommandLineParser parser;
    parser.setApplicationDescription("OpenCN GUI");
    parser.addHelpOption();

    QCommandLineOption ipOption(QStringList() << "i" << "ip",
            QCoreApplication::translate("main", "target's IP address"),
            QCoreApplication::translate("main", "ip"));
    parser.addOption(ipOption);

    QCommandLineOption portOption(QStringList() << "p" << "port",
            QCoreApplication::translate("main", "target's IP port"),
            QCoreApplication::translate("main", "port"));
    parser.addOption(portOption);

    parser.process(a);

    if (parser.isSet(ipOption)) {

        // Get the IP value
        if (isValidIpAddress(parser.value(ipOption)))
            ip = parser.value(ipOption);
        else
            qFatal("[ERROR] IP address is not valid");

        // Get the port's value if set
        bool ok;
        if (parser.isSet(portOption)) {
            port = parser.value(portOption).toUInt(&ok, 10);
            if (!ok)
                qFatal("[ERROR] 'port' has to be a number ");
        }

    } else {
        // No IP set at cmd line - show GUI to set it
        showIPGui(ip, port);
    }
}


int main(int argc, char *argv[])
{
    qDebug() << "Using qt version: " << QT_VERSION_STR;

    //TODO: comment before deploy
    //QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication a(argc, argv);

    // Get application settings
    QSettings settings("config.conf", QSettings::NativeFormat);
    settings.beginGroup("Comm_params");
    QString ip = settings.value("ip", DEFAULT_IP_ADDR).toString();
    uint port   = settings.value("port", DEFAULT_IP_PORT).toUInt();

    getParameters(a, ip, port);

    // Save parameters
    settings.setValue("ip", ip);
    settings.setValue("port", port);

    capnp::EzRpcClient client(ip.toStdString(), port);
    RpcClient rpcClient(&client);
    MainWindow w(&rpcClient);
    w.show();

    int ret = a.exec();
    return ret;
}

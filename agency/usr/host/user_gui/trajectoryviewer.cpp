#include "trajectoryviewer.h"

#include <QMatrix4x4>

#include <GL/glu.h>

const float TrajectoryViewer::RotationSpeedFactor = 0.005f;
const float TrajectoryViewer::MouseTranslationSpeedFactor = 0.0001f;

TrajectoryViewer::TrajectoryViewer(QWidget *parent, Qt::WindowFlags f)
    : QOpenGLWidget(parent, f), _cameraAngleMoving(false)
{

    QSurfaceFormat newFormat(format());

    newFormat.setVersion(3,0);
    newFormat.setOption(QSurfaceFormat::DeprecatedFunctions, true);
    setFormat(newFormat);

    makeCurrent();

    connect(this, &TrajectoryViewer::mousePressEvent, this, &TrajectoryViewer::mousePressed);
    connect(this, &TrajectoryViewer::mouseReleaseEvent, this, &TrajectoryViewer::mouseReleased);
    connect(this, &TrajectoryViewer::mouseMoveEvent, this, &TrajectoryViewer::mouseMove);
    connect(this, &TrajectoryViewer::wheelEvent, this, &TrajectoryViewer::wheel);


    connect(&_part, &Part::pointsUpdated, this, static_cast<void (QWidget::*)()>(&QWidget::update));
    connect(&_toolPath, &ToolPath::isUpdated, this, static_cast<void (QWidget::*)()>(&QWidget::update));

    _camera.setPosition(QVector3D(0.05f, 0.03f, 0.05f));
    _camera.setOrbitTargetPoint(QVector3D(0.0f, 0.0f, 0.0f));

}

TrajectoryViewer::~TrajectoryViewer()
{
    _toolPath.stop();
}

void TrajectoryViewer::setRpcClient(RpcClient *client)
{
    _toolPath.setRpcClient(client);
    _toolPath.start();
}

Part* TrajectoryViewer::getPart()
{
    return &_part;
}

ToolPath *TrajectoryViewer::getPath()
{
    return &_toolPath;
}

void TrajectoryViewer::initializeGL()
{
    initializeOpenGLFunctions();

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);


    GLfloat specular[] = {0.2f, 0.2f, 0.2f , 1.f};
    GLfloat diffuse[] = {0.6f, 0.6f, 0.6f , 1.f};
    GLfloat ambient[] = { 0.6f, 0.6f, 0.6f };

    glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);

    glLightfv(GL_LIGHT1, GL_SPECULAR, specular);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuse);
    glLightfv(GL_LIGHT1, GL_AMBIENT, ambient);

    GLfloat position1[] = { 0.0f, -2.0f, 0.0f, 1.0f };
    GLfloat position2[] = { 0.0f, 2.0f, 0.0f, 1.0f };


    glLightfv(GL_LIGHT0, GL_POSITION, position1);
    glLightfv(GL_LIGHT1, GL_POSITION, position2);

    GLfloat lightParam[] = {0.6f, 0.6f, 0.6f, 1.0f};
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lightParam);

    glShadeModel(GL_SMOOTH);
    glEnable(GL_SMOOTH_LINE_WIDTH_RANGE);
}

void TrajectoryViewer::paintGL()
{
//    static int n = 0;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    _tool.setPosition(_toolPath.getLastPosition());

    _camera.look();

    _tool.draw();
    _axes.draw();
    _part.draw();
    _toolPath.draw();

//    qDebug() << n++ << "paintGL";
}

void TrajectoryViewer::resizeGL(int w, int h)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(70.0, static_cast<GLdouble>(w) / static_cast<GLdouble>(h), 0.001, 5.0);
}

void TrajectoryViewer::mousePressed(QMouseEvent* event)
{
    if (event->button() == Qt::MouseButton::LeftButton) {
        _cameraPan = true;
        _cameraAngleMoving = false;
    }
    else if (event->button() == Qt::MouseButton::RightButton) {
        _cameraAngleMoving = true;
        _cameraPan = false;
    }

    _lastX = event->x();
    _lastY = event->y();
}

void TrajectoryViewer::mouseReleased(QMouseEvent* event)
{
    Q_UNUSED(event)

    _cameraAngleMoving = false;
    _cameraPan = false;
}

void TrajectoryViewer::mouseMove(QMouseEvent *event)
{
    if (_cameraAngleMoving) {
        int dX = event->x() - _lastX;
        int dY = event->y() - _lastY;

        _lastX = event->x();
        _lastY = event->y();

        _camera.incrementRotationX(RotationSpeedFactor * static_cast<float>(dX));
        _camera.incrementRotationY(RotationSpeedFactor * static_cast<float>(dY));

//        update();
    }
    else if(_cameraPan) {
        int dX = event->x() - _lastX;
        int dY = event->y() - _lastY;

        _lastX = event->x();
        _lastY = event->y();

        _camera.moveRight(MouseTranslationSpeedFactor * dX);
        _camera.moveUp(MouseTranslationSpeedFactor * dY);
    }
}

void TrajectoryViewer::wheel(QWheelEvent* event)
{
    _camera.incrementPositionZ(MouseTranslationSpeedFactor * static_cast<float>(event->delta()));
//    update();
}

#include "glcamera.h"

#include <GL/glu.h>

#include <math.h>

GlCamera::GlCamera()
    : _quaternion(QQuaternion(1.0, 0.0, 0.0, 0.0)), _position(0.0, 0.0, 0.0), _mode(ORBIT)
{
    update();
}

void GlCamera::look()
{
    update();
    gluLookAt(static_cast<GLdouble>(_position.x()), static_cast<GLdouble>(_position.y()), static_cast<GLdouble>(_position.z()),
              0.0, 0.0, 0.0,
              0.0, 1.0, 0.0);

    glTranslated(_orbitTarget.x(), _orbitTarget.y(), _orbitTarget.z());
    glRotated(-_orbit_pitch * 180.0 / M_PI, 0, 0, 1);
    glRotated(_orbit_yaw * 180.0 / M_PI, 0, 1, 0);

}

void GlCamera::setPosition(const QVector3D& pos)
{
    _position = pos;
    update();
}

QVector3D GlCamera::getPosition()
{
    return _position;
}

void GlCamera::setOrbitTargetPoint(const QVector3D& target)
{
    _orbitTarget = target;
    update();
}

void GlCamera::setCameraQuaternion(const QQuaternion& quat)
{
    _quaternion = quat;
    update();
}

QQuaternion GlCamera::getCameraQuaternion()
{
    return _quaternion;
}

void GlCamera::incrementRotationX(float inc)
{
    if (_mode == ORBIT) {
//        QQuaternion q = QQuaternion::fromEulerAngles(0.0, inc, 0.0); /* Y rotation */
//        _position = q.rotatedVector(_position - _orbitTarget);
        _orbit_yaw += inc;
    } else { /* TODO Freefly */

    }
    update();
}

void GlCamera::incrementRotationY(float inc)
{
    if (_mode == ORBIT) {
//        QQuaternion q = QQuaternion::fromEulerAngles(inc, 0.0, 0.0); /* Y rotation */
//        _position = q.rotatedVector(_position - _orbitTarget);
//        update();
        _orbit_pitch += inc;
        if (_orbit_pitch > M_PI_2 - 1e-3) {
            _orbit_pitch = M_PI_2 - 1e-3;
        }

        if (_orbit_pitch < -M_PI_2 + 1e-3) {
            _orbit_pitch = -M_PI_2 + 1e-3;
        }
    } else { /* TODO Freefly */

    }
    update();
}

void GlCamera::incrementPositionX(float inc)
{
/* TODO Freefly */
}

void GlCamera::incrementPositionY(float inc)
{
/* TODO Freefly */
}

void GlCamera::incrementPositionZ(float inc)
{
    if (_mode == ORBIT) {
        _orbit_distance += inc;
        if (_orbit_distance > 100.0e-3) {
            _orbit_distance = 100.0e-3;
        }
        if (_orbit_distance < 1.0e-3) {
            _orbit_distance = 1.0e-3;
        }
        //_position += _quaternion.rotatedVector(QVector3D(0.0, 0.0, inc));
        //update();
    } /* TODO Freefly */
}

void GlCamera::moveRight(float inc)
{
//    qDebug() << "moveRight: yaw = " << _orbit_yaw << ", inc = " << inc;
    _orbitTarget.setZ(_orbitTarget.z() - inc);
}

void GlCamera::moveUp(float inc)
{
    _orbitTarget.setY(_orbitTarget.y() - inc);
}

void GlCamera::update()
{
    if (_mode == ORBIT) {
        _target = _orbitTarget;
        //_quaternion = QQuaternion::fromDirection(_position - _orbitTarget, QVector3D(0.0, 1.0, 0.0));
        _position.setX(_orbit_distance);
        _position.setZ(0);
        _position.setY(0);
    } else { /* TODO freefly */
        QVector3D viewDir = _quaternion.rotatedVector(QVector3D(0.0, 0.0, 1.0));
        _target = _position + viewDir;
    }
}

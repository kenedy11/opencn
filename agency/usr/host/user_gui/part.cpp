#include "part.h"

#include <QDebug>
#include <QFile>
#include <cmath>

static const double xmin = -24e-3;
static const double xmax = 24e-3;
static const double ymin = -24e-3;
static const double ymax = 24e-3;

const int Part::SubDivisionCount = 10;
static const int STREAMING_SUBSAMPLE = 100;

Part::Part() {}

Part::~Part()
{

}

void Part::draw()
{

    if (!display_list_index) {
        display_list_index = glGenLists(1);
        glNewList(display_list_index, GL_COMPILE);
        glPushMatrix();
        glEnable(GL_COLOR_MATERIAL);

        glLineWidth(1.0f);

        glBegin(GL_LINES);

        int last_id = 0;
        GLfloat r = 1.0f, g = 1.0f, b = 1.0f;
        for (int i = 0; i < (int)_segmentPoints.size() - 1; i++) {
//            if (_segmentSourceId[i] != last_id) {
//                last_id = _segmentSourceId[i];
//                if (last_id % 2 == 1) {
//                    g = 0.0f;
//                    b = 1.0f;
//                } else {
//                    g = 1.0f;
//                    b = 0.0f;
//                }
//            }
            glColor3f(r,g,b);
            glVertex3f(_segmentPoints[i].x(), -_segmentPoints[i].y(), _segmentPoints[i].z());

            glColor3f(r,g,b);
            glVertex3f(_segmentPoints[i+1].x(), -_segmentPoints[i+1].y(), _segmentPoints[i+1].z());
        }

        glEnd();

        glDisable(GL_COLOR_MATERIAL);
        glPopMatrix();
        glEndList();
    } else {
        glPushMatrix();
        glTranslatef(offsetX, -offsetZ, offsetY);
        glRotated(offsetThetaZ, 0.0, 1.0, 0.0);
        glCallList(display_list_index);
        glPopMatrix();
    }

    glEnable(GL_COLOR_MATERIAL);
    glPushMatrix();
    glTranslatef(offsetX, 0, offsetY);
    if (_xmin + offsetX <= xmin ||
        _xmax + offsetX >= xmax ||
        _ymin + offsetY <= ymin ||
        _ymax + offsetY >= ymax) {
        glColor3f(1.0, 0.0, 0.0);
    } else {
        glColor3f(0.0, 0.1, 0.0);
    }

    glLineWidth(3.0f);
    glBegin(GL_LINE_LOOP);
        glVertex3f(_xmin, 0, _ymin);
        glVertex3f(_xmax, 0, _ymin);
        glVertex3f(_xmax, 0, _ymax);
        glVertex3f(_xmin, 0, _ymax);
    glEnd();
    glPopMatrix();
    glDisable(GL_COLOR_MATERIAL);
}

void Part::loadGCodeFile(const QString &filename)
{
    ocn::FeedoptConfig cfg{};
    ocn::FeedoptDefaultConfig(&cfg);
    int filename_size[2] = {1, (int)filename.size()};

    ocn::ConfigSetSource(&cfg, filename.toStdString().c_str(), filename_size);

    ocn::InitFeedoptPlan(cfg, &m_ctx);

    while(m_ctx.op != ocn::Fopt_Check) {
        bool optimized;
        ocn::CurvStruct opt_curv;
        ocn::FeedoptPlan(&m_ctx, &optimized, &opt_curv);
    }

    _gcodeStructs.clear();
    const size_t len = m_ctx.q_gcode.size();

    for (size_t i = 1; i <= len; i++) {
        ocn::CurvStruct tmp;
        m_ctx.q_gcode.get(i, &tmp);
        _gcodeStructs.push_back(std::move(tmp));
    }

    loadCurvStruct(_gcodeStructs);
}

void Part::offsetXChange(double value)
{
    offsetX = value / 1000.0;
    emit pointsUpdated();
}
void Part::offsetYChange(double value)
{
    offsetY = -value / 1000.0;
    emit pointsUpdated();
}
void Part::offsetZChange(double value)
{
    offsetZ = value / 1000.0;
    emit pointsUpdated();
}

void Part::offsetThetaZChange(double value)
{
    offsetThetaZ = value;
    emit pointsUpdated();
}

void Part::loadCurvStruct(const std::vector<ocn::CurvStruct>& curvStruct)
{
    if (display_list_index) {
        glDeleteLists(display_list_index, 1);
        display_list_index = 0;
    }

    _segmentPoints.clear();
    _segmentPoints.reserve(curvStruct.size() * SubDivisionCount);

    _segmentSourceId.clear();
    _segmentSourceId.reserve(curvStruct.size() * SubDivisionCount);


    _xmin = std::numeric_limits<float>::max();
    _xmax = std::numeric_limits<float>::min();

    _ymin = std::numeric_limits<float>::max();
    _ymax = std::numeric_limits<float>::min();

    int source_id = 0;

    coder::array<double, 2U> u_vec, r0D, r1D, r2D, r3D;
    ocn::linspace(0.0, 1.0, SubDivisionCount, u_vec);

    for(const auto& curv: curvStruct) {

        // printf("dataSize: %dx%d\n", dataSize[0], dataSize[1]);
        ocn::EvalCurvStruct(&m_ctx, &curv, u_vec, r0D, r1D, r2D, r3D);

        for (int j = 0; j < SubDivisionCount; j++) {
            // Warning: The OpenGL frame is right handed with Y upward whereas LinuxCNC frame is left handed with Z
            // upward

            auto x = static_cast<float>( r0D.at(0, j) / 1000.0);  //  x
            auto y = static_cast<float>(-r0D.at(2, j) / 1000.0);  //  z
            auto z = static_cast<float>(-r0D.at(1, j) / 1000.0);  // -y
            _segmentPoints.emplace_back(x, y, z);
            _xmin = std::min(x, _xmin);
            _xmax = std::max(x, _xmax);

            _ymin = std::min(z, _ymin);
            _ymax = std::max(z, _ymax);

            _segmentSourceId.push_back(source_id);
        }
        source_id++;
    }

    emit pointsUpdated();
}


bool Part::loadStreamingFile(const QString &path)
{
    if (display_list_index) {
        glDeleteLists(display_list_index, 1);
        display_list_index = 0;
    }
    QFile file(path);

    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Error reading " << path;
        return false;
    }

    QTextStream in(&file);

    // Count lines
    int lineCount = 0;
    while (!in.atEnd()) {
        QString line = in.readLine();
        lineCount++;
    }

    in.seek(0);

    _segmentPoints.clear();
    _segmentPoints.resize(lineCount/STREAMING_SUBSAMPLE);

    _xmin = std::numeric_limits<float>::max();
    _xmax = std::numeric_limits<float>::min();

    _ymin = std::numeric_limits<float>::max();
    _ymax = std::numeric_limits<float>::min();

    int i = 0;
    int k = 0;
    while (!in.atEnd() && i < 100000 && i < _segmentPoints.size()) {

        QString line = in.readLine();
        if (k % 100 != 0) {
            k++;
            continue;
        }
        QStringList list = line.split(" ", QString::SkipEmptyParts);

        if (list.size() != 3) {
            _segmentPoints.clear();
            return false;
        }

        _segmentPoints[i].setX(list[0].toFloat() / 1000.0f);
        _segmentPoints[i].setY(list[2].toFloat() / 1000.0f);
        _segmentPoints[i].setZ(-list[1].toFloat() / 1000.0f);

        _xmin = std::min(_segmentPoints[i].x(), _xmin);
        _xmax = std::max(_segmentPoints[i].x(), _xmax);

        _ymin = std::min(_segmentPoints[i].z(), _ymin);
        _ymax = std::max(_segmentPoints[i].z(), _ymax);

//        if (i > 0 && _segmentPoints[i].distanceToPoint(_segmentPoints[i-1]) > 1e-3) {
//            i++;
//        }
//        if (i == 0) i = 1;
        k++;
        i++;
    }

    emit pointsUpdated();

    return true;

}

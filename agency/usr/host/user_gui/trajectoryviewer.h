#ifndef TRAJECTORY_VIEWER_H
#define TRAJECTORY_VIEWER_H

#include <QMouseEvent>
#include <QOpenGLFunctions>
#include <QOpenGLWidget>
#include <QWheelEvent>

#include "axes.h"
#include "glcamera.h"
#include "part.h"
#include "tool.h"
#include "toolpath.h"

class TrajectoryViewer : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT
public:
    TrajectoryViewer(QWidget *parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());
    ~TrajectoryViewer() override;

    void setRpcClient(RpcClient *client);

    Part *getPart();
    ToolPath *getPath();

protected:
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int w, int h) override;

signals:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;

private slots:
    void mousePressed(QMouseEvent *event);
    void mouseReleased(QMouseEvent *event);
    void mouseMove(QMouseEvent *event);
    void wheel(QWheelEvent *event);

private:
    GlCamera _camera;
    bool _cameraAngleMoving, _cameraPan;
    int _lastX, _lastY;
    Tool _tool;
    Axes _axes;
    Part _part;
    ToolPath _toolPath;

    static const float RotationSpeedFactor;
    static const float MouseTranslationSpeedFactor;
};

#endif /* TRAJECTORY_VIEWER_H */

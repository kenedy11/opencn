
#include <sys/types.h>
#include <sys/stat.h>

#include <fstream>
#include <experimental/filesystem>

namespace fs = std::experimental::filesystem;

#include "rpcclient.h"

RpcClient::RpcClient(capnp::EzRpcClient* client)
    :_client(client), cap(_client->getMain<OpenCNServerInterface>())
{
}

Option<OpenCNServerInterface::CyclicData::Reader> RpcClient::getCyclicData()
{
    kj::WaitScope& waitScope = _client->getWaitScope();

    static auto promise = cap.getCyclicDataRequest().send();
    static Option<capnp::Response<OpenCNServerInterface::GetCyclicDataResults>> value{};

    if (promise.poll(waitScope)) {
        value = promise.wait(waitScope);
        promise = cap.getCyclicDataRequest().send();
    }

    if (value) {
        return value.value().getData();
    }

    return {};
}

void RpcClient::setFeedoptCommitCfg(bool val)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setFeedoptCommitCfgRequest();

    request.setCommit(val);
    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setLcctSetMachineModeHoming(bool mode)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setLcctSetMachineModeHomingRequest();

    request.setMode(mode);
    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setLcctSetMachineModeStream(bool mode)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setLcctSetMachineModeStreamRequest();

    request.setMode(mode);
    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setLcctSetMachineModeJog(bool mode)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setLcctSetMachineModeJogRequest();

    request.setMode(mode);

    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setLcctSetMachineModeInactive(bool mode)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setLcctSetMachineModeInactiveRequest();

    request.setMode(mode);

    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setLcctSetMachineModeGCode(bool mode)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setLcctSetMachineModeGCodeRequest();

    request.setMode(mode);

    auto promise = request.send();
    promise.wait(waitScope);
}

//bool RpcClient::getInModeHoming()
//{
//    kj::WaitScope& waitScope = _client->getWaitScope();


//    auto request = cap.getInModeHomingRequest();
//    auto promise = request.send();
//    auto response = promise.wait(waitScope);

//    return response.getMode();
//}

void RpcClient::setStartHoming(bool mode)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setStartHomingRequest();

    request.setMode(mode);

    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setStopHoming(bool mode)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setStopHomingRequest();

    request.setMode(mode);

    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setHomePositionX(double position)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setHomePositionXRequest();

    request.setPosition(position);

    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setHomePositionY(double position)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setHomePositionYRequest();

    request.setPosition(position);

    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setHomePositionZ(double position)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setHomePositionZRequest();

    request.setPosition(position);

    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setSpeedSpindle(double speed)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setSpeedSpindleRequest();

    request.setSpeed(speed);

    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setActiveSpindle(bool mode)
{
    kj::WaitScope& waitScope = _client->getWaitScope();

    auto request = cap.setActiveSpindleRequest();

    request.setMode(mode);

    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setSpindleThreshold(int percent)
{
    kj::WaitScope& waitScope = _client->getWaitScope();
    auto request = cap.setSpindleThresholdRequest();

    request.setPercent(percent);

    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setJogX(bool mode)
{
    kj::WaitScope& waitScope = _client->getWaitScope();
    auto request = cap.setJogXRequest();

    request.setMode(mode);

    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setJogY(bool mode)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setJogYRequest();

    request.setMode(mode);

    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setJogZ(bool mode)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setJogZRequest();

    request.setMode(mode);

    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setRelJog(double value)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setRelJogRequest();

    request.setValue(value);

    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setPlusJog(bool plus)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setPlusJogRequest();

    request.setPlus(plus);

    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setMinusJog(bool minus)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setMinusJogRequest();

    request.setMinus(minus);

    auto promise = request.send();
    promise.wait(waitScope);
}


void RpcClient::setAbsJog(double value)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setAbsJogRequest();

    request.setValue(value);

    auto promise = request.send();
    promise.wait(waitScope);

}
void RpcClient::setGoJog(bool mode)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setGoJogRequest();

    request.setMode(mode);

    auto promise = request.send();
    promise.wait(waitScope);
}
void RpcClient::setSpeedJog(double speed)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setSpeedJogRequest();

    request.setSpeed(speed);

    auto promise = request.send();
    promise.wait(waitScope);
}
void RpcClient::setStopJog(bool mode)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setStopJogRequest();

    request.setMode(mode);

    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setOffset(double x, double y, double z, double c)
{
    kj::WaitScope& waitScope = _client->getWaitScope();

    auto request = cap.setOffsetRequest();

    request.setX(x);
    request.setY(y);
    request.setZ(z);
    request.setC(c);

    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setStartStream(bool mode)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setStartStreamRequest();

    request.setMode(mode);

    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setStopStream(bool mode)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setStopStreamRequest();

    request.setMode(mode);

    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setPauseStream(bool mode)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setPauseStreamRequest();

    request.setMode(mode);

    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setLoadStream(bool mode)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setLoadStreamRequest();

    request.setMode(mode);

    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setGcodeStart(bool mode)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setGcodeStartRequest();

    request.setMode(mode);

    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setGcodePause(bool mode)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setGcodePauseRequest();

    request.setMode(mode);

    auto promise = request.send();
    promise.wait(waitScope);
}

void RpcClient::setFaultReset(bool reset)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setFaultResetRequest();

    request.setReset(reset);

    auto promise = request.send();
    promise.wait(waitScope);
}


void RpcClient::setFeedrateScale(double scale)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setFeedrateScaleRequest();

    request.setScale(scale);

    auto promise = request.send();
    // promise.wait(waitScope);
}

void RpcClient::setFeedoptReset(bool reset)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setFeedoptResetRequest();

    request.setReset(reset);

    auto promise = request.send();
    promise.wait(waitScope);
}

std::string RpcClient::readLog()
{
    kj::WaitScope& waitScope = _client->getWaitScope();

    static auto promise = cap.readLogRequest().send();

    if (promise.poll(waitScope)) {
        std::string value = promise.wait(waitScope).getMessage().cStr();
        promise = cap.readLogRequest().send();
        return value;
    } else {
        return "";
    }
}

void RpcClient::setFeedoptConfig(ocn::FeedoptConfig *cfg)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.setFeedoptConfigRequest();

    request.getConfig().setNHorz(cfg->NHorz);
    request.getConfig().setNDiscr(cfg->NDiscr);
    request.getConfig().setNBreak(cfg->NBreak);
    request.getConfig().setLSplit(cfg->LSplit);
    request.getConfig().setCutOff(cfg->CutOff);

    request.getConfig().setVmax(cfg->vmax);

    request.getConfig().setAmaxX(cfg->amax[0]);
    request.getConfig().setAmaxY(cfg->amax[1]);
    request.getConfig().setAmaxZ(cfg->amax[2]);

    request.getConfig().setJmaxX(cfg->jmax[0]);
    request.getConfig().setJmaxY(cfg->jmax[1]);
    request.getConfig().setJmaxZ(cfg->jmax[2]);
    request.getConfig().setSource(cfg->source);

    auto promise = request.send();
    promise.wait(waitScope);
}

ocn::FeedoptConfig RpcClient::getFeedoptConfig()
{
    ocn::FeedoptConfig cfg{};

    kj::WaitScope& waitScope = _client->getWaitScope();


    auto request = cap.getFeedoptConfigRequest();
    auto promise = request.send();
    auto response = promise.wait(waitScope);

    cfg.NHorz = response.getConfig().getNHorz() ;
    cfg.NDiscr = response.getConfig().getNDiscr();
    cfg.NBreak = response.getConfig().getNBreak();
    cfg.LSplit = response.getConfig().getLSplit();
    cfg.CutOff = response.getConfig().getCutOff();

    cfg.vmax = response.getConfig().getVmax();

    cfg.amax[0] = response.getConfig().getAmaxX();
    cfg.amax[1] = response.getConfig().getAmaxY();
    cfg.amax[2] = response.getConfig().getAmaxZ();

    cfg.jmax[0] = response.getConfig().getJmaxX();
    cfg.jmax[1] = response.getConfig().getJmaxY();
    cfg.jmax[2] = response.getConfig().getJmaxZ();

    return cfg;
}

bool RpcClient::toolpathStartChannel(int sampleRate)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.toolpathStartChannelRequest();

    request.setSampleRate(sampleRate);

    auto promise = request.send();
    auto response = promise.wait(waitScope);

    return response.getResult();
}

void RpcClient::toolpathStopChannel()
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.toolpathStopChannelRequest();

    auto promise = request.send();
    promise.wait(waitScope);
}

int RpcClient::toolpathReadSamples(std::vector<sampler_sample_t> &samples)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    static auto promise = cap.toolpathReadSamplesRequest().send();
    samples.clear();

    if (promise.poll(waitScope)) {
        auto response = promise.wait(waitScope);
        auto res_samples = response.getSamples();
        for(size_t i = 0; i < res_samples.size(); i++) {
            sampler_sample_t sample;
            sample.n_pins = res_samples[i].getValues().size();
            for(size_t k = 0; k < res_samples[i].getValues().size(); k++) {
                auto value = res_samples[i].getValues()[k].getValue();
                if (value.isB()) {
                    sample.pins[k].b = value.getB();
                    sample.pins[k].type = HAL_BIT;
                }
                else if (value.isU()) {
                    sample.pins[k].u = value.getU();
                    sample.pins[k].type = HAL_U32;
                }
                else if (value.isS()) {
                    sample.pins[k].s = value.getS();
                    sample.pins[k].type = HAL_S32;
                }
                else if (value.isF()) {
                    sample.pins[k].f = value.getF();
                    sample.pins[k].type = HAL_FLOAT;
                }
            }

            samples.push_back(sample);
        }


        promise = cap.toolpathReadSamplesRequest().send();
        return response.getSamples().size();
    }

    return 0;
}

bool RpcClient::pathExist(const std::string& path)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.pathExistRequest();

    request.setPath(path);

    auto promise = request.send();
    auto response = promise.wait(waitScope);

    return response.getResult();
}


int RpcClient::sendFileParam(const char *fileName, uint32_t size, file_ops_t file_op)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.sendFileParamRequest();

    request.setFileName(fileName);
    request.setFileOp(file_op);
    request.setSize(size);

    auto promise = request.send();
    auto response = promise.wait(waitScope);

    return response.getResult();
}

int RpcClient::sendFileData(uint8_t *data, size_t count)
{
    kj::WaitScope& waitScope = _client->getWaitScope();


    auto request = cap.sendFileDataRequest();

    request.setData(capnp::Data::Reader(data, count));

    auto promise = request.send();
    auto response = promise.wait(waitScope);

    return response.getResult();
}

int RpcClient::getFileData(uint8_t *file_chunk){

    kj::WaitScope& waitScope = _client->getWaitScope();



    auto promise = cap.getFileDataRequest().send();
    auto response = promise.wait(waitScope);

    auto len = response.getData().size();

    memcpy(file_chunk, response.getData().begin(), len);

    return len;
}


int RpcClient::createFolder(std::string folderPath)
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.createFolderRequest();

    request.setFolderPath(folderPath);

    auto promise = request.send();
    auto response = promise.wait(waitScope);

    return response.getResult();
}

void RpcClient::samplerNewFile()
{
    kj::WaitScope& waitScope = _client->getWaitScope();



    auto request = cap.samplerNewFileRequest();
    auto promise = request.send();
    promise.wait(waitScope);
}

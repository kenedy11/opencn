#include <iostream>
#include <cstdlib>

#include <capnp/ez-rpc.h>

#include <opencn_interface.capnp.h>


const char *readLog(capnp::EzRpcClient *client)
{
    kj::WaitScope& waitScope = client->getWaitScope();
    OpenCNServerInterface::Client cap = client->getMain<OpenCNServerInterface>();

    auto request = cap.readLogRequest();
    auto promise = request.send();
    auto response = promise.wait(waitScope);

    auto message = response.getMessage();

    return message.cStr();
}


int main(int argc, char *argv[])
{
    capnp::EzRpcClient client("127.0.0.1", 7001);

    while (true) {
//        kj::WaitScope& waitScope = client.getWaitScope();
//        OpenCNServerInterface::Client cap = client.getMain<OpenCNServerInterface>();
//
//        auto request = cap.getFeedoptSampleRequest();
//        auto promise = request.send();
//        auto response = promise.wait(waitScope);
//
//        auto recvFeedoptSample = response.getSample();
//
//        std::cout << recvFeedoptSample.getX() << ", "
//                  << recvFeedoptSample.getY() << ", "
//                  << recvFeedoptSample.getZ() << std::endl;
//

        std::cout << readLog(&client) << std::endl;

    }

    return EXIT_SUCCESS;
}

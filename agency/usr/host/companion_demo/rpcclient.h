#ifndef RPC_CLIENT_H
#define RPC_CLIENT_H

#include <string>
#include <capnp/ez-rpc.h>

#include <opencn/uapi/hal.h>

#include "opencn-interface/opencn_interface.capnp.h"

class RpcClient {

public:
	RpcClient(capnp::EzRpcClient* client);

	template <class T>
	void setPin(std::string name, T value)
	{
		kj::WaitScope& waitScope = _client->getWaitScope();

		auto request = cap.setPinRequest();

		request.setName(name);
		auto val = request.getValue().getValue();

		if (std::is_same<int32_t, T>::value)
			val.setS(value);
		else if (std::is_same<uint32_t, T>::value)
			val.setU(value);
		else if (std::is_same<bool, T>::value)
			val.setB(value);
		else if (std::is_same<double, T>::value)
			val.setF(value);

		auto promise = request.send();
		auto response = promise.wait(waitScope);
	}

	template <class T>
	T getPin(std::string name)
	{
		kj::WaitScope& waitScope = _client->getWaitScope();

		auto request = cap.getPinRequest();

		request.setName(name);

		auto promise = request.send();
		auto response = promise.wait(waitScope);

		auto value = response.getValue().getValue();

		if (std::is_same<int32_t, T>::value)
			return value.getS();
		else if (std::is_same<uint32_t, T>::value)
			return value.getU();
		else if (std::is_same<bool, T>::value)
			return value.getB();
		else if (std::is_same<double, T>::value)
			return value.getF();
		else
			// Todo Add error check
			return 0;
	}

private:
	capnp::EzRpcClient* _client;
	OpenCNPins::Client cap;
};


#endif /* RPC_CLIENT_H */

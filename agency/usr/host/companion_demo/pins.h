#ifndef PINS_H
#define PINS_H

#include "rpcclient.h"

template <class T>
class HalPin {
public:
    explicit HalPin(std::string pinName, RpcClient *client) noexcept : _pinName(pinName), _client(client) {
    }

    inline operator T() {
        T value;
        value = _client->getPin<T>(_pinName);
        return value;
    }

    inline void operator=(T value) {
        _client->setPin<T>(_pinName, value);
    }

protected:
    RpcClient  *_client;
    std::string _pinName;
};

using HalPinI32    = HalPin<int32_t>;
using HalPinU32    = HalPin<uint32_t>;
using HalPinBool   = HalPin<bool>;
using HalPinDouble = HalPin<double>;

#endif /* PINS_H */

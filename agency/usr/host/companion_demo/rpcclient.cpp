

#include "rpcclient.h"

RpcClient::RpcClient(capnp::EzRpcClient *client)
    :_client(client), cap(_client->getMain<OpenCNPins>())
{
}

#if 0

template <class T>
void RpcClient::setPin(std::string name, T value)
{
	kj::WaitScope& waitScope = _client->getWaitScope();

	auto request = cap.setPinRequest();

	request.setName(name);
	auto val = request.getValue().getValue();

	if (std::is_same<int32_t, T>::value)
		val.setS(value);
	else if (std::is_same<uint32_t, T>::value)
		val.setU(value);
	else if (std::is_same<bool, T>::value)
		val.setB(value);
	else if (std::is_same<double, T>::value)
		val.setF(value);

	auto promise = request.send();
	auto response = promise.wait(waitScope);
}

template <class T>
T RpcClient::getPin(std::string name)
{
	kj::WaitScope& waitScope = _client->getWaitScope();

	auto request = cap.getPinRequest();

	request.setName(name);

	auto promise = request.send();
	auto response = promise.wait(waitScope);

	auto value = response.getValue().getValue();

	if (std::is_same<int32_t, T>::value)
		return value.getS();
	else if (std::is_same<uint32_t, T>::value)
		return value.getU();
	else if (std::is_same<bool, T>::value)
		return value.getB();
	else if (std::is_same<double, T>::value)
		return value.getF();
	else
		// Todo Add error check
		return 0;
}

#endif

@0x8da26520438371a4;

interface OpenCNPins {

    struct PinValue {
        value :union {
            u @0: UInt32;
            s @1: Int32;
            f @2: Float64;
            b @3: Bool;
        }
    }

    setPin @0 (name: Text, value: PinValue) -> ();
    getPin @1 (name: Text) -> (value: PinValue);

# TODO
#  1. Add error code
#  2. PinAction --> multiple access in one call
#        Check how 'toolpathReadSamples' is made - good example

#    struct PinAction { # PinTransaction or PinTransfer ?) ?
#        cmd   #  RD or WR
#        type
#        value
#    }

#    pinActions @2 (actions: List(PinAction)) -> (values: list(Data));
}


interface OpenCNServerInterface  extends(OpenCNPins) {

    struct CyclicData {
        feedoptStepDt       @0: Float64;
        feedoptQueueSize    @1: Int32;
        feedoptQueueMax     @19: Int32;
        feedoptProgress     @2: Progress;
        homingFinished      @3: Bool;
        streamFinished      @4: Bool;
        streamRunning       @5: Bool;
        jogFinished         @6: Bool;
        gcodeFinished       @7: Bool;
        gcodeRunning        @8: Bool;
        currentPosition     @9: Position;
        spindleVelocity     @10: Float64;
        axisMode            @11: AxisMode;
        homed               @12: Bool;
        currentU            @13: Float64;
        feedoptUsActive     @14: Bool;
        feedoptRtActive     @15: Bool;
        feedoptReady        @16: Bool;
        streamerFIFO        @17: Int32;
        machineMode         @18: MachineMode;
        currentGCodeLine    @20: Int32;
    }

    struct FeedoptSample {
        x @0 :Float64;
        y @1 :Float64;
        z @2 :Float64;
    }

    struct Position {
        x @0 :Float64;
        y @1 :Float64;
        z @2 :Float64;
    }

    struct AxisMode {
        inactive    @0: AxisMask;
        fault       @1: AxisMask;
        homing      @2: AxisMask;
        csp         @3: AxisMask;
        csv         @4: AxisMask;
    }

    struct AxisMask {
        x @0 :Bool;
        y @1 :Bool;
        z @2 :Bool;
        spindle @3 :Bool;
    }

    struct MachineMode {
        homing      @0 :Bool;
        stream      @1 :Bool;
        jog         @2 :Bool;
        inactive    @3 :Bool;
        gcode       @4 :Bool;
    }

    struct Progress {
        compressingProgress @0 :Int32;
        compressingCount    @1 :Int32;
        smoothingProgress   @2 :Int32;
        smoothingCount      @3 :Int32;
        splittingProgress   @4 :Int32;
        splittingCount      @5 :Int32;
        optimisingProgress  @6 :Int32;
        optimisingCount     @7 :Int32;
        resamplingProgress  @8 :Int32;
        resamplingCount     @9 :Int32;
    }

    struct FeedOptCfg {
        nHorz      @0 : Int32;
        nDiscr     @1 : Int32;
        nBreak     @2 : Int32;
        lSplit     @3 : Float64;
        cutOff     @4 : Float64;
        debugPrint @5 : Bool;
        source @6 : Text;
        amaxX @7 : Float64;
        amaxY @8 : Float64;
        amaxZ @9 : Float64;
        jmaxX @10 : Float64;
        jmaxY @11 : Float64;
        jmaxZ @12 : Float64;
        vmax @13 : Float64;
    }

    # == PINs related commands ==

    dummy0 @0 () -> ();
    dummy1 @1 () -> ();
    setFeedoptCommitCfg @2 (commit: Bool) -> ();
    getCyclicData @3 () -> (data: CyclicData);

    setLcctSetMachineModeHoming @4 (mode: Bool) -> ();
    setLcctSetMachineModeStream @5 (mode: Bool) -> ();
    setLcctSetMachineModeJog @6 (mode: Bool) -> ();
    setLcctSetMachineModeInactive @7 (mode: Bool) -> ();
    setLcctSetMachineModeGCode @8 (mode: Bool) -> ();

    dummy9 @9 () -> ();

    setStartHoming @10 (mode: Bool) -> ();
    setStopHoming  @11 (mode: Bool) -> ();

    setHomePositionX @12 (position: Float64) -> ();
    setHomePositionY @13 (position: Float64) -> ();
    setHomePositionZ @14 (position: Float64) -> ();

    setSpeedSpindle  @15 (speed: Float64) -> ();
    setActiveSpindle @16 (mode: Bool) -> ();
    setSpindleThreshold @52 (percent: Int32) -> ();

    setJogX @17 (mode: Bool) -> ();
    setJogY @18 (mode: Bool) -> ();
    setJogZ @19 (mode: Bool) -> ();

    setRelJog    @20  (value: Float64) -> ();
    setPlusJog   @21 (plus: Bool) -> ();
    setMinusJog @22 (minus: Bool) -> ();

    setAbsJog   @23 (value: Float64) -> ();
    setGoJog    @24 (mode: Bool)     -> ();
    setSpeedJog @25 (speed: Float64) -> ();
    setStopJog  @26 (mode: Bool)     -> ();

    setOffset  @27 (x: Float64, y: Float64, z: Float64, c: Float64) -> ();
    dummy28    @28 ();
    dummy29    @29 ();

    setStartStream @30 (mode: Bool) -> ();
    setStopStream  @31 (mode: Bool) -> ();

    setGcodeStart @32 (mode: Bool) -> ();
    setGcodePause @33 (mode: Bool) -> ();

    setFaultReset @34 (reset: Bool) -> ();

    setFeedrateScale   @35 (scale: Float64) -> ();
    setFeedoptReset    @36 (reset: Bool) -> ();

    # == Log related commands ==
    readLog @37 () -> (message: Text);

    # == share memory between feedopt and GUI related commands ==

    setFeedoptConfig @38 (config : FeedOptCfg) -> ();
    getFeedoptConfig @39 () -> (config : FeedOptCfg);

    # == Toolpath channel/sample support

    toolpathStartChannel @40 (sampleRate: Int32) -> (result : Bool);
    toolpathStopChannel @41 () -> ();

    struct PinValue {
        value :union {
            u @0: UInt32;
            s @1: Int32;
            f @2: Float64;
            b @3: Bool;
        }
    }

    struct Sample {
        values @0 :List(PinValue);
    }

    toolpathReadSamples  @42 () -> (samples: List(Sample));

    # == File support
    sendFileParam @43 (fileName: Text, size: UInt32, fileOp: Int32) -> (result: Int32);
    sendFileData @44 (data: Data) -> (result: Int32);
    pathExist @45 (path: Text) -> (result: Bool);
    createFolder @46 (folderPath: Text) -> (result: Bool);

    setLoadStream       @47 (mode: Bool) -> ();
    setPauseStream      @48 (mode: Bool) -> ();
    samplerNewFile @49 () -> ();
    getFileData @50 () -> (data: Data);
    setSamplerDownloadFile @51 (download: Bool) -> ();
}

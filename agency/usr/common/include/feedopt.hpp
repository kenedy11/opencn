#pragma once

#include <semaphore.h>
#include <vector>

#include <matlab_headers.h>

#if !defined(MATLAB_MEX_FILE)
#include <matlab/generated/sinspace.h>
#endif

#define MAX_SHARED_CURV_STRUCTS (1024 * 1024)

struct SharedCurvStructs {
    sem_t mutex;
    int generation;
    int curv_struct_count;
    ocn::CurvStruct curv_structs[MAX_SHARED_CURV_STRUCTS];
};

void push_curv_struct(const ocn::CurvStruct* curv);



#ifdef DEBUG_RS274

#define DUMMY_FN                                                                                   \
    do {                                                                                           \
        fprintf(stderr, "DUMMY_CALL to %s%s%s\n", ASCII_CYAN, __FUNCTION__, ASCII_RESET);          \
    } while (0)
#define FNLOG(fmt, args...)                                                                        \
    fprintf(stderr, "[%s%s%s] " fmt "\n", ASCII_GREEN, __FUNCTION__, ASCII_RESET, ##args)

#else

#define DUMMY_FN
#define FNLOG(...)

#endif

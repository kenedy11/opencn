#!/bin/bash

function usage {
  echo "$0 [OPTIONS]"
  echo "  -c        Clean"
  echo "  -d        Debug build"
  echo "  -h        Print this help"
}

clean=n
debug=n

while getopts cdh option
  do
    case "${option}"
      in
        c) clean=y;;
        d) debug=y;;
        h) usage && exit 1;;
    esac
  done

SCRIPT=$(readlink -f $0)
SCRIPTPATH=`dirname $SCRIPT`

if [ $clean == y ]; then
  echo "Cleaning $SCRIPTPATH/build-host"
  rm -rf $SCRIPTPATH/build-host
  exit
fi

if [ $debug == y ]; then
  build_type="Debug"
else
  build_type="Release"
fi

echo "Starting $build_type build"
mkdir -p $SCRIPTPATH/build-host

cd $SCRIPTPATH/build-host
cmake -DCMAKE_BUILD_TYPE=$build_type -DOPENCN_BUILD_HOST=ON  ..

NRPROC=$((`cat /proc/cpuinfo | awk '/^processor/{print $3}' | wc -l` + 1))
make -j$NRPROC

cd -


function cp_app_to_bin {
  [ -f $1 ] && echo "Copy $1" && cp $1 build-host/bin
}


# copy generated binaries to the 'bin' folder
rm -rf $SCRIPTPATH/build-host/bin
mkdir -p $SCRIPTPATH/build-host/bin

# usr-gui
cp_app_to_bin build-host/host/user_gui/user-gui

# companion demo
cp_app_to_bin build-host/host/companion_demo/companion_demo

/** RTAPI is a library providing a uniform API for several real time
 operating systems.
 This version is devoted to the opencn project.
 */
/********************************************************************
 * Description:  opencn_rtapi.c
 *               Realtime RTAPI implementation for the opencn-amp platform.
 *
 * Author: John Kasunich, Paul Corner
 * Authors: Jean-Pierre Miceli, Kevin Joly, Daniel Rossier
 * License: GPL Version 2
 *
 * Copyright (c) 2004 All rights reserved.
 * Copyright (c) 2019 REDS Institute, HEIG-VD
 *
 * Last change:
 ********************************************************************/

/** This file, 'rtai_rtapi.c', implements the realtime portion of the
 API for the RTAI platform.  The API is defined in rtapi.h, which
 includes documentation for all of the API functions.  The non-
 real-time portion of the API is implemented in rtai_ulapi.c (for
 the RTAI platform).  This implementation attempts to prevent
 kernel panics, 'oops'es, and other nasty stuff that can happen
 when writing and testing realtime code.  Wherever possible,
 common errors are detected and corrected before they can cause a
 crash.  This implementation also includes several /proc filesystem
 entries and numerous debugging print statements.
 */

/** Copyright (C) 2003 John Kasunich
 <jmkasunich AT users DOT sourceforge DOT net>
 Copyright (C) 2003 Paul Corner
 <paul_c AT users DOT sourceforge DOT net>
 This library is based on version 1.0, which was released into
 the public domain by its author, Fred Proctor.  Thanks Fred!
 */

/* This library is free software; you can redistribute it and/or
 modify it under the terms of version 2 of the GNU General Public
 License as published by the Free Software Foundation.
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/** THE AUTHORS OF THIS LIBRARY ACCEPT ABSOLUTELY NO LIABILITY FOR
 ANY HARM OR LOSS RESULTING FROM ITS USE.  IT IS _EXTREMELY_ UNWISE
 TO RELY ON SOFTWARE ALONE FOR SAFETY.  Any machinery capable of
 harming persons must have provisions for completely removing power
 from all motors, etc, before persons enter any danger area.  All
 machinery must be designed to comply with local and national safety
 codes, and the authors of this software can not, and do not, take
 any responsibility for such compliance.

 This code was written as part of the EMC HAL project.  For more
 information, go to www.linuxcnc.org.
 */

#include <stdio.h>
#include <stdarg.h>		/* va_* */
#include <errno.h>

#include <opencn/uapi/rtapi.h>

static int msg_level = RTAPI_MSG_ALL; /* message printing level */

#define BUFFERLEN 1024

void default_rtapi_msg_handler(msg_level_t level, const char *fmt, va_list ap) {
	char buf[BUFFERLEN];
	vsnprintf(buf, BUFFERLEN, fmt, ap);

	printf("%s", buf);
}

static rtapi_msg_handler_t rtapi_msg_handler = default_rtapi_msg_handler;

rtapi_msg_handler_t rtapi_get_msg_handler(void) {
	return rtapi_msg_handler;
}

void rtapi_set_msg_handler(rtapi_msg_handler_t handler) {
	if (handler == NULL)
		rtapi_msg_handler = default_rtapi_msg_handler;
	else
		rtapi_msg_handler = handler;
}

void rtapi_print(const char *fmt, ...) {
	va_list args;

	va_start(args, fmt);
	rtapi_msg_handler(RTAPI_MSG_ALL, fmt, args);
	va_end(args);
}

void rtapi_print_msg(msg_level_t level, const char *fmt, ...) {
	va_list args;

	if ((level <= msg_level) && (msg_level != RTAPI_MSG_NONE)) {
		va_start(args, fmt);
		rtapi_msg_handler(level, fmt, args);
		va_end(args);
	}
}


int rtapi_set_msg_level(int level) {
	if ((level < RTAPI_MSG_NONE) || (level > RTAPI_MSG_ALL)) {
		return -EINVAL;
	}
	msg_level = level;
	return 0;
}

int rtapi_get_msg_level(void) {
	return msg_level;
}

#if 0

/***********************************************************************
 *                        I/O RELATED FUNCTIONS                         *
 ************************************************************************/

void rtapi_outb(unsigned char byte, unsigned int port) {
	outb(byte, port);
}

unsigned char rtapi_inb(unsigned int port) {
	return inb(port);
}

int rtapi_is_realtime() {
	return 1;
}
int rtapi_is_kernelspace() {
	return 1;
}


#endif

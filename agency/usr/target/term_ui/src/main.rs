#[allow(dead_code)]
mod util;

fn clamp<T: Ord>(value: T, min: T, max: T) -> T {
    if value < min {
        min
    } else if value > max {
        max
    } else {
        value
    }
}

use crate::util::event::{Config, Event, Events};
use std::marker::PhantomData;
use std::{error::Error, io};
use termion::{event::Key, input::MouseTerminal, raw::IntoRawMode, screen::AlternateScreen};
use tui::{
    backend::TermionBackend,
    layout::{Constraint, Direction, Layout},
    style::{Color, /*Modifier, */ Style},
    widgets::{Block, Borders, Gauge, Paragraph, Text},
    Terminal,
};

use std::ffi::c_void;
use std::os::raw::c_char;
// use std::ptr::null;
use std::time::Duration;

// from aflib
#[cfg(TargetOpenCN)]
#[link(name = "opencn", kind = "static")]
#[allow(dead_code)]
extern "C" {
    fn pin_find_by_name(name: *const c_char) -> *mut c_void;
    fn pin_get_value(ptr: *const c_void) -> *mut c_void;
}

#[repr(C)]
union HalPinData {
    b: bool,
    s: i32,
    u: u32,
    f: f64,
}

#[cfg(TargetOpenCN)]
struct HalPin<T> {
    phantom: PhantomData<T>,
    name: String,
    handle: *mut c_void,
}

#[cfg(not(TargetOpenCN))]
struct HalPin<T> {
    phantom: PhantomData<T>,
    name: String,
    handle: *mut c_void,
    dummy: HalPinData,
}

#[cfg(TargetOpenCN)]
impl<T> HalPin<T> {
    pub fn new(name: &str) -> Self {
        Self {
            phantom: PhantomData,
            name: String::from(name),
            handle: std::ptr::null_mut(),
        }
    }

    fn handle(&mut self) -> *const c_void {
        if self.handle.is_null() {
            unsafe {
                self.handle =
                    pin_find_by_name(std::ffi::CString::new(self.name.as_str()).unwrap().as_ptr());
            }
        }
        unsafe { self.handle.as_mut().unwrap() }
    }

    fn data_ref(&mut self) -> &HalPinData {
        let h = self.handle();
        unsafe { (pin_get_value(h) as *const HalPinData).as_ref().unwrap() }
    }

    fn data_ref_mut(&mut self) -> &mut HalPinData {
        let h = self.handle();
        unsafe { (pin_get_value(h) as *mut HalPinData).as_mut().unwrap() }
    }
}

#[cfg(not(TargetOpenCN))]
impl<T> HalPin<T> {
    pub fn new(name: &str) -> Self {
        Self {
            phantom: PhantomData,
            name: String::from(name),
            handle: std::ptr::null_mut(),
            dummy: HalPinData { u: 0 },
        }
    }

    fn data_ref(&mut self) -> &HalPinData {
        &self.dummy
    }

    fn data_ref_mut(&mut self) -> &mut HalPinData {
        &mut self.dummy
    }
}

trait HalPinValue<T> {
    fn get(&mut self) -> T;
    fn set(&mut self, value: T);
}

impl HalPinValue<bool> for HalPin<bool> {
    fn get(&mut self) -> bool {
        unsafe { self.data_ref().b }
    }

    fn set(&mut self, value: bool) {
        self.data_ref_mut().b = value;
    }
}

impl HalPinValue<i32> for HalPin<i32> {
    fn get(&mut self) -> i32 {
        unsafe { self.data_ref().s }
    }

    fn set(&mut self, value: i32) {
        self.data_ref_mut().s = value;
    }
}

impl HalPinValue<u32> for HalPin<u32> {
    fn get(&mut self) -> u32 {
        unsafe { self.data_ref().u }
    }

    fn set(&mut self, value: u32) {
        self.data_ref_mut().u = value;
    }
}

impl HalPinValue<f64> for HalPin<f64> {
    fn get(&mut self) -> f64 {
        unsafe { self.data_ref().f }
    }

    fn set(&mut self, value: f64) {
        self.data_ref_mut().f = value;
    }
}

// enum MachineMode {
//     Inactive,
//     Homing,
//     Jog,
//     Stream,
//     GCode,
// }

// struct App {
//     in_machine_mode_inactive: HalPin<bool>,
//     in_machine_mode_homing: HalPin<bool>,
//     in_machine_mode_jog: HalPin<bool>,
//     in_machine_mode_stream: HalPin<bool>,
//     in_machine_mode_gcode: HalPin<bool>,
// }

// impl App {
//     fn new() -> App {
//         App {
//             in_machine_mode_inactive: HalPin::new("lcct.in-machine-mode-inactive"),
//             in_machine_mode_homing: HalPin::new("lcct.in-machine-mode-homing"),
//             in_machine_mode_jog: HalPin::new("lcct.in-machine-mode-jog"),
//             in_machine_mode_stream: HalPin::new("lcct.in-machine-mode-stream"),
//             in_machine_mode_gcode: HalPin::new("lcct.in-machine-mode-gcode"),
//         }
//     }

//     fn in_mode(&mut self) -> Option<MachineMode> {
//         if self.in_machine_mode_inactive.get() {
//             Some(MachineMode::Inactive)
//         } else if self.in_machine_mode_homing.get() {
//             Some(MachineMode::Homing)
//         } else if self.in_machine_mode_jog.get() {
//             Some(MachineMode::Jog)
//         } else if self.in_machine_mode_stream.get() {
//             Some(MachineMode::Stream)
//         } else if self.in_machine_mode_gcode.get() {
//             Some(MachineMode::GCode)
//         } else {
//             None
//         }
//     }
// }

fn connection_string() -> String {
    // use std::net::{Ipv4Addr, Ipv6Addr};
    let all_interfaces = pnet::datalink::interfaces();
    let iface = all_interfaces
        .iter()
        .filter(|e| !e.is_loopback() && e.is_up() && e.ips.len() > 0)
        .next();
    if let Some(iface) = iface {
        for network in &iface.ips {
            if let pnet::ipnetwork::IpNetwork::V4(ip) = network {
                if !ip.ip().is_loopback() && !ip.ip().is_link_local() {
                    return ip.ip().to_string();
                }
            }
        }
        return String::from("");
    } else {
        return String::from("");
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let mut pin_feedopt_optimisation_count: HalPin<i32> = HalPin::new("feedopt.optimising.count");
    let mut pin_feedopt_optimisation_progress: HalPin<i32> =
        HalPin::new("feedopt.optimising.progress");
    let mut pin_feedopt_resampling_progress: HalPin<i32> =
        HalPin::new("feedopt.resampling.progress");
    let mut pin_feedopt_queue_size: HalPin<i32> = HalPin::new("feedopt.queue-size");

    let mut pin_x = HalPin::<f64>::new("lcct.target-position-0");
    let mut pin_y = HalPin::<f64>::new("lcct.target-position-1");
    let mut pin_z = HalPin::<f64>::new("lcct.target-position-2");

    let mut pin_runtime_s = HalPin::<i32>::new("lcct.gcode.runtime-s");

    // Terminal initialization
    let stdout = io::stdout().into_raw_mode()?;
    let stdout = MouseTerminal::from(stdout);
    let stdout = AlternateScreen::from(stdout);
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    terminal.hide_cursor()?;
    terminal.clear()?;

    let config = Config {
        exit_key: Key::Char('q'),
        tick_rate: Duration::from_millis(100),
    };

    let events = Events::with_config(config);
    // let mut app = App::new();

    loop {
        terminal.draw(|mut f| {
            let chunks = Layout::default()
                .direction(Direction::Vertical)
                .margin(0)
                .constraints(
                    [
                        Constraint::Length(3),
                        Constraint::Length(3),
                        Constraint::Length(3),
                        Constraint::Length(3),
                    ]
                    .as_ref(),
                )
                .split(f.size());

            let optimisation_percent = {
                if pin_feedopt_optimisation_count.get() > 0 {
                    pin_feedopt_optimisation_progress.get() * 100
                        / pin_feedopt_optimisation_count.get()
                } else {
                    0
                }
            } as u16;
            let optimisation_percent = clamp(optimisation_percent, 0, 100);

            let resampling_percent = {
                if pin_feedopt_optimisation_count.get() > 0 {
                    pin_feedopt_resampling_progress.get() * 100
                        / pin_feedopt_optimisation_count.get()
                } else {
                    0
                }
            } as u16;
            let resampling_percent = clamp(resampling_percent, 0, 100);

            let samples_percent = { pin_feedopt_queue_size.get() * 100 / 1000 } as u16;
            let samples_percent = clamp(samples_percent, 0, 100);

            let label = format!(
                "{}/{}",
                pin_feedopt_optimisation_progress.get(),
                pin_feedopt_optimisation_count.get()
            );
            let gauge = Gauge::default()
                .block(
                    Block::default()
                        .title("Optimisation Progress")
                        .borders(Borders::ALL),
                )
                .style(Style::default().fg(Color::Yellow))
                .percent(optimisation_percent)
                .label(&label);
            f.render_widget(gauge, chunks[0]);

            let label = format!(
                "{}/{}",
                pin_feedopt_resampling_progress.get(),
                pin_feedopt_optimisation_count.get()
            );
            let gauge = Gauge::default()
                .block(
                    Block::default()
                        .title("Resampling Progress")
                        .borders(Borders::ALL),
                )
                .style(Style::default().fg(Color::Yellow))
                .percent(resampling_percent)
                .label(&label);
            f.render_widget(gauge, chunks[1]);

            let label = format!("{}/{}", pin_feedopt_queue_size.get(), 1000);
            let gauge = Gauge::default()
                .block(
                    Block::default()
                        .title("Samples Queue")
                        .borders(Borders::ALL),
                )
                .style(Style::default().fg(Color::Yellow))
                .percent(samples_percent)
                .label(&label);
            f.render_widget(gauge, chunks[2]);

            let duration_sec_total = pin_runtime_s.get();
            let duration_min = duration_sec_total / 60;
            let duration_sec = duration_sec_total % 60;

            let text = [
                Text::Raw(
                    format!(
                        "X: {:7.3}\nY: {:7.3}\nZ: {:7.3}\n",
                        pin_x.get(),
                        pin_y.get(),
                        pin_z.get()
                    )
                    .into(),
                ),
                Text::Raw(format!("Running: {:02}:{:02}\n", duration_min, duration_sec).into()),
                Text::Raw(format!("Ip: {}\n", connection_string()).into()),
            ];
            let text = Paragraph::new(text.iter());
            f.render_widget(text, chunks[3]);
        })?;

        match events.next()? {
            Event::Input(input) => {
                if input == Key::Char('q') {
                    break;
                } else if input == Key::Char('l') {
                    terminal.flush()?;
                }
            }
            Event::Tick => {}
        }
    }

    Ok(())
}

/********************************************************************
 * Description:  loopback.c
 *               User space part of "loopback", a HAL component that
 *      can be used to create pair of in/out pins and loopback anything that is
 *      written on the input pin to the assiociate output pin.
 *
 * Author: Elieva Pignat <elieva.pignat at heig-vd dot ch>
 * License: GPL Version 2
 *
 * Copyright (c) 2006 All rights reserved.
 *
 ********************************************************************/
/** This file, 'loopback.c', is the user part of a HAL component
 that allows to create component with configurable number of pair of HAL pins.
 A pair of HAL pin is composed of an input HAL pin and an output HAL pin of
 the same type. When the realtime module is loaded, anything written on an
 input pin is written back on its associated output pin.

 Invoking:

 'cfg', HAL pins configuration. Char representing the type of the pair of pins
 is concatenated to indicate the configuration of the HAL pins. The chars can
 be 'f' for float type, 'u' for unsigned 32 bits, 's' for signed 32 bits or 'b'
 for bit. For instance, cfg=ffusb would create a HAL component with 5 pairs of
 input/output HAL pins. 2 pairs of float pins, one pair of unsigned pins, one
 pair of signed pins and one pair of bit pin.
 */

/** This program is free software; you can redistribute it and/or
 modify it under the terms of version 2 of the GNU General
 Public License as published by the Free Software Foundation.
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 THE AUTHORS OF THIS LIBRARY ACCEPT ABSOLUTELY NO LIABILITY FOR
 ANY HARM OR LOSS RESULTING FROM ITS USE.  IT IS _EXTREMELY_ UNWISE
 TO RELY ON SOFTWARE ALONE FOR SAFETY.  Any machinery capable of
 harming persons must have provisions for completely removing power
 from all motors, etc, before persons enter any danger area.  All
 machinery must be designed to comply with local and national safety
 codes, and the authors of this software can not, and do not, take
 any responsibility for such compliance.

 This code was written as part of the EMC HAL project.  For more
 information, go to www.linuxcnc.org.
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include <cmdline.h>
#include <af.h>

#include <debug.h>

#include <opencn/uapi/loopback.h>

int exitval = 1; /* program return code - 1 means error */
int ignore_sig = 0; /* used to flag critical regions */
char comp_name[HAL_NAME_LEN + 1]; /* name for this instance of streamer */

static int devfd;

/*
 * Connect to the kernel HAL.
 * @comp_name: name of the component (unique)
 * @devname
 * @cfg HAL pins configuration
 * @returns 0 if successful, a negative code if it failed.
 */
int loopback_connect(char *comp_name, char *devname, char *cfg) {
    loopback_connect_args_t args = {0};
    int ret = 0;

    devfd = open(devname, O_RDWR);

    if (comp_name) {
        strncpy(args.name, comp_name, sizeof(args.name));
    }
    if (cfg) {
        strncpy(args.cfg, cfg, sizeof(args.cfg));
    }

    ret = ioctl(devfd, LOOPBACK_IOCTL_CONNECT, &args);
    if (ret != 0) {
        BUG()
    }

    printf("hal_connect returned with ret = %d\n", ret);

    return ret;
}

/***********************************************************************
 *                            MAIN PROGRAM                              *
 ************************************************************************/

/* signal handler */
static void quit(int sig) {
    if (ignore_sig == 1) {
        return;
    }
    /* Inform the parent (normally halcmd) that we are up and running. */
    kill(getppid(), SIGUSR1);
    exit(0);
}

/*
 * Syntax: loopback cfg=y
 */
int main(int argc, char **argv) {
    int n;
    int ret = 0;
    char *cfg = "";
    char *params = NULL, *value = NULL;

    /* set return code to "fail", clear it later if all goes well */
    exitval = 1;
    for (n = 1; n < argc; n++) {
        /* First parse the option with "=" */
        next_arg(argv[n], &params, &value);

        if (params) {
            if (!strcmp(params, "cfg")) {
                cfg = value;
                continue ;
            }
        }
    }

    /* register signal handlers - if the process is killed
     we need to call hal_exit() to free the shared memory */
    signal(SIGINT, quit);
    signal(SIGTERM, quit);
    /* connect to HAL */
    /* create a unique module name, to allow for multiple loopbacks */
    snprintf(comp_name, sizeof(comp_name), "halloopback%d", getpid());
    /* connect to the HAL */
    ignore_sig = 1;
    ret = loopback_connect(comp_name, LOOPBACK_DEV_NAME, cfg);
    ignore_sig = 0;


    /* check result */
    if (ret < 0) {
        fprintf(stderr, "ERROR: loopback_connect failed with ret = %d\n", ret);
        goto out;
    }

    /* run was succesfull */
    exitval = 0;
    /* Inform the parent (normally halcmd) that we are up and running. */
    kill(getppid(), SIGUSR1);

out:
    ignore_sig = 1;

    return exitval;
}



#include <fcntl.h>
#include <csignal>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>

#include <sys/ipc.h>
#include <sys/shm.h>

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <mutex>
#include <queue>
#include <thread>

extern "C" {
#include <debug.h>
#include <gsl/gsl_bspline.h>
#include <opencn/uapi/feedopt.h>

#include <matlab/common/src/c_spline.h>

#include <af.h>

#include <sys/time.h>
}

#include "feedopt.hpp"
//#define TRACY_TIMER_QPC
//#define TRACY_NO_INVARIANT_CHECK
#include "common/tracy/Tracy.hpp"
#include "common/tracy/TracyClient.cpp"

#include "rs274ngc/rs274ngc_interp.hh"

#include <cblas.h>
#include <af.hpp>

using namespace std::chrono_literals;



class LowPass {
public:
	explicit LowPass(double time_constant, double initial_value) noexcept : m_time_constant{time_constant}, m_value{initial_value} {}

	void reset(double value) {
		m_value = value;
	}

	double update(double dt, double value) {
		const double a = dt/(dt + m_time_constant);
		m_value = a*value + (1-a)*m_value;
		return m_value;
	}

	explicit operator double() const {
		return m_value;
	}

private:
	double m_time_constant;
	double m_value;
};



static int exitval = 1;	   /* program return code - 1 means error */
static int ignore_sig = 0; /* used to flag critical regions */

static int devfd = 0;

static timeval tv_tic, tv_toc;

static void tic()
{
	gettimeofday(&tv_tic, nullptr);
}

static double toc()
{
	gettimeofday(&tv_toc, nullptr);
	return static_cast<double>(tv_toc.tv_sec - tv_tic.tv_sec) + static_cast<double>(tv_toc.tv_usec - tv_tic.tv_usec) / 1e6;
}

/*
 * Connect to the kernel HAL.
 * @mod_name: name of the component (unique)
 * @returns a comp_id (component ID).
 */
int feedopt_connect(const char *comp_name, const char *device_name)
{
    FNTRACE;
	feedopt_connect_args_t args = {};
	int ret;

	devfd = open(device_name, O_RDWR);

	if (comp_name)
		strncpy(args.name, comp_name, sizeof(args.name));

	ret = ioctl(devfd, FEEDOPT_IOCTL_CONNECT, &args);
	if (ret != 0)
		BUG();

	printf("hal_connect returned with ret = %d\n", ret);

	return ret;
}

void feedopt_disconnect(void)
{
    FNTRACE;
	ioctl(devfd, FEEDOPT_IOCTL_DISCONNECT);
	close(devfd);
}

/***********************************************************************
 *                            MAIN PROGRAM                              *
 ************************************************************************/

/* signal handler */
static sig_atomic_t stop;
static void quit(int /* sig*/)
{
    FNTRACE;
	if (ignore_sig)
	{
		return;
	}
	stop = 1;
}

#define BUF_SIZE 4000

// std::vector<CurvStruct> curv_structs;

enum FEEDOPT_STATE { INACTIVE, ACTIVE };

namespace
{

HalPinBool pin_active{"feedopt.us-active"};
HalPinBool pin_start{"feedopt.us-start"};
HalPinBool pin_reset{"feedopt.opt-us-reset"};
HalPinBool pin_commit{"feedopt.commit-cfg"};
HalPinU32 pin_queue_size{"feedopt.queue-size"};
HalPinBool pin_rt_active{"feedopt.rt-active"};
HalPinU32 pin_sampling_period_ns{"feedopt.sampling-period-ns"};


HalPinI32 pin_optimising_progress{"feedopt.optimising.progress"};
HalPinI32 pin_optimising_count{"feedopt.optimising.count"};
HalPinI32 pin_resampling_progress{"feedopt.resampling.progress"};
HalPinBool pin_resampling_paused{"feedopt.resampling.paused"};

HalPinDouble pin_manual_override{"feedopt.resampling.manual_override"};
HalPinDouble pin_auto_override{"feedopt.resampling.auto_override"};

struct OptStruct {
    bool end_flag = false;
    ocn::CurvStruct curv;
};

std::vector<ocn::CurvStruct> opt_splines;
std::vector<OptStruct> opt_curv_structs;

ocn::SplineBase Bl;

std::mutex opt_mutex;
std::condition_variable opt_cv;

//

std::thread resample_thread;
std::atomic_bool stop_resample{true};

ocn::FeedoptContext ctx;

// hal_float_t* pin_opt_per_second = nullptr;

// shared memory for configuration
key_t config_key{0};
int config_shmid{0};
ocn::FeedoptConfig *config_mem{nullptr};

Interp interp;
FEEDOPT_STATE state = FEEDOPT_STATE::INACTIVE;

LowPass resample_dt{1e-2, 0.0};


} // namespace

void feedopt_resample_worker(uint32_t resample_period_ns);

void reset_buttons()
{
    FNTRACE
    pin_reset = false;
	pin_start = false;
	pin_commit = false;
}

void set_active() {
    state = FEEDOPT_STATE::ACTIVE;
#if OPENCN_FEEDOPT_DEBUG
    printf("FEEDOPT: state = ACTIVE\n");
#endif
}

void set_inactive() {
    state = FEEDOPT_STATE::INACTIVE;
#if OPENCN_FEEDOPT_DEBUG
    printf("FEEDOPT: state = INACTIVE\n");
#endif
}

void send_reset()
{
	FNTRACE
#if OPENCN_FEEDOPT_DEBUG
	printf("FEEDOPT: sending FEEDOPT_IOCTL_RESET\n");
#endif
	ioctl(devfd, FEEDOPT_IOCTL_RESET, 0);
#if OPENCN_FEEDOPT_DEBUG
	printf("FEEDOPT: completed FEEDOPT_IOCTL_RESET\n");
    opt_splines.clear();
    opt_curv_structs.clear();
#endif
}

void feedopt_state_inactive()
{
	ZoneScoped;
	FNTRACE
	pin_active = false;

	if (pin_reset) {
#if OPENCN_FEEDOPT_DEBUG
	    printf("FEEDOPT(inactive): Received reset\n");
#endif
		stop_resample = true;

		// wait for the resampling thread to finish its  business before continuing
		if (resample_thread.joinable()) {
			resample_thread.join();
		}
		send_reset();
		reset_buttons();
	}

	else if (pin_commit) {
		reset_buttons();
#if OPENCN_FEEDOPT_DEBUG
		printf("FEEDOPT: Commit\n");
		printf("config_mem = %p\n", config_mem);
#endif
		ocn::FeedoptConfig cfg{};
		ocn::FeedoptDefaultConfig(&cfg);
		if (config_mem) {
			cfg = *config_mem;
		}

		cfg.Compressing.Skip = false;

		ocn::InitFeedoptPlan(cfg, &ctx);
		ocn::bspline_copy(&ctx.Bl, &Bl);

		opt_curv_structs.clear();
		opt_splines.clear();

		stop_resample = false;
		resample_thread = std::thread(feedopt_resample_worker, (uint32_t)pin_sampling_period_ns);
		pthread_setname_np(resample_thread.native_handle(), "resample_thread");

		set_active();
	}
}

void feedopt_state_active()
{
	FNTRACE
	ZoneScoped;
	pin_active = true;

	if (pin_reset) {
		reset_buttons();
		set_inactive();
		stop_resample = true;

		// wait for the resampling thread to finish its  business before continuing
		if (resample_thread.joinable()) {
			resample_thread.join();
		}

		send_reset();
		return;
	}

	auto previous_op = ctx.op;

	if (ctx.op != ocn::Fopt_Finished) {
		ocn::CurvStruct opt_struct;
		ocn::CurvStruct opt_spline;
		bool optimized = false;
		ocn::FeedoptPlan(&ctx, &optimized, &opt_struct);

		if (optimized) {
			{
				std::lock_guard<std::mutex> lock(opt_mutex);
				opt_curv_structs.push_back({false, opt_struct});

				ctx.q_splines.get(opt_struct.sp_index, &opt_spline);
				opt_splines.push_back(opt_spline);

				ctx.go_next = true;
				pin_optimising_count = ctx.q_split.size();
				pin_optimising_progress = opt_curv_structs.size();
			}
			opt_cv.notify_one();
		}

		if (ctx.op == ocn::Fopt_Finished) {
		    // If the optimisation is finished, push a last struct acting as an end flag
            {
                std::lock_guard<std::mutex> lock(opt_mutex);
                opt_curv_structs.push_back({true, {}});
                opt_splines.push_back({});
            }
            opt_cv.notify_one();
			set_inactive();
		}

#if OPENCN_FEEDOPT_DEBUG
		switch(previous_op) {
		    case ocn::Fopt_GCode:
		        printf("FEEDOPT: Read %d structs from %s\n", ctx.q_gcode.size(), ctx.cfg.source);
		        break;
            case ocn::Fopt_Compress:
                printf("FEEDOPT: Compress: %d\n", ctx.q_compress.size());
                break;
            case ocn::Fopt_Smooth:
                printf("FEEDOPT: Smooth: %d\n", ctx.q_smooth.size());
                break;
            case ocn::Fopt_Split:
                printf("FEEDOPT: Split: %d\n", ctx.q_split.size());
                break;
		    default:
		        break;
		}

		if (ctx.op == ocn::Fopt_Finished) {
		    printf("FEEDOPT: Optimization finished\n");
		    printf("Jmax was increased %d times\n", ctx.jmax_increase_count);
		}
#endif
	}
}

/**
 * @brief Tries sending the sample to the kernel module until it succeeds or resample_stop = true
 * @param sample
 */
static void send_sample(feedopt_sample_t sample)
{
	ZoneScoped;
	while (write(devfd, &sample, sizeof(sample)) == PushStatus_TryAgain && !stop_resample) {
		sched_yield();
	}
}

static void adapt_auto_override() {
	if (pin_rt_active && pin_resampling_progress > pin_optimising_progress - 50 && pin_optimising_progress < pin_optimising_count) {
		pin_auto_override = 0.01;
	} else {
		pin_auto_override = 1.0;
	}

	if (pin_resampling_paused) {
		pin_auto_override = 0.0;
	}
}

void feedopt_resample_worker(uint32_t resample_period_ns)
{
	ZoneScoped;
	const double dt = resample_period_ns*1e-9;
	ocn::ResampleStateClass resampleState{};
	ocn::ResampleState(dt*pin_auto_override*pin_manual_override, &resampleState);

	resample_dt.reset(dt*pin_auto_override*pin_manual_override);

	size_t k = 0;
	OptStruct Curv;
	ocn::CurvStruct Spline;

	while (!stop_resample) {
		std::unique_lock<std::mutex> lk(opt_mutex);
		if(opt_cv.wait_for(lk, 1ms, [k](){return k < opt_curv_structs.size() && !stop_resample;})) {
			// wait_for returns true if the predicate has been satisfied

			Curv = opt_curv_structs.at(k);
            Spline = opt_splines.at(k);

			// as soon as we get the two structs, we can unlock the mutex that the optimization worker
			// uses for protecting the two queues
			lk.unlock();

			// Check if the new struct is the end flag, if so, stop resampling and send a last sample
			// acting as the end flag
			if (Curv.end_flag) {
                feedopt_sample_t sample;
                sample.end_flag = 1;
                send_sample(sample);
			    stop_resample = true;
			    continue;
			}
			{
				ZoneScopedN("ResampleOuter");
				ocn::ResampleNoCtx(&resampleState, &Bl, &Curv.curv);
			}
			while (!resampleState.go_next && !stop_resample) {
				adapt_auto_override();
                resampleState.dt = resample_dt.update(dt, pin_auto_override*pin_manual_override*dt);
				feedopt_sample_t sample;
				{
					ZoneScopedN("EvalPosition");
					ocn::EvalPosition(&Curv.curv, &Spline, resampleState.u, sample.axis_position);
				}
				sample.end_flag = 0;
				sample.index = k;
                sample.gcode_line = Curv.curv.gcode_source_line;
                sample.spindle_speed = Curv.curv.SpindleSpeed;

				send_sample(sample);
				{
					ZoneScopedN("ResampleInner");
					ocn::ResampleNoCtx(&resampleState, &Bl, &Curv.curv);
				}
			}

            resampleState.go_next = false;
			k++;
		}
	}
#if OPENCN_FEEDOPT_DEBUG
	printf("FEEDOPT: Resampling stopped\n");
#endif
}

void feedopt_opt_worker()
{
	FNTRACE
	ZoneScoped;
	tic();
	while (!stop) {
		switch (state) {
		case FEEDOPT_STATE::INACTIVE:
			feedopt_state_inactive();
			usleep(100);
			break;
		case FEEDOPT_STATE::ACTIVE:
			feedopt_state_active();
			break;
		}
		double t = toc();
		if (t > 1.0) {
			// TODO: Bring back the OPT/sec metric
			tic();
		}
	}
}

int main(int argc, char ** argv)
{
	int ret = 0;

	cpu_set_t cpuset;

	CPU_ZERO(&cpuset);
	CPU_SET(2, &cpuset);
	CPU_SET(3, &cpuset);

	sched_setaffinity(getpid(), sizeof(cpuset), &cpuset);

	signal(SIGINT, quit);
	signal(SIGTERM, quit);

	signal(SIGPIPE, SIG_IGN);

	/* connect to the HAL */
	ignore_sig = 1;
	ret = feedopt_connect("feedopt-us", FEEDOPT_DEV_NAME);
	ignore_sig = 0;

	/* Inform the parent (normally halcmd) that we are up and running. */
	kill(getppid(), SIGUSR1);

	/* check result */
	if (ret < 0) {
		fprintf(stderr, "ERROR: feedopt_connect failed with ret = %d\n", ret);
		exitval = 0;

		ignore_sig = 1;

		feedopt_disconnect();

		return exitval;
	}

	// setup shared memory
	config_key = ftok(FEEDOPT_SHMEM_NAME, FEEDOPT_SHMEM_KEY_CONFIG);
	config_shmid = shmget(config_key, sizeof(ocn::FeedoptConfig), 0666 | IPC_CREAT);
	config_mem = static_cast<ocn::FeedoptConfig *>(shmat(config_shmid, nullptr, 0));

	if ((void *)config_mem == (void *) -1) {
		perror("shmat");
		fprintf(stderr, "Failed to create shared memory for CONFIG\n");
		config_mem = nullptr;
	}

	ocn::FeedoptConfig cfg{};
	ocn::FeedoptDefaultConfig(&cfg);

	if (config_mem) {
		*config_mem = cfg;
	}

	reset_buttons();


	//	int num_threads = openblas_get_num_threads();
	//	printf("OPENBLAS: Using %d threads\n", num_threads);
	fflush(stdout);

	feedopt_opt_worker();

	/* run was succesfull */
	exitval = 0;

	ignore_sig = 1;

	feedopt_disconnect();

	return exitval;
}

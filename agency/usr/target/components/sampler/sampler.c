/********************************************************************
 * Description:  sampler.c
 *               User space part of "streamer", a HAL component that
 *		can be used to stream data from a file onto HAL pins
 *		 at a specific realtime sample rate.
 *
 * Author: John Kasunich <jmkasunich at sourceforge dot net>
 * License: GPL Version 2
 *
 * Copyright (c) 2006 All rights reserved.
 *
 ********************************************************************/
/** This file, 'streamer_usr.c', is the user part of a HAL component
 that allows numbers stored in a file to be "streamed" onto HAL
 pins at a uniform realtime sample rate.  When the realtime module
 is loaded, it creates a stream in shared memory.  Then, the user
 space program 'hal_stream' is invoked.  'hal_stream' takes
 input from stdin and writes it to the stream, and the realtime
 part transfers the data from the stream to HAL pins.

 Invoking:


 'chan_num', if present, specifies the streamer channel to use.
 The default is channel zero.  Since hal_stream takes its data
 from stdin, it will almost always either need to have stdin
 redirected from a file, or have data piped into it from some
 other program.
 */

/** This program is free software; you can redistribute it and/or
 modify it under the terms of version 2 of the GNU General
 Public License as published by the Free Software Foundation.
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 THE AUTHORS OF THIS LIBRARY ACCEPT ABSOLUTELY NO LIABILITY FOR
 ANY HARM OR LOSS RESULTING FROM ITS USE.  IT IS _EXTREMELY_ UNWISE
 TO RELY ON SOFTWARE ALONE FOR SAFETY.  Any machinery capable of
 harming persons must have provisions for completely removing power
 from all motors, etc, before persons enter any danger area.  All
 machinery must be designed to comply with local and national safety
 codes, and the authors of this software can not, and do not, take
 any responsibility for such compliance.

 This code was written as part of the EMC HAL project.  For more
 information, go to www.linuxcnc.org.
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sendfile.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include <sys/ipc.h>
#include <sys/shm.h>

#include <sampler.h>
#include <af.h>
#include <cmdline.h>

#include <opencn/uapi/sampler.h>

#include <debug.h>

#define MAX_FILENAME_LEN 1000

#define LOGS_HEADER_FILENAME "logs/logs_header.txt"
#define LOGS_FILENAME "logs/logfile.txt"
#define PARAMS_FILENAME "logs/fopt_params.txt"

int exitval = 1; /* program return code - 1 means error */
int ignore_sig = 0; /* used to flag critical regions */
int linenumber = 0; /* used to print linenumber on errors */
char comp_name[HAL_NAME_LEN + 1]; /* name for this instance of streamer */

int devfd;

/*
 * Connect to the kernel HAL.
 * @mod_name: name of the component (unique)
 * @returns a comp_id (component ID).
 */
int sampler_connect(char *comp_name, char *devname, int channel, int depth, char *cfg) {
	sampler_connect_args_t args = {0};
	int ret;

	devfd = open(devname, O_RDWR);

	if (comp_name)
		strncpy(args.name, comp_name, sizeof(args.name));
	args.channel = channel;
	args.depth = depth;
	if (cfg)
		strncpy(args.cfg, cfg, sizeof(args.cfg));

	ret = ioctl(devfd, SAMPLER_IOCTL_CONNECT, &args);
	if (ret != 0)
		BUG()

	printf("hal_connect returned with ret = %d\n", ret);

	return ret;
}

void sampler_disconnect(void) {

	ioctl(devfd, SAMPLER_IOCTL_DISCONNECT);

	close(devfd);
}

/***********************************************************************
 *                            MAIN PROGRAM                              *
 ************************************************************************/

/* signal handler */
static sig_atomic_t stop;
static void quit(int sig) {
	if (ignore_sig) {
		return;
	}
	stop = 1;
}

#define BUF_SIZE 1000
static sampler_sample_t s_sample;

/*
 * Syntax: sampler depth=x cfg=y
 */
int main(int argc, char **argv) {
	int n, channel, tag;
	char *cp, *cp2;
	long int samples;
	char buf[BUF_SIZE];
	int ret, read_status = 0;
	unsigned int this_sample, last_sample = 0;
	int depth = 0;
	char *cfg = "";
	char *params = NULL, *value = NULL;
	FILE *fd = NULL, *fd_header = NULL, *fd_params = NULL;
	communicationChannel_t *sample_channel = NULL;
	/* set return code to "fail", clear it later if all goes well */
	exitval = 1;
	channel = 0;
	tag = 0;
	stop = 0;
	samples = -1; /* -1 means run forever */
	uint32_t curr_sample_freq = 0;
	int nsamples = 0;

    hal_pin_t *pin_new_file = NULL;

    fprintf(stderr, "[SAMPLER] Parse arguments\n");
	for (n = 1; n < argc; n++) {
		cp = argv[n];

		/* First parse the option with "=" */
		next_arg(argv[n], &params, &value);

		if (params) {
			if (!strcmp(params, "depth")) {
				depth = atoi(value);
				continue ;
			}

			if (!strcmp(params, "cfg")) {
			cfg = value;
			continue ;
			}
		}

		if (*cp != '-') {
			break;
		}
		switch (*(++cp)) {
			case 'c':
				if ((*(++cp) == '\0') && (++n < argc)) {
					cp = argv[n];
				}
				channel = strtol(cp, &cp2, 10);
				if ((*cp2) || (channel < 0) || (channel >= MAX_SAMPLERS)) {
					fprintf(stderr, "ERROR: invalid channel number '%s'\n", cp);
					exit(1);
				}
				break;
			case 'n':
				if ((*(++cp) == '\0') && (++n < argc)) {
					cp = argv[n];
				}
				samples = strtol(cp, &cp2, 10);
				if ((*cp2) || (samples < 0)) {
					fprintf(stderr, "ERROR: invalid sample count '%s'\n", cp);
					exit(1);
				}
				break;
			case 't':
				tag = 1;
				break;
			default:
				fprintf(stderr, "ERROR: unknown option '%s'\n", cp);
				exit(1);
		}
	}

    fprintf(stderr, "[SAMPLER] register signals handlers\n");

	/* register signal handlers - if the process is killed
	 we need to call hal_exit() to free the shared memory */
	signal(SIGINT, quit);
	signal(SIGTERM, quit);
	signal(SIGPIPE, SIG_IGN);

	/* connect to HAL */
	/* create a unique module name, to allow for multiple streamers */
	snprintf(comp_name, sizeof(comp_name), "halsampler%d", getpid());

    fprintf(stderr, "[SAMPLER] sampler_connect\n");
	/* connect to the HAL */
	ignore_sig = 1;
	ret = sampler_connect(comp_name, SAMPLER_DEV_NAME, channel, depth, cfg);
	ignore_sig = 0;


    fprintf(stderr, "[SAMPLER] inform parent\n");

	/* Inform the parent (normally halcmd) that we are up and running. */
	kill(getppid(), SIGUSR1);

	/* check result */
	if (ret < 0) {
		fprintf(stderr, "[SAMPLER] ERROR: sampler_connect failed with ret = %d\n", ret);
		goto out;
	}

    fprintf(stderr, "[SAMPLER] pin_find_by_name\n");

	pin_new_file = pin_find_by_name("sampler.0.new-file");

#if OPENCN_SAMPLER_DEBUG
    fprintf(stderr, "[SAMPLER] Initial fopen\n");
#endif

    fd = fopen("/root/sampler_log.csv", "w");
    if (!fd) {
        fprintf(stderr, "[SAMPLER] Failed to open log file\n");
        exitval = -1;
        goto out;
    }

#if OPENCN_SAMPLER_DEBUG
    fprintf(stderr, "[SAMPLER] Starting loop\n");
#endif

	while (!stop && (samples != 0)) {
		/* Give a chance to the others threads */
		usleep(1);

		if (pin_get_value(pin_new_file)->b) {
#if OPENCN_SAMPLER_DEBUG
		    fprintf(stderr, "[SAMPLER] Received new-file request\n");
#endif
		    fclose(fd);
            fd = fopen("/root/sampler_log.csv", "w");

            FILE* header_fd = fopen("/root/header.txt", "r");
            if (header_fd) {
                struct stat stats;
                stat("/root/header.txt", &stats);
                char* header_content = malloc(stats.st_size);
                if (header_content) {
                    fread(header_content, stats.st_size, 1, header_fd);
                    fwrite(header_content, stats.st_size, 1, fd);
                    free(header_content);
                } else {
                    fprintf(stderr, "[SAMPLER] Failed to alloc memory for header copy\n");
                }
                fclose(header_fd);
            } else {
				fprintf(stderr, "[SAMPLER] Failed to open header file\n");
			}

            // opencn-server waits for this value to switch back to 0 in order to complete the request
            pin_get_value(pin_new_file)->b = 0;
#if OPENCN_SAMPLER_DEBUG
            fprintf(stderr, "[SAMPLER] Ack new-file\n");
#endif

            if (!fd) {
                fprintf(stderr, "[SAMPLER] failed to open log file\n");
                exitval = -1;
                goto out;
            }
		}

		/* WARNING: read here is non-blocking */
		if(read(devfd, (void*)&s_sample, sizeof(sampler_sample_t)) == 0) {
            nsamples++;
            if (tag)
                fprintf(fd, "%d ", nsamples);

            for (int i = 0; i < s_sample.n_pins; i++) {
                switch (s_sample.pins[i].type) {
                    case HAL_FLOAT:
                        fprintf(fd, "%f ", s_sample.pins[i].f);
                        break;
                    case HAL_S32:
                        fprintf(fd, "%d ", s_sample.pins[i].s);
                        break;
                    case HAL_U32:
                        fprintf(fd, "%u ", s_sample.pins[i].u);
                        break;
                    case HAL_BIT:
                        fprintf(fd, "%d ", s_sample.pins[i].b);
                        break;
                    default:
                        fprintf(fd, "%s", "<UNHANDLED> ");
                        break;
                }
            }

            fputs("\n", fd);

            if (samples > 0) {
                samples--;
            }
        }
		fflush(fd);
	}

	/* run was succesfull */
	exitval = 0;

	close_channel(sample_channel);

out:
	fprintf(stderr, "[SAMPLER] Exited with %d\n", exitval);

	if (fd != NULL) {
		fclose(fd);
		fd = NULL;
	}

	ignore_sig = 1;

	sampler_disconnect();

	return exitval;
}

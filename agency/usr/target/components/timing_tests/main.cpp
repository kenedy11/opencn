
//#include <cblas-openblas.h>
#include <matlab_headers.h>
#include <openblas_config.h>
#include <cstdio>
#include <time.h>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include "sinspace.h"

namespace
{
timespec t1, t2;
ocn::FeedoptContext ctx;
}

int feedopt_read_gcode(int)
{
    return 1;
}

void tic()
{
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t1);
}

// returns elapsed time since tic in [ns]
double toc()
{
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t2);
    return (t2.tv_sec - t1.tv_sec) * 1e9 + (t2.tv_nsec - t1.tv_nsec);
}

int get_double(int argc, const char **argv, const char *name, double *value)
{
    for (int i = 0; i < argc; i++) {
        if (strcmp(name, argv[i]) == 0) {
            char *end;
            *value = strtod(argv[i + 1], &end);
            if (end == argv[i + 1]) {
                printf("FAILED TO PARSE '%s' as FLOAT for '%s'\n", argv[i + 1], name);
                return 0;
            }
            printf("Found '%s' = %f\n", name, (double)*value);
            return 1;
        }
    }
    return 0;
}

int get_int(int argc, const char **argv, const char *name, int *value)
{
    for (int i = 0; i < argc; i++) {
        if (strcmp(name, argv[i]) == 0) {
            char *end;
            *value = strtol(argv[i + 1], &end, 10);
            if (end == argv[i + 1]) {
                printf("FAILED TO PARSE '%s' as INT for '%s'\n", argv[i + 1], name);
                return 0;
            }
            printf("Found '%s' = %d\n", name, *value);
            return 1;
        }
    }
    return 0;
}
int main(int argc, const char **argv)
{
    ocn::FeedoptConfig cfg;

    ocn::FeedoptDefaultConfig(&cfg);


    double value_float;
    int value_int;

    int Nopt = 1;

    if (get_int(argc, argv, "NDiscr", &value_int)) {
        cfg.NDiscr = value_int;
    }

    if (get_int(argc, argv, "NBreak", &value_int)) {
        cfg.NBreak = value_int;
    }

    if (get_int(argc, argv, "NHorz", &value_int)) {
        cfg.NHorz = value_int;
    }

    if (get_int(argc, argv, "Nopt", &value_int)) {
        Nopt = value_int;
    }

    if (get_double(argc, argv, "amax", &value_float)) {
        cfg.amax[0] = value_float;
        cfg.amax[1] = value_float;
        cfg.amax[2] = value_float;
    }

    if (get_double(argc, argv, "jmax", &value_float)) {
        cfg.jmax[0] = value_float;
        cfg.jmax[1] = value_float;
        cfg.jmax[2] = value_float;
    }

    if (get_double(argc, argv, "CutOff", &value_float)) {
        cfg.CutOff = value_float;
    }
    double feedrate = 1;
    if (get_double(argc, argv, "FeedRate", &value_float)) {
        feedrate = value_float;
    }

//     printf("using %d blas threads\n", openblas_get_num_threads());
//     printf("par config: %d\n", openblas_get_parallel());

    //    cpu_set_t cpuset;

    //    CPU_ZERO(&cpuset);
    //    CPU_SET(2, &cpuset);
    //    CPU_SET(3, &cpuset);

    //    sched_setaffinity(getpid(), sizeof(cpuset), &cpuset);

    ocn::InitFeedoptPlan(cfg, &ctx);

    const double P[][3] = {{0, 0, 0}, {1, 1, 0}, {1, 0, 1}, {0, 0, 0}};
//    const double C[][3] ={{0,1,0}, };

    const double PLines[][3] = {{0, 0, 0}, {1, 0, 0}, {0, 1, 0}};

    const double evec[][3] = {{0, 0, 1}, {1, 0, 0}, {0, -1, 0}};

    const double theta[] = {M_PI_2, M_PI_2, M_PI_2};

    ocn::CurvStruct c1, c2, c3;
    ocn::CurvStruct c1c, ct1, c2c, ct2, ct3;

//    ConstrHelixStruct(P[0], P[1], evec[0], theta[0], 0.0, feedrate, ocn::ZSpdMode_NN, &c1);
//    ConstrHelixStruct(P[1], P[2], evec[1], theta[1], 0.0, feedrate, ocn::ZSpdMode_NN, &c2);
//    ConstrHelixStruct(P[2], P[3], evec[2], theta[2], 0.0, feedrate, ocn::ZSpdMode_NN, &c3);

    //    ConstrLineStruct(PLines[0], PLines[1], 1, ZSpdMode_NN, &c1);
    //    ConstrLineStruct(PLines[1], PLines[2], 1, ZSpdMode_NN, &c2);

    //    printf("L = %f\n", LengthHelix(P[0], P[1], evec[0], theta[0], 0.0));
    //    return 0;

    printf("==== CALC TRANSITION ====\n");
    printf("Warming path\n");
    fflush(stdout);
    for (int i = 0; i < 1000; i++) {
        ocn::TransitionResult status = ocn::TransitionResult_Ok;
        ocn::CalcTransition(&ctx, &c1, &c2, &c1c, &ct1, &c2c, &status);
    }

    int N = 1000;
    printf("Running %d times\n", N);
    fflush(stdout);
    tic();
    for (int i = 0; i < N; i++) {
        ocn::TransitionResult status = ocn::TransitionResult_Ok;
        ocn::CalcTransition(&ctx, &c1, &c2, &c1c, &ct1, &c2c, &status);
    }
    double t = toc();
    printf("DONE: t = %f [ms], dt = %f [us]\n", t / 1e6, t / N / 1e3);
    fflush(stdout);

//    double Coeffs[g_FeedoptConfig.MaxNHorz * g_FeedoptConfig.MaxNCoeff];
//    int CoeffsSize[2];

//    int CurvStructsSize[2] = {1, g_FeedoptConfig.NHorz};
//    CurvStruct CurvStructs[11];
//    CalcTransition(&c1, &c2, g_FeedoptConfig.CutOff, &CurvStructs[0], &CurvStructs[1],
//                   &CurvStructs[2]);
//    CalcTransition(&CurvStructs[2], &c3, g_FeedoptConfig.CutOff, &CurvStructs[2], &CurvStructs[3],
//                   &CurvStructs[4]);
//    CalcTransition(&CurvStructs[4], &c1, g_FeedoptConfig.CutOff, &CurvStructs[4], &CurvStructs[5],
//                   &CurvStructs[6]);
//    CalcTransition(&CurvStructs[6], &c2, g_FeedoptConfig.CutOff, &CurvStructs[6], &CurvStructs[7],
//                   &CurvStructs[8]);
//    CalcTransition(&CurvStructs[8], &c3, g_FeedoptConfig.CutOff, &CurvStructs[8], &CurvStructs[9],
//                   &CurvStructs[10]);

//    printf("==== FEEDRATE PLANNING ====\n");
//    printf("NBreak = %d\n"
//           "NDiscr = %d\n"
//           "NHorz  = %d\n"
//           "amax   = %f\n"
//           "jmax   = %f\n"
//           "CutOff = %f\n"
//           "FeedRate = %f\n\n",
//           g_FeedoptConfig.NBreak, g_FeedoptConfig.NDiscr, g_FeedoptConfig.NHorz,
//           g_FeedoptConfig.amax[0], g_FeedoptConfig.jmax[0], g_FeedoptConfig.CutOff, feedrate);

//    printf("Warming path\n");
//    fflush(stdout);
//    BenchmarkFeedratePlanning(CurvStructs, CurvStructsSize, 10, Coeffs, CoeffsSize, &t);

//    printf("Running %d times\n", Nopt);
//    BenchmarkFeedratePlanning(CurvStructs, CurvStructsSize, Nopt, Coeffs, CoeffsSize, &t);
//    printf("DONE: t = %f [s], dt = %f [ms]\n", t, t * 1e3 / Nopt);
//    fflush(stdout);

	return 0;
}

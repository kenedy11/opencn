/********************************************************************
 *  Copyright (C) 2012 Sascha Ittner <sascha.ittner@modusoft.de>
 *  Copyright (C) 2019 Jean-Pierre Miceli Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 ********************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <expat.h>

#include <opencn/uapi/lcec_conf.h>

#include "lcec_conf_priv.h"

typedef enum {
	icmdTypeNone = 0,
	icmdTypeMailbox,
	icmdTypeCoe,
	icmdTypeCoeIcmds,
	icmdTypeCoeIcmd,
	icmdTypeCoeIcmdTrans,
	icmdTypeCoeIcmdComment,
	icmdTypeCoeIcmdTimeout,
	icmdTypeCoeIcmdCcs,
	icmdTypeCoeIcmdIndex,
	icmdTypeCoeIcmdSubindex,
	icmdTypeCoeIcmdData,
	icmdTypeSoe,
	icmdTypeSoeIcmds,
	icmdTypeSoeIcmd,
	icmdTypeSoeIcmdTrans,
	icmdTypeSoeIcmdComment,
	icmdTypeSoeIcmdTimeout,
	icmdTypeSoeIcmdOpcode,
	icmdTypeSoeIcmdDriveno,
	icmdTypeSoeIcmdIdn,
	icmdTypeSoeIcmdElements,
	icmdTypeSoeIcmdAttribute,
	icmdTypeSoeIcmdData,
} lcec_icmd_type_t;

typedef struct {
	lcec_conf_xml_inst_t xml;

	lcec_slave_sdoconf_t *first_sdo_config;
	lcec_slave_sdoconf_t *last_sdo_config;
	lcec_slave_sdoconf_t *curr_sdo_config;

	lcec_slave_idnconf_t *first_idn_config;
	lcec_slave_idnconf_t *last_idn_config;
	lcec_slave_idnconf_t *curr_idn_config;
} lcec_conf_icmds_state_t;

static void icmdTypeCoeIcmdStart(lcec_conf_xml_inst_t *inst, int next, const char **attr)
{
	lcec_conf_icmds_state_t *state = (lcec_conf_icmds_state_t *) inst;
	lcec_slave_sdoconf_t *sdo_config = (lcec_slave_sdoconf_t *)malloc(sizeof(lcec_slave_sdoconf_t));

	state->curr_sdo_config = sdo_config;

	sdo_config->index = 0xffff;
	sdo_config->subindex = 0xff;
}

static void icmdTypeCoeIcmdEnd(lcec_conf_xml_inst_t *inst, int next)
{
	lcec_conf_icmds_state_t *state = (lcec_conf_icmds_state_t *) inst;
	lcec_slave_sdoconf_t *sdo_config = state->curr_sdo_config;

	if (sdo_config->index == 0xffff) {
		fprintf(stderr, "%s: ERROR: sdoConfig has no idx attribute\n", module_name);
		XML_StopParser(inst->parser, 0);
		return;
	}

	if (sdo_config->subindex == 0xff) {
		fprintf(stderr, "%s: ERROR: sdoConfig has no subIdx attribute\n", module_name);
		XML_StopParser(inst->parser, 0);
		return;
	}

	LCEC_LIST_APPEND(state->first_sdo_config, state->last_sdo_config, sdo_config);
}


static void icmdTypeSoeIcmdStart(lcec_conf_xml_inst_t *inst, int next, const char **attr)
{
	lcec_conf_icmds_state_t *state = (lcec_conf_icmds_state_t *) inst;
	lcec_slave_idnconf_t *idn_config = (lcec_slave_idnconf_t *)malloc(sizeof(lcec_slave_idnconf_t));

	state->curr_idn_config = idn_config;

	state->curr_idn_config->drive = 0;
	state->curr_idn_config->idn = 0xffff;
	state->curr_idn_config->state = 0;
}

static void icmdTypeSoeIcmdEnd(lcec_conf_xml_inst_t *inst, int next)
{
	lcec_conf_icmds_state_t *state = (lcec_conf_icmds_state_t *) inst;
	lcec_slave_idnconf_t *idn_config = state->curr_idn_config;


	if (state->curr_idn_config->idn == 0xffff) {
		fprintf(stderr, "%s: ERROR: idnConfig has no idn attribute\n", module_name);
		XML_StopParser(inst->parser, 0);
		return;
	}

	if (state->curr_idn_config->state == 0) {
		fprintf(stderr, "%s: ERROR: idnConfig has no state attribute\n", module_name);
		XML_StopParser(inst->parser, 0);
		return;
	}

	LCEC_LIST_APPEND(state->first_idn_config, state->last_idn_config, idn_config);
}

static const lcec_conf_xml_hanlder_t xml_states[] = {
	{ "EtherCATMailbox", icmdTypeNone, icmdTypeMailbox, NULL, NULL },
	{ "CoE", icmdTypeMailbox, icmdTypeCoe, NULL, NULL },
	{ "InitCmds", icmdTypeCoe, icmdTypeCoeIcmds, NULL, NULL },
	{ "InitCmd", icmdTypeCoeIcmds, icmdTypeCoeIcmd, icmdTypeCoeIcmdStart, icmdTypeCoeIcmdEnd },
	{ "Transition", icmdTypeCoeIcmd, icmdTypeCoeIcmdTrans, NULL, NULL },
	{ "Comment", icmdTypeCoeIcmd, icmdTypeCoeIcmdComment, NULL, NULL },
	{ "Timeout", icmdTypeCoeIcmd, icmdTypeCoeIcmdTimeout, NULL, NULL },
	{ "Ccs", icmdTypeCoeIcmd, icmdTypeCoeIcmdCcs, NULL, NULL },
	{ "Index", icmdTypeCoeIcmd, icmdTypeCoeIcmdIndex, NULL, NULL },
	{ "SubIndex", icmdTypeCoeIcmd, icmdTypeCoeIcmdSubindex, NULL, NULL },
	{ "Data", icmdTypeCoeIcmd, icmdTypeCoeIcmdData, NULL, NULL },
	{ "SoE", icmdTypeMailbox, icmdTypeSoe, NULL, NULL },
	{ "InitCmds", icmdTypeSoe, icmdTypeSoeIcmds, NULL, NULL },
	{ "InitCmd", icmdTypeSoeIcmds, icmdTypeSoeIcmd, icmdTypeSoeIcmdStart, icmdTypeSoeIcmdEnd },
	{ "Transition", icmdTypeSoeIcmd, icmdTypeSoeIcmdTrans, NULL, NULL },
	{ "Comment", icmdTypeSoeIcmd, icmdTypeSoeIcmdComment, NULL, NULL },
	{ "Timeout", icmdTypeSoeIcmd, icmdTypeSoeIcmdTimeout, NULL, NULL },
	{ "OpCode", icmdTypeSoeIcmd, icmdTypeSoeIcmdOpcode, NULL, NULL },
	{ "DriveNo", icmdTypeSoeIcmd, icmdTypeSoeIcmdDriveno, NULL, NULL },
	{ "IDN", icmdTypeSoeIcmd, icmdTypeSoeIcmdIdn, NULL, NULL },
	{ "Elements", icmdTypeSoeIcmd, icmdTypeSoeIcmdElements, NULL, NULL },
	{ "Attribute", icmdTypeSoeIcmd, icmdTypeSoeIcmdAttribute, NULL, NULL },
	{ "Data", icmdTypeSoeIcmd, icmdTypeSoeIcmdData, NULL, NULL },
	{ "NULL", -1, -1, NULL, NULL }
};

static int parseHex(const char *s, int slen, uint8_t *buf)
{
	char c;
	int len;
	bool nib = 0;
	uint8_t tmp = 0;

	for (len = 0; (slen == -1 || slen > 0) && (c = *s) != 0; s++) {
		/* update remaining length */
		if (slen > 0) {
			slen --;
		}

		/* skip blanks if no current nibble */
		if (!nib && strchr(" \t\r\n", c)) {
			continue;
		}

		/* get nibble value */
		if (c >= '0' && c <= '9') {
			c = c - '0';
		} else if (c >= 'a' && c <= 'f') {
			c = c - 'a' + 10;
		} else if (c >= 'A' && c <= 'F') {
			c = c - 'A' + 10;
		} else {
			return -1;
		}

		/* store nibble */
		if (nib) {
			tmp |= c & 0x0f;
			if (buf) {
				*(buf++) = tmp;
			}
			len++;
		} else {
			tmp = c << 4;
		}
		nib = !nib;
	}

	/* nibble must not be active */
	if (nib) {
		return -1;
	}

	return len;
}

static long int parse_int(lcec_conf_icmds_state_t *state, const char *s, int len, long int min, long int max)
{
	char buf[32];
	char *end;
	long int ret;

	if (s == NULL || len == 0) {
		fprintf(stderr, "%s: ERROR: Missing number value\n", module_name);
		XML_StopParser(state->xml.parser, 0);
		return 0;
	}

	if (len >= sizeof(buf)) {
		fprintf(stderr, "%s: ERROR: Number value size exceeded\n", module_name);
		XML_StopParser(state->xml.parser, 0);
		return 0;
	}

	strncpy(buf, s, len);
	buf[len] = 0;

	ret = strtol(buf, &end, 0);
	if (*end != 0 || ret < min || ret > max) {
		fprintf(stderr, "%s: ERROR: Invalid number value '%s'\n", module_name, s);
		XML_StopParser(state->xml.parser, 0);
		return 0;
	}

	return ret;
}

static int parse_data(lcec_conf_icmds_state_t *state, uint8_t **data, const char *s, int len)
{
	int size;
	uint8_t *tmp = NULL;

	/* get size */
	size = parseHex(s, len, NULL);
	if (size < 0) {
		fprintf(stderr, "%s: ERROR: Invalid data\n", module_name);
		XML_StopParser(state->xml.parser, 0);
		return 0;
	}

	/* allocate memory */
	tmp = (uint8_t *)malloc(sizeof(uint8_t)*size);

	/* parse data */
	parseHex(s, len, tmp);

	*data = tmp;

	return size;
}

static void xml_data_handler(void *data, const XML_Char *s, int len)
{
	lcec_conf_xml_inst_t *inst = (lcec_conf_xml_inst_t *) data;
	lcec_conf_icmds_state_t *state = (lcec_conf_icmds_state_t *) inst;
	lcec_slave_sdoconf_t *sdo_config = state->curr_sdo_config;
	lcec_slave_idnconf_t *idn_config = state->curr_idn_config;

	switch (inst->state) {
	case icmdTypeCoeIcmdTrans:
		if (len == 2) {
			if (strncmp("IP", s, len) == 0) {
				return;
			}
			if (strncmp("PS", s, len) == 0) {
				return;
			}
		}
		fprintf(stderr, "%s: ERROR: Invalid Transition state\n", module_name);
		XML_StopParser(inst->parser, 0);
		return;
	case icmdTypeCoeIcmdIndex:
		sdo_config->index = parse_int(state, s, len, 0, 0xffff);
		return;
	case icmdTypeCoeIcmdSubindex:
		sdo_config->subindex = parse_int(state, s, len, 0, 0xff);
		return;
	case icmdTypeCoeIcmdData:
		sdo_config->length = parse_data(state, &sdo_config->data, s, len);
		return;

	case icmdTypeSoeIcmdTrans:
		if (len == 2) {
			if (strncmp("IP", s, len) == 0) {
				idn_config->state = EC_AL_STATE_PREOP;
				return;
			}
			if (strncmp("PS", s, len) == 0) {
				idn_config->state = EC_AL_STATE_PREOP;
				return;
			}
			if (strncmp("SO", s, len) == 0) {
				idn_config->state = EC_AL_STATE_SAFEOP;
				return;
			}
		}
		fprintf(stderr, "%s: ERROR: Invalid Transition state\n", module_name);
		XML_StopParser(inst->parser, 0);
		return;
	case icmdTypeSoeIcmdDriveno:
		idn_config->drive = parse_int(state, s, len, 0, 7);
		return;
    case icmdTypeSoeIcmdIdn:
		idn_config->idn = parse_int(state, s, len, 0, 0xffff);
		return;
	case icmdTypeSoeIcmdData:
		idn_config->length = parse_data(state, &idn_config->data, s, len);
		return;
	}
}

int parseIcmds(lcec_slave_t *slave, const char *filename)
{
	int ret = 1;
	int done;
	char buffer[LCEC_BUFFSIZE];
	FILE *file;
	lcec_conf_icmds_state_t state;

	/* open file */
	file = fopen(filename, "r");
	if (file == NULL) {
		fprintf(stderr, "%s: ERROR: unable to open config file %s\n", module_name, filename);
		goto fail1;
	}

	/* create xml parser */
	memset(&state, 0, sizeof(state));
	if (initXmlInst((lcec_conf_xml_inst_t *) &state, xml_states)) {
		fprintf(stderr, "%s: ERROR: Couldn't allocate memory for parser\n", module_name);
		goto fail2;
	}

	/* setup handlers */
	XML_SetCharacterDataHandler(state.xml.parser, xml_data_handler);

	for (done=0; !done;) {
		/* read block */
		int len = fread(buffer, 1, LCEC_BUFFSIZE, file);
		if (ferror(file)) {
			fprintf(stderr, "%s: ERROR: Couldn't read from file %s\n", module_name, filename);
			goto fail3;
		}

		/* check for EOF */
		done = feof(file);

		/* parse current block */
		if (!XML_Parse(state.xml.parser, buffer, len, done)) {
			fprintf(stderr, "%s: ERROR: Parse error at line %u: %s\n", module_name,
						(unsigned int)XML_GetCurrentLineNumber(state.xml.parser),
			XML_ErrorString(XML_GetErrorCode(state.xml.parser)));
			goto fail3;
		}
	}

	if (state.first_sdo_config) {
		slave->sdo_config = state.first_sdo_config;
		state.last_sdo_config->next = NULL;
	}

	if (state.first_idn_config) {
		slave->idn_config = state.first_idn_config;
		state.last_idn_config->next = NULL;
	}

	/* everything is fine */
	ret = 0;

fail3:
	XML_ParserFree(state.xml.parser);
fail2:
	fclose(file);
fail1:
	return ret;
}

/********************************************************************
 *  Copyright (C) 2012 Sascha Ittner <sascha.ittner@modusoft.de>
 *  Copyright (C) 2019 Jean-Pierre Miceli Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 ********************************************************************/

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include <cmdline.h>

#include <opencn/uapi/lcec.h>
#include <opencn/uapi/lcec_conf.h>

#include "lcec_conf_priv.h"

/* Enable this marco to print EtherCAT configuration parsed for XML file */
#if 0
#define PRINT_CONFIG
#endif

char *module_name = "lcec";
int ignore_sig = 0; /* used to flag critical regions */

static int devfd;

/* signal handler */
static sig_atomic_t stop;

static void quit(int sig)
{
	if (ignore_sig) {
		return;
	}
	stop = 1;
}

static int lcec_connect(char *devname, lcec_master_t *lcec_config, int debug)
{
	int ret = 0;
	lcec_connect_args_t args;

	args.name = module_name;
	args.config = lcec_config;
	args.debug = debug;

	devfd = open(devname, O_RDWR);
	if (!devfd) {
		fprintf(stderr, "%s: ERROR: failed to open %s\n", module_name, LCEC_DEV_NAME);
		return -EIO;
	}

	ret = ioctl(devfd, LCEC_IOCTL_CONNECT, &args);
	if (ret)
		fprintf(stderr, "%s: ERROR: Connection error (%d)\n", module_name, ret);

	return ret;
}

/*
 * Syntax: lcec cfg=configfile -d
 */
int main(int argc, char **argv)
{
	char *params = NULL, *value = NULL;
	char *cp;
	int n;
	char *filename = NULL;
	lcec_conf_xml_inst_t xml_inst;
	int ret = -1;
	int debug = 0;

	printf("Starting %s component\n",  module_name);

	if ((argc < 2) || (argc > 3)) {
		fprintf(stderr, "%s: ERROR: invalid arguments\n", module_name);
		fprintf(stderr, "%s: usage: lcec cfg=<ETHERCAT_CFG> [-d]\n", module_name);

		goto out;
	}

	for (n = 1; n < argc; n++) {
		cp = argv[n];

		/* First parse the option with "=" */
		next_arg(argv[n], &params, &value);

		if (!strcmp(params, "cfg")) {
			filename = value;
			continue ;
		}

		if (*cp != '-') {
			break;
		}

		switch (*(++cp)) {
		case 'd':
			debug = 1;
			break;

		default:
			fprintf(stderr, "ERROR: unknown option '%s'\n", cp);
			exit(1);
			break;
		}
	}

	if (filename == NULL) {
		fprintf(stderr, "ERROR: missing EtherCAT configuration file (cfg parameter)\n");
		exit(1);
	}

	/* register signal handlers - if the process is killed
	 we need to call hal_exit() to free the shared memory */
	signal(SIGINT, quit);
	signal(SIGTERM, quit);
	signal(SIGPIPE, SIG_IGN);

	ret = lcec_parse_xml(filename, &xml_inst);
	if (ret) {
		fprintf(stderr, "%s: ERROR: XML parsing failed (%s)\n", module_name, filename);
		goto out;
	}

#ifdef PRINT_CONFIG
	print_parsed_cfg();
#endif

	/* connect to the HAL */
	ignore_sig = 1;
	ret = lcec_connect(LCEC_DEV_NAME, first_master, debug);
	ignore_sig = 0;
	if (ret)
		fprintf(stderr, "%s: ERROR: Connection with kernel failed\n", module_name);

	/* Delete the cfg */
	lcec_clear_config();

out:
	/* Inform the parent (normally halcmd) that we are up and running. */
	kill(getppid(), SIGUSR1);

	return ret;
}

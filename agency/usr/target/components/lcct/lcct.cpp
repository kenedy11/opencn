/********************************************************************
 *  Copyright (C) 2019  Peter Lichard  <peter.lichard@heig-vd.ch>
 *  Copyright (C) 2019 Jean-Pierre Miceli Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 ********************************************************************/

#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <string.h>

#include <opencn/uapi/lcct.h>
#include <af.h>
#include <af.hpp>

char *module_name = "lcct";

namespace {

    int ignore_sig = 0; /* used to flag critical regions */

    int devfd = 0;
    communicationChannel_t *sample_channel = nullptr;

/* signal handler */
    sig_atomic_t stop = 0;

    HalPinDouble p_pos_x{"lcct.joint-pos-cur-in-0"};
    HalPinDouble p_pos_y{"lcct.joint-pos-cur-in-1"};
    HalPinDouble p_pos_z{"lcct.joint-pos-cur-in-2"};
    HalPinI32 p_gcode_line{"feedopt.resampling.gcodeline"};
}

static void quit(int sig) {
    if (ignore_sig) {
        return;
    }
    stop = 1;
}

static int lcct_connect(const char *devname) {
    int ret = 0;
    lcct_connect_args_t args = {0};

    if(devname) strncpy(args.name, module_name, sizeof(args.name));

    devfd = open(devname, O_RDWR);
    if (!devfd) {
        fprintf(stderr, "%s: ERROR: failed to open %s\n", module_name, LCCT_DEV_NAME);
        return -EIO;
    }

    ret = ioctl(devfd, LCCT_IOCTL_CONNECT, &args);
    if (ret) fprintf(stderr, "%s: ERROR: Connection error (%d)\n", module_name, ret);

    return ret;
}

int main(int argc, char **argv) {
    int ret;

    printf("Starting %s component\n", module_name);

    /* register signal handlers - if the process is killed
     we need to call hal_exit() to free the shared memory */
    signal(SIGINT, quit);
    signal(SIGTERM, quit);
    signal(SIGPIPE, SIG_IGN);

    /* connect to the HAL */
    ignore_sig = 1;
    ret = lcct_connect(LCCT_DEV_NAME);
    ignore_sig = 0;

    /* Inform the parent (normally halcmd) that we are up and running. */
    kill(getppid(), SIGUSR1);

    if (ret) {
        fprintf(stderr, "%s: ERROR: Connection with kernel failed\n", module_name);
        goto out;
    }

    sample_channel_set_freq(1);
    open_sample_channel(&sample_channel, O_WRONLY);

    sampler_sample_t sample;
    sample.n_pins = 3;
    sample.pins[0].type = HAL_FLOAT;
    sample.pins[1].type = HAL_FLOAT;
    sample.pins[2].type = HAL_FLOAT;


    while(!stop) {
        if (sample_channel_enabled()) {
            sample.pins[0].f = p_pos_x;
            sample.pins[1].f = p_pos_y;
            sample.pins[2].f = p_pos_z;

            write_sample(sample_channel, &sample);
        }
        /* We want approximately 60 samples/s */
        usleep(1000000/60);
    }

out:
    close(devfd);
    return ret;
}

add_executable(curses_gui main.c)


target_link_libraries(curses_gui opencn ncurses m)
set_target_properties(curses_gui PROPERTIES COMPILE_FLAGS ${_components_cflags})
set_target_properties(curses_gui PROPERTIES LINK_FLAGS "${_components_ldflags}")
target_include_directories(curses_gui PRIVATE ${_components_include_directories})

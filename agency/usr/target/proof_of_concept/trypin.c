#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <fcntl.h>
#include <sampler.h>
#include <string.h>

#include <sys/ioctl.h>

#include <af.h>

#define FEEDOPT_IOCTL_CONNECT _IOW(0x05000000, 0, char)
#define FEEDOPT_IOCTL_DISCONNECT _IOW(0x05000000, 1, char)

typedef struct {
	char name[HAL_NAME_LEN];
	int channel;
	int depth;
} feedopt_connect_args_t;


int main(int argc, char *argv[]) {
	hal_pin_t *pin0;
	hal_data_u *val0;
	int fd;
	feedopt_connect_args_t args;

	fd = open("/dev/opencn/feedopt/0", O_RDWR);

	strcpy(args.name, "feedopt-us");
	ioctl(fd, FEEDOPT_IOCTL_CONNECT, &args);

	pin0 = pin_find_by_name("feedopt.opt-us-reset");
	printf("## pin0: %x\n", pin0);

	printf("## val = %d\n", pin_get_value(pin0)->b);

	ioctl(fd, FEEDOPT_IOCTL_DISCONNECT);

	close(fd);

	return 0;
}

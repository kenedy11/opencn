/*
 * Copyright (C) 2019 Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef OPENCN_PINS_IMPL_HPP
#define OPENCN_PINS_IMPL_HPP

#include "opencn-interface/opencn_interface.capnp.h"

class OpenCNPinsImpl: public OpenCNPins::Server {

    public:
        OpenCNPinsImpl();

    protected:
        ::kj::Promise<void> setPin(SetPinContext context) override;
        ::kj::Promise<void> getPin(GetPinContext context) override;
};

#endif /* OPENCN_PINS_IMPL_HPP */

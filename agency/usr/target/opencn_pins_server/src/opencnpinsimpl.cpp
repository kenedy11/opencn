/*
 * Copyright (C) 2021 Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <iostream>
#include <sys/types.h>

#include "opencnpinsimpl.hpp"

#include "af.h"

OpenCNPinsImpl::OpenCNPinsImpl()
{

}

::kj::Promise<void> OpenCNPinsImpl::setPin(SetPinContext context)
{
	hal_pin_t *pin;
	const char *name = context.getParams().getName().cStr();

	pin = pin_find_by_name(name);
	if (!pin)
		std::cerr << "PIN named " << name << "not found" << std::endl;

	hal_type_t type = pin_get_type(pin);
	switch(type) {
		case HAL_BIT:
			pin_get_value(pin)->b = context.getParams().getValue().getValue().getB();
			break;
		case HAL_FLOAT:
			pin_get_value(pin)->f = context.getParams().getValue().getValue().getF();;
			break;
		case HAL_S32:
			pin_get_value(pin)->s = context.getParams().getValue().getValue().getS();;
			break;
		case HAL_U32:
			pin_get_value(pin)->u = context.getParams().getValue().getValue().getU();;
			break;
		default:
			std::cerr << "PIN type '" << type << "' is not supported" << std::endl;
	}

    return kj::READY_NOW;
}

::kj::Promise<void> OpenCNPinsImpl::getPin(GetPinContext context)
{
	hal_pin_t *pin;
	const char *name = context.getParams().getName().cStr();

	// int size;

	pin = pin_find_by_name(name);
	if (!pin)
		std::cerr << "PIN named " << name << "not found" << std::endl;

	hal_type_t type = pin_get_type(pin);
	switch(type) {
		case HAL_BIT:
			// context.getResults().getValue().initValue().setB(pin_get_value(pin)->b);
			context.getResults().getValue().getValue().setB(pin_get_value(pin)->b);
			break;

		case HAL_FLOAT:
			context.getResults().getValue().getValue().setF(pin_get_value(pin)->f);
			// context.getResults().getValue().initValue().setF(pin_get_value(pin)->f);
			break;

		case HAL_S32:
			context.getResults().getValue().getValue().setS(pin_get_value(pin)->s);
			// context.getResults().getValue().initValue().setS(pin_get_value(pin)->s);
			break;

		case HAL_U32:
			context.getResults().getValue().getValue().setU(pin_get_value(pin)->u);
			// context.getResults().getValue().initValue().setU(pin_get_value(pin)->u);
			break;

		default:
			std::cerr << "PIN type '" << type << "' is not supported" << std::endl;
	}

    return kj::READY_NOW;
}


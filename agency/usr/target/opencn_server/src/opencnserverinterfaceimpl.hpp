/*
 * Copyright (C) 2019 Jean-Pierre Miceli <jean-pierre.miceli@heig-vd.ch>
 *                    Kevin Joly <kevin.joly@heig-vd.ch>
 *                    Peter Lichard <peter.lichard@heig-vd.ch>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef OPENCN_SERVER_INTERFACE_IMPL_HPP
#define OPENCN_SERVER_INTERFACE_IMPL_HPP

#include "opencn-interface/opencn_interface.capnp.h"

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#include "sampler.h"
#include "af.h"
#include "feedopt.hpp"

#include <af.hpp>

class OpenCNServerInterfaceImpl: public OpenCNServerInterface::Server {
    public:
        OpenCNServerInterfaceImpl();

    protected:
        ::kj::Promise<void> setFeedoptCommitCfg(SetFeedoptCommitCfgContext context) override;
        ::kj::Promise<void> getCyclicData(GetCyclicDataContext context) override;

        ::kj::Promise<void> setLcctSetMachineModeHoming(SetLcctSetMachineModeHomingContext context) override;
        ::kj::Promise<void> setLcctSetMachineModeStream(SetLcctSetMachineModeStreamContext context) override;
        ::kj::Promise<void> setLcctSetMachineModeJog(SetLcctSetMachineModeJogContext context) override;
        ::kj::Promise<void> setLcctSetMachineModeInactive(SetLcctSetMachineModeInactiveContext context) override;
        ::kj::Promise<void> setLcctSetMachineModeGCode(SetLcctSetMachineModeGCodeContext context) override;

        ::kj::Promise<void> setStartHoming(SetStartHomingContext context) override;
        ::kj::Promise<void> setStopHoming(SetStopHomingContext context) override;

        ::kj::Promise<void> setHomePositionX(SetHomePositionXContext context) override;
        ::kj::Promise<void> setHomePositionY(SetHomePositionYContext context) override;
        ::kj::Promise<void> setHomePositionZ(SetHomePositionZContext context) override;

        ::kj::Promise<void> setSpeedSpindle(SetSpeedSpindleContext context) override;
        ::kj::Promise<void> setActiveSpindle(SetActiveSpindleContext context) override;
        ::kj::Promise<void> setSpindleThreshold(SetSpindleThresholdContext context) override;

        ::kj::Promise<void> setJogX(SetJogXContext context) override;
        ::kj::Promise<void> setJogY(SetJogYContext context) override;
        ::kj::Promise<void> setJogZ(SetJogZContext context) override;

        ::kj::Promise<void> setRelJog(SetRelJogContext context) override;
        ::kj::Promise<void> setPlusJog(SetPlusJogContext context) override;
        ::kj::Promise<void> setMinusJog(SetMinusJogContext context) override;

        ::kj::Promise<void> setAbsJog(SetAbsJogContext context) override;
        ::kj::Promise<void> setGoJog(SetGoJogContext context) override;
        ::kj::Promise<void> setSpeedJog(SetSpeedJogContext context) override;
        ::kj::Promise<void> setStopJog(SetStopJogContext context) override;

        ::kj::Promise<void> setOffset(SetOffsetContext context) override;

        ::kj::Promise<void> setStartStream(SetStartStreamContext context) override;
        ::kj::Promise<void> setStopStream(SetStopStreamContext context) override;
        ::kj::Promise<void> setPauseStream(SetPauseStreamContext context) override;
        ::kj::Promise<void> setLoadStream(SetLoadStreamContext context) override;

        ::kj::Promise<void> setGcodeStart(SetGcodeStartContext context) override;
        ::kj::Promise<void> setGcodePause(SetGcodePauseContext context) override;

        ::kj::Promise<void> setFaultReset(SetFaultResetContext context) override;

        ::kj::Promise<void> setFeedrateScale(SetFeedrateScaleContext context) override;
        ::kj::Promise<void> setFeedoptReset(SetFeedoptResetContext context) override;

        ::kj::Promise<void> readLog(ReadLogContext context) override;

        ::kj::Promise<void> setFeedoptConfig(SetFeedoptConfigContext context) override;
        ::kj::Promise<void> getFeedoptConfig(GetFeedoptConfigContext context) override;

        ::kj::Promise<void> toolpathStartChannel(ToolpathStartChannelContext context) override;
        ::kj::Promise<void> toolpathStopChannel(ToolpathStopChannelContext context) override;
        ::kj::Promise<void> toolpathReadSamples(ToolpathReadSamplesContext context) override;

        ::kj::Promise<void> sendFileParam(SendFileParamContext context) override;
        ::kj::Promise<void> sendFileData(SendFileDataContext context) override;
        ::kj::Promise<void> pathExist(PathExistContext context) override;
        ::kj::Promise<void> createFolder(CreateFolderContext context) override;
        ::kj::Promise<void> samplerNewFile(SamplerNewFileContext context) override;
        ::kj::Promise<void> getFileData(GetFileDataContext context) override;

    private:
        HalPinI32 _pinFeedoptOptimisingProgress{"feedopt.optimising.progress"};
        HalPinI32 _pinFeedoptOptimisingCount{"feedopt.optimising.count"};
        HalPinI32 _pinFeedoptResamplingProgress{"feedopt.resampling.progress"};

        HalPinDouble _pinFeedoptResamplingManualOverride{"feedopt.resampling.manual_override"};
        HalPinDouble _pinFeedoptResamplingAutoOverride{"feedopt.resampling.auto_override"};

        HalPinBool _pinGcodeRunning{"lcct.gcode-running"};
        HalPinBool _pinStreamRunning{"lcct.stream-running"};

        HalPinBool _pinSamplerNewFile{"sampler.0.new-file"};

        HalPinDouble _pinFeedoptSampleX{"feedopt.sample-0"};
        HalPinDouble _pinFeedoptSampleY{"feedopt.sample-1"};
        HalPinDouble _pinFeedoptSampleZ{"feedopt.sample-2"};
        HalPinI32 _pinFeedoptLine{"feedopt.resampling.gcodeline"};

        HalPinBool _pinFeedoptCommitCfg{"feedopt.commit-cfg"};
        HalPinI32 _pinFeedoptQueueSize{"feedopt.queue-size"};
        HalPinBool _pinLcctSetMachineModeHoming{"lcct.set-machine-mode-homing"};
        HalPinBool _pinLcctSetMachineModeStream{"lcct.set-machine-mode-stream"};
        HalPinBool _pinLcctSetMachineModeJog{"lcct.set-machine-mode-jog"};
        HalPinBool _pinLcctSetMachineModeInactive{"lcct.set-machine-mode-inactive"};
        HalPinBool _pinLcctSetMachineModeGCode{"lcct.set-machine-mode-gcode"};

        HalPinBool _pinInModeHoming{"lcct.in-machine-mode-homing"};
        HalPinBool _pinInModeStream{"lcct.in-machine-mode-stream"};
        HalPinBool _pinInModeJog{"lcct.in-machine-mode-jog"};
        HalPinBool _pinInModeInactive{"lcct.in-machine-mode-inactive"};
        HalPinBool _pinInModeGCode{"lcct.in-machine-mode-gcode"};

        HalPinBool _pinHomingFinished{"lcct.homing-finished"};
        HalPinBool _pinStreamFinished{"lcct.stream-finished"};
        HalPinBool _pinJogFinished{"lcct.jog-finished"};
        HalPinBool _pinGcodeFinished{"lcct.gcode-finished"};

        HalPinBool _pinStartHoming{"lcct.home.start-homing-sequence"};
        HalPinBool _pinStopHoming{"lcct.home.stop-homing-sequence"};

        HalPinDouble _pinHomePositionX{"lcct.home-position-X"};
        HalPinDouble _pinHomePositionY{"lcct.home-position-Y"};
        HalPinDouble _pinHomePositionZ{"lcct.home-position-Z"};

        HalPinDouble _pinSpeedSpindle{"lcct.gui.spindle-target-velocity"};
        HalPinBool _pinActiveSpindle{"lcct.spindle-active"};
        HalPinDouble _pinSpindleThreshold{"lcct.spindle-threshold"};

        HalPinBool _pinJogX{"lcct.jog.axis-X"};
        HalPinBool _pinJogY{"lcct.jog.axis-Y"};
        HalPinBool _pinJogZ{"lcct.jog.axis-Z"};

        HalPinDouble _pintRelJog{"lcct.jog.move-rel"};
        HalPinBool _pintPlusJog{"lcct.jog.plus"};
        HalPinBool _pintMinusJog{"lcct.jog.minus"};

        HalPinDouble _pintAbsJog{"lcct.jog.move-abs"};
        HalPinBool _pintGoJog{"lcct.jog.goto"};
        HalPinDouble _pintSpeedJog{"lcct.jog.velocity"};
        HalPinBool _pintStopJog{"lcct.jog.stop"};

        HalPinDouble _pinOffsetX{"lcct.spinbox-offset-X"};
        HalPinDouble _pinOffsetY{"lcct.spinbox-offset-Y"};
        HalPinDouble _pinOffsetZ{"lcct.spinbox-offset-Z"};
        HalPinDouble _pinOffsetC{"lcct.spinbox-offset-ThetaZ"};

        HalPinBool _pinStartStream{"lcct.stream.start"};
        HalPinBool _pinStopStream{"lcct.stream.stop"};
        HalPinBool _pinPauseStream{"lcct.stream.pause"};
        HalPinBool _pinLoadStream{"streamer.0.load"};

        HalPinI32 _pinFIFOStream{"streamer.0.curr-depth"};
        HalPinBool _pinGcodeStart{"lcct.gcode.start-in"};
        HalPinBool _pinGcodePause{"lcct.gcode.pause-in"};
        HalPinBool _pinFeedoptReady{"feedopt.ready"};

        HalPinBool _pinFaultReset{"lcct.fault-reset"};

        HalPinDouble _pinCurrPosX{"lcct.joint-pos-cur-in-0"};
        HalPinDouble _pinCurrPosY{"lcct.joint-pos-cur-in-1"};
        HalPinDouble _pinCurrPosZ{"lcct.joint-pos-cur-in-2"};

        HalPinDouble _pinCurrVelSpindle{"lcct.spindle-cur-in"};

        HalPinBool _pinCurrModeInactiveX{"lcct.in-mode-inactive-0"};
        HalPinBool _pinCurrModeInactiveY{"lcct.in-mode-inactive-1"};
        HalPinBool _pinCurrModeInactiveZ{"lcct.in-mode-inactive-2"};
        HalPinBool _pinCurrModeInactiveSpindle{"lcct.in-mode-inactive-3"};

        HalPinBool _pinCurrModeFaultX{"lcct.in-fault-0"};
        HalPinBool _pinCurrModeFaultY{"lcct.in-fault-1"};
        HalPinBool _pinCurrModeFaultZ{"lcct.in-fault-2"};
        HalPinBool _pinCurrModeFaultSpindle{"lcct.in-fault-3"};

        HalPinBool _pinCurrModeHomingX{"lcct.in-mode-hm-0"};
        HalPinBool _pinCurrModeHomingY{"lcct.in-mode-hm-1"};
        HalPinBool _pinCurrModeHomingZ{"lcct.in-mode-hm-2"};
        HalPinBool _pinCurrModeHomingSpindle{"lcct.in-mode-hm-3"};

        HalPinBool _pinCurrModeCSPX{"lcct.in-mode-csp-0"};
        HalPinBool _pinCurrModeCSPY{"lcct.in-mode-csp-1"};
        HalPinBool _pinCurrModeCSPZ{"lcct.in-mode-csp-2"};
        HalPinBool _pinCurrModeCSPSpindle{"lcct.in-mode-csp-3"};

        HalPinBool _pinCurrModeCSVX{"lcct.in-mode-csv-0"};
        HalPinBool _pinCurrModeCSVY{"lcct.in-mode-csv-1"};
        HalPinBool _pinCurrModeCSVZ{"lcct.in-mode-csv-2"};
        HalPinBool _pinCurrModeCSVSpindle{"lcct.in-mode-csv-3"};

        HalPinBool _pinIsHomed{"lcct.homed"};
        HalPinDouble _pinCurrentU{"feedopt.current-u"};
        HalPinBool _pinFeedoptReset{"lcct.gcode.feedopt-reset"};
        HalPinBool _pinFeedoptUsActive{"feedopt.us-active"};
        HalPinBool _pinFeedoptRtActive{"feedopt.rt-active"};

        // shared memory for configuration
        key_t config_key{0};
        int config_shmid{0};
        ocn::FeedoptConfig* config_mem{nullptr};
	    char *config_mem_sampl{nullptr};

        communicationChannel_t *toolpathChannel;
};

#endif // OPENCN_SERVER_INTERFACE_IMPL_HPP

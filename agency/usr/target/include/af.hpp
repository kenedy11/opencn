#pragma once

#include <string>
#include <af.h>
#include <cassert>

class HalPin {
public:
    explicit HalPin(const char* pin_name) noexcept : m_pin_name(pin_name) {
    }

protected:
    inline hal_pin_t* handle() {
        if (m_handle) {
            return m_handle;
        }

        m_handle = pin_find_by_name(m_pin_name.c_str());
        if (!m_handle) {
            fprintf(stderr, "====== ERROR: Failed to find pin: %s\n", m_pin_name.c_str());
            assert(0);
        }
        return m_handle;
    }
private:
    hal_pin_t *m_handle{nullptr};
    std::string m_pin_name;
};

class HalPinI32: public HalPin {
public:
    explicit HalPinI32(const char* pin_name) noexcept : HalPin(pin_name) {}
    inline operator int32_t() {
        return pin_get_value(handle())->s;
    }

    inline void operator=(int32_t value) {
        pin_get_value(handle())->s = value;
    }
};

class HalPinU32: public HalPin {
public:
    explicit HalPinU32(const char* pin_name) noexcept : HalPin(pin_name) {}
    inline operator uint32_t() {
        return pin_get_value(handle())->u;
    }

    inline void operator=(uint32_t value) {
        pin_get_value(handle())->u = value;
    }
};

class HalPinBool: public HalPin {
public:
    explicit HalPinBool(const char* pin_name) noexcept : HalPin(pin_name) {}
    inline operator bool() {
        return pin_get_value(handle())->b;
    }

    inline void operator=(bool value) {
        pin_get_value(handle())->b = value;
    }
};

class HalPinDouble: public HalPin {
public:
    explicit HalPinDouble(const char* pin_name) noexcept : HalPin(pin_name) {}
    inline operator double() {
        return pin_get_value(handle())->f;
    }

    inline void operator=(double value) {
        pin_get_value(handle())->f = value;
    }
};


#ifndef CMDLINE_H
#define CMDLINE_H

#include <stdbool.h>

char *next_arg(char *args, char **param, char **val);
bool parse_option_str(const char *str, const char *option);


#endif /* CMDLINE_H */

#!/bin/bash

# -- This script can be use to build the different components of OpenCN framework
# --
# -- NB: it does not provides option to build Host/GUI apps

# By default, no target is defined
target=''

# Define OpenCN items paths. It is defined relatively the path of this script
#   - 'build.sh' path is <OPENCN_HOME>/agency/build.sh
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
AGENCY=$SCRIPTPATH
ROOTFS=$SCRIPTPATH/rootfs
USR=$SCRIPTPATH/usr
QEMU=$SCRIPTPATH/../qemu
UBOOT=$SCRIPTPATH/../u-boot

usage()
{
    echo "Usage: $0 [OPTIONS] "
    echo ""
    echo "Where OPTIONS are:"
    echo "  -a    build all components"
    echo "  -b    build u-boot and/or qemu depending on the target"
    echo "  -c    clean the selected components (no action if used alone)"
    echo "  -k    build linux kernel"
    echo "  -r    build rootfs (secondary)"
    echo "  -u    build user apps"
    echo "  -t    select the wanted target (rpi4, vexpress, x86, x86-qemu)"
    echo "        It has to be done at least once (creation of build.conf file)"
    echo ""
    exit 1
}

set_config()
{
    if [ $1 ]; then
        if [ "$1" = "rpi4" ] || [ "$1" = "vexpress" ] ||
           [ "$1" = "x86" ]  || [ "$1" = "x86-qemu" ]; then
           touch $AGENCY/build.conf
           echo "PLATFORM=$1" > $AGENCY/build.conf
           PLATFORM=$1
        else
            echo "ERROR: target '$1' is not supported. Valid targets are 'rpi4', 'vexpress', 'x86' or 'x86-qemu'"
            exit 1
        fi
    elif [ ! -f "$AGENCY/build.conf" ]; then
        echo "ERROR: build.conf file does NOT exist, use -t option to select the wanted target"
        exit 1
    else
        echo "read target from the file"
        # auto-export all variables found in build.conf (POSIX compatible method)
        set -o allexport
        . $AGENCY/build.conf
        set +o allexport
    fi
}

build_uboot()
{
    echo "-- [ Build U-Boot ] -----------------------------------------"

    cd $UBOOT

    if [ "$2" = "y" ]; then
        echo "Clean"
        make distclean
    else
        echo "Config=$1"
        make $1
        make -j$(nproc)
    fi

    cd -
}

build_qemu()
{
    echo "-- [ Build qemu ] -------------------------------------------"

    cd $QEMU

    if [ "$2" = "y" ]; then
        make distclean
        cd -
        return
    fi

    if [ "$PLATFORM" = "vexpress" ]; then
        ./configure --target-list=arm-softmmu --disable-attr --disable-werror --disable-docs

    elif [ "$PLATFORM" = "x86-qemu" ]; then
        ./configure --target-list=x86_64-softmmu --enable-kvm

    else
        echo "ERROR: no qemu build for PLATFORM '$1'"
        exit 1
    fi

    make -j$(nproc)

    cd -
}

buid_kernel()
{
    echo "-- [ Build Linux kernel ] ----------------------------------"

    if [ "$1" = "y" ]; then
        echo "Clean"
        cd $AGENCY/linux
        make distclean
    else
        cd $AGENCY
        make
    fi

    cd -
}

build_rootfs()
{
    echo "-- [ Build rootfs ] -----------------------------------------"

    cd $ROOTFS

    if [ "$2" = "y" ]; then
        echo "Clean"
        make distclean
    else
        echo "Config=$1_defconfig"
        make "$1_defconfig"
        make source
        make
    fi

    cd -
}

build_usr()
{
    cd $USR

    echo "Build usr applications"

    if [ "$1" = "y" ]; then
        ./build.sh -c
    else
        ./build.sh
    fi

    cd -
}

while getopts "abckrut:" o; do
    case "$o" in
        a)
            buildBoot=y
            buildKernel=y
            buildRootfs=y
            buildUsr=y
            ;;
        b)
            buildBoot=y
            ;;
        c)
            clean=y
            ;;
        k)
            buildKernel=y
            ;;
        r)
            buildRootfs=y
            ;;
        u)
            buildUsr=y
            ;;
        t)
            target=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done

if [ $OPTIND -eq 1 ]; then usage; fi

set_config $target

# Buid u-boot and/or qemu depending on the PLATFORM
if [ "$buildBoot" == "y" ]; then

    if [ "$PLATFORM" = "rpi4" ]; then
        build_uboot rpi_4_32b_defconfig $clean
    elif [ "$PLATFORM" = "vexpress" ]; then
        build_uboot vexpress_defconfig $clean
        build_qemu vexpress $clean
    elif [ "$PLATFORM" = "x86-qemu" ]; then
        build_qemu x86-qemu $clean
    fi
fi

if [ "$buildKernel" == "y" ]; then
    buid_kernel $clean
fi

if [ "$buildRootfs" == "y" ]; then
    build_rootfs $PLATFORM $clean
fi

if [ "$buildUsr" == "y" ]; then
    build_usr $clean
fi

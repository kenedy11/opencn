#!/usr/bin/env bash

if [ -z $1 ]; then
	echo "Usage: ./install partition"
	exit 1
fi

echo "Formatting $1"
mkfs.ext4 $1

echo "Installing OpenCN to $1"
mount $1 /mnt
cp -a /fs/. /mnt/
grub-install --target=i386-pc --root-directory=/mnt /dev/sda
umount /mnt

echo "Dont forget to add root=$1 to your grub cmdline"

#!/bin/bash

ifconfig eth0 down
ifconfig eth1 down

ethtool -s eth0 speed 10 duplex full
ethtool -s eth1 speed 10 duplex full

ifconfig eth0 up
ifconfig eth1 up

#!/bin/bash

cd /root

echo -e "Loading OpenCN configuration... "
/root/halcmd -f two-drives.hal
echo "OK"

echo "Starting OpenCN server"
/root/opencn-server &

cd -

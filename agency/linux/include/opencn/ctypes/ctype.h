
#ifndef OPENCN_CTYPE_H
#define OPENCN_CTYPE_H

int __isalnum(int c);
int __isdigit(int c);
int __isalpha(int c);
int __iscntrl(int c);
int __isgraph(int c);
int __islower(int c);
int __isprint(int c);
int __ispunct(int c);
int __isspace(int c);
int __isupper(int c);
int __isxdigit(int c);

int isblank(int c);

#endif /* OPENCN_CTYPE_H */


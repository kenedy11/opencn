/********************************************************************
 *  Copyright (C) 2021 Jean-Pierre Miceli Miceli <jean-pierre.miceli@heig-vd.ch>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 ********************************************************************/

#include <linux/slab.h>

#include "lcec_priv.h"
#include "lcec_ax5106.h"

#define LCEC_IDN(type, set, block) (type | ((set & 0x07) << 12) | (block & 0x0fff))

#define IDN_TYPE_S(set, block) LCEC_IDN(0x0000, set, block)
#define IDN_TYPE_P(set, block) LCEC_IDN(0x8000, set, block)

/* Master 0, Slave 0, "AX5106-0000-0012"
 * Vendor ID:       0x00000002
 * Product code:    0x13f26012
 * Revision number: 0x000c0000
 */

static ec_pdo_entry_info_t ax5106_pdo_entries[] = {
	{0x0086, 0x00, 16}, /* Master control word */
	{0x002F, 0x00, 32}, /* Position command value */

	{0x0087, 0x00, 16}, /* Drive status word */
	{0x0033, 0x00, 32}, /* Position feedback 1 value */
};

static ec_pdo_info_t ax5106_pdos[] = {
	{0x0018, 2, ax5106_pdo_entries + 0}, /* MDT */
	{0x0010, 2, ax5106_pdo_entries + 2}, /* AT */
};

static ec_sync_info_t ax5106_syncs[] = {
	{0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
	{1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
	{2, EC_DIR_OUTPUT, 1, ax5106_pdos + 0, EC_WD_DISABLE},
	{3, EC_DIR_INPUT, 1, ax5106_pdos + 1, EC_WD_DISABLE},
	{0xff}
};

typedef struct {
	/* pins */
	hal_bit_t   *enable_pin;
	hal_bit_t   *enabled_pin;
	hal_bit_t   *fault_pin;
	hal_float_t *target_position_pin;
	hal_u32_t   *status_pin;
	hal_float_t *cur_position_pin;

	/* parameters */
	hal_float_t scale_param;
	hal_float_t velocity_scale_param;
	hal_u32_t   position_resolution_param;

	/* PDOs */
	unsigned int ctrl_word;
	unsigned int position_command;
	unsigned int status_word;
	unsigned int pos_feedback;
} lcec_ax5106_axis_t;

typedef struct {
	lcec_ax5106_axis_t axis[LCEC_AX5106_AXIS];
} lcec_ax5106_data_t;


static const lcec_pindesc_t slave_pins[] = {
	{ HAL_BIT,   HAL_IN,  offsetof(lcec_ax5106_axis_t, enable_pin),  "%s.%s.%s.enable-%d" },
	{ HAL_BIT,   HAL_OUT, offsetof(lcec_ax5106_axis_t, enabled_pin), "%s.%s.%s.enabled-%d" },
	{ HAL_BIT,   HAL_OUT, offsetof(lcec_ax5106_axis_t, fault_pin),   "%s.%s.%s.fault-%d" },
	{ HAL_FLOAT, HAL_IN,  offsetof(lcec_ax5106_axis_t, target_position_pin), "%s.%s.%s.target-position-%d" },
	{ HAL_U32,   HAL_OUT, offsetof(lcec_ax5106_axis_t, status_pin), "%s.%s.%s.status-%d" },
	{ HAL_FLOAT, HAL_OUT, offsetof(lcec_ax5106_axis_t, cur_position_pin), "%s.%s.%s.current-position-%d" },

	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};


static const lcec_pindesc_t slave_params[] = {
	{ HAL_FLOAT, HAL_RW, offsetof(lcec_ax5106_axis_t, scale_param), "%s.%s.%s.scale-%d" },
	// { HAL_FLOAT, HAL_RO, offsetof(lcec_ax5106_axis_t, velocity_scale_param), "%s.%s.%s.velocity-scale-%d" },
	{ HAL_U32, HAL_RO, offsetof(lcec_ax5106_axis_t, position_resolution_param), "%s.%s.%s.pos-resolution-%d" },
	{ HAL_TYPE_UNSPECIFIED, HAL_DIR_UNSPECIFIED, -1, NULL }
};

void lcec_ax5106_read(struct lcec_slave *slave, long period)
{
	lcec_master_t *master = slave->master;
	uint8_t *pd = master->process_data;
	lcec_ax5106_data_t *hal_data = (lcec_ax5106_data_t *)slave->hal_data;
	lcec_ax5106_axis_t *axis;
	uint16_t status;
	int pos_feedback;
	int i;

	/* wait for slave to be operational */
#if 0
	if (!slave->state.operational) {
		for (i = 0; i < LCEC_AX5106_AXIS; i++) {
			axis = &hal_data->axis[i];

			*(axis->fault_pin) = 1;
			*(axis->enabled_pin) = 0;
		}
		return;
	}
#endif

	for (i = 0; i < LCEC_AX5106_AXIS; i++) {
		axis = &hal_data->axis[i];

		status = EC_READ_U16(&pd[axis->status_word]);

		*(axis->status_pin) = status;

		/* TODO - check the position value type in the doc */
		pos_feedback = EC_READ_S32(&pd[axis->pos_feedback]);
		*axis->cur_position_pin = (double)pos_feedback / (axis->position_resolution_param * axis->scale_param);

		*axis->fault_pin = (status >> 13) & 1;

		*axis->enabled_pin = (((status >> 14) & 3)  && ((status >> 3) & 1));
    }
}

void lcec_ax5106_write(struct lcec_slave *slave, long period)
{
	lcec_master_t *master = slave->master;
	uint8_t *pd = master->process_data; /* process data */
	lcec_ax5106_data_t *hal_data = (lcec_ax5106_data_t *)slave->hal_data;
	lcec_ax5106_axis_t *axis;
	int i;
	int position_command;
	uint16_t ctrl;

	for (i = 0; i < LCEC_AX5106_AXIS; i++) {
		axis = &hal_data->axis[i];

		/* Enable - Disable drive */
        ctrl = EC_READ_U16(&pd[axis->ctrl_word]);
		if (*axis->enable_pin) {
			ctrl |= (1 << 15); /* Drive ON */
			ctrl |= (1 << 14); /* Enable */
			ctrl |= (1 << 13); /* Halt/Restart */
		} else {
			ctrl &= ~(1 << 15); /* Drive ON */
			ctrl &= ~(1 << 14); /* Enable */
			ctrl &= ~(1 << 13); /* Halt/Restart */
		}
		ctrl ^= 1 << 10; /* Sync */
        EC_WRITE_U16(&pd[axis->ctrl_word], ctrl);

        /* Set target position */
        position_command = *(axis->target_position_pin) * axis->position_resolution_param * axis->scale_param;

		EC_WRITE_S32(&pd[axis->position_command], position_command);
	}
}

int lcec_ax5106_init(int comp_id, struct lcec_slave *slave, ec_pdo_entry_reg_t *pdo_entry_regs)
{
	lcec_master_t *master = slave->master;
	lcec_ax5106_data_t *hal_data;
	lcec_ax5106_axis_t *axis;
	uint8_t idn_buf[4];
	int i;
	int err;

	/* initialize callbacks */
	slave->proc_read  = lcec_ax5106_read;
	slave->proc_write = lcec_ax5106_write;

	/* alloc hal memory */
	if ((hal_data = hal_malloc(__core_hal_user, sizeof(lcec_ax5106_data_t))) == NULL) {
		rtapi_print_msg(RTAPI_MSG_ERR, LCEC_MSG_PFX "hal_malloc() for slave %s.%s failed\n", master->name, slave->name);
		return -EIO;
	}
	memset(hal_data, 0, sizeof(lcec_ax5106_data_t));
	slave->hal_data = hal_data;

	/* initializer sync info */
	slave->sync_info = ax5106_syncs;

	for (i=0; i<LCEC_AX5106_AXIS; i++) {
		axis = &hal_data->axis[i];

		/* read idns */
    	if (lcec_read_idn(slave, i, LCEC_IDN(LCEC_IDN_TYPE_S, 0, 79), idn_buf, 4)) {
			return -EIO;
    	}
    	axis->position_resolution_param = EC_READ_U32(idn_buf);

    	axis->scale_param = 1.0;

		/* initialize PDO entries     position      vend.id     prod.code   index   sindx  offset                   bit pos */
		LCEC_PDO_INIT(pdo_entry_regs, slave->index, slave->vid, slave->pid, 0x0086, 0x00,  &axis->ctrl_word,        NULL);
		LCEC_PDO_INIT(pdo_entry_regs, slave->index, slave->vid, slave->pid, 0x002F, 0x00,  &axis->position_command, NULL);
		LCEC_PDO_INIT(pdo_entry_regs, slave->index, slave->vid, slave->pid, 0x0087, 0x00,  &axis->status_word,      NULL);
		LCEC_PDO_INIT(pdo_entry_regs, slave->index, slave->vid, slave->pid, 0x0033, 0x00,  &axis->pos_feedback,     NULL);

		/* export pins */
		if ((err = lcec_pin_newf_list(axis, slave_pins, lcec_module_name, master->name, slave->name, i)) != 0)
			return err;

#if 0
		/* export params */
		if ((err = lcec_param_newf_list(axis, slave_params, lcec_module_name, master->name, slave->name, i)) != 0)
			return err;
#endif
	}

	return 0;
}

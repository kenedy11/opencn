#!/bin/bash

# Define OpenCN items paths. It is defined relatively the path of this script.
#   - 'build.sh' path is <OPENCN_HOME>/agency/deploy.sh
SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
AGENCY=$SCRIPTPATH
ROOTFS=$SCRIPTPATH/rootfs
USR=$SCRIPTPATH/usr
TARGET=$SCRIPTPATH/target
FILESYSTEM=$SCRIPTPATH/filesystem
UBOOT=$SCRIPTPATH/../u-boot
BSP=$SCRIPTPATH/../bsp

usage()
{
  echo "Usage: $0 [OPTIONS] [DEVICE]"
  echo ""
  echo "Where OPTIONS are:"
  echo "  -a    Deploy all (no SD card formating)"
  echo "  -b    Deploy boot (kernel, U-boot, etc.)"
  echo "  -f    format the SD card / USB key"
  echo "  -r    Deploy rootfs (secondary)"
  echo "  -u    Deploy usr apps"
  echo "At least one OPTION should be set"
  echo ""
  echo "DEVICE is the device name of the SD Card or USB key (ex: sdb or mmcblk0 or other...)"
  exit 1
}

if [ ! -f $AGENCY/build.conf ]; then
	echo "Missing build.conf file, please read build.conf.default"
	exit 1
fi

while getopts "abfru" o; do
  case "$o" in
    a)
      deploy_rootfs=y
      deploy_boot=y
      deploy_usr=y
      ;;
    b)
      deploy_boot=y
      ;;
    f)
      format_sd=y
      ;;
    r)
      deploy_rootfs=y
      ;;
    u)
      deploy_usr=y
      ;;
    *)
      usage
      ;;
  esac
done

# Parameter checks - At least one OPTION should be present & DEVICE is optional
if [ $OPTIND -eq 1 ]; then usage; fi

shift $(($OPTIND - 1))
device=$1

# auto-export all variables found in build.conf (POSIX compatible method)
set -o allexport
. $AGENCY/build.conf
set +o allexport

# On x86, if we deploy the rootfs, we need to re-deploy the boot partition
# This will also install grub, but it is quite fast.
if [ "$PLATFORM" == "x86" -a "$deploy_rootfs" == "y" ]; then
        deploy_boot=y
fi

if [ "$TYPE" != "" ]; then
  PLATFORM_TYPE=${PLATFORM}_${TYPE}
else
  PLATFORM_TYPE=${PLATFORM}
fi

export PLATFORM_TYPE

# We now have ${PLATFORM} which names the platform base
# and ${PLATFORM_TYPE} to be used when the type is required.
# Note that ${PLATFORM_TYPE} can be equal to ${PLATFORM} if no type is specified.

if [ "$device" != "" ]; then
    devname=$device
    export devname="$devname"
elif [ "$PLATFORM" == "rpi4" -o "$PLATFORM" == "x86" ]; then
    echo "Specify the device name of MMC (ex: sdb or mmcblk0 or other...)"
    read devname
    export devname="$devname"
fi

if [ "$format_sd" ==  "y" ]; then
      echo "Formating SD Card on '$devname' (platform: $PLATFORM)..."
      cd $FILESYSTEM
      ./create_img.sh
      cd -
fi

if [ "$deploy_rootfs" == "y" ]; then
    # Deploy of the rootfs (second partition)
    cd $ROOTFS
    ./deploy.sh
    cd -
fi

if [ "$deploy_boot" == "y" ]; then
    # Deploy files into the boot partition (first partition)
    echo Deploying boot files into the first partition...

    if [ "$PLATFORM" != "x86" -a "$PLATFORM" != "x86-qemu" ]; then
        cd $TARGET
        ./mkuboot.sh ${PLATFORM}/${PLATFORM_TYPE}
        cd -

        cd $FILESYSTEM
        ./mount.sh 1
        sudo rm -rf fs/*
        sudo cp $TARGET/${PLATFORM}/${PLATFORM_TYPE}.itb fs/${PLATFORM}.itb
        sudo cp $UBOOT/uEnv.d/uEnv_${PLATFORM}.txt fs/uEnv.txt
        cd -
    fi

    if [ "$PLATFORM" == "x86" ]; then
      echo "Installing grub on "$devname""
      cd $FILESYSTEM
      ./mount.sh 1
      sudo rm -rf fs/boot
      sudo mkdir fs/boot

      sudo grub-install --target=i386-pc --boot-directory=fs/boot /dev/"$devname" --directory=$BSP/x86/boot/grub/i386-pc/ || abort "Failed to install grub in "$devname""
      sudo cp $BSP/x86/boot/grub/grub.cfg fs/boot/grub || abort "Failed to copy bsp/x86/boot/grub/grub.cfg"

      # Copy the kernel image
      sudo cp $AGENCY/linux/arch/x86/boot/bzImage fs/boot

      ./umount.sh
      cd -
    fi

    if [ "$PLATFORM" == "vexpress" ]; then
        # Nothing else ...
        cd $FILESYSTEM
        ./umount.sh
        cd -
    fi

    if [ "$PLATFORM" == "rpi4" ]; then
        cd $FILESYSTEM

        sudo cp -r $BSP/rpi4/* fs/
        sudo cp $UBOOT/u-boot.bin fs/kernel7.img
        ./umount.sh
        cd -
    fi
fi

if [ "$deploy_usr" == "y" ]; then
    # Deploy the usr apps related to the agency
    cd $USR
    ./deploy.sh
    cd -
fi

if [ "$PLATFORM" == "x86" ]; then
	cd $FILESYSTEM
	./mount.sh 1
	echo "We are currently in $PWD"
	sudo rsync -aP --stats --exclude='fs/fs' fs/ fs/fs
	./umount.sh
	cd -
fi

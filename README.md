Welcome to OpenCN Framework
***************************

Retrieve the source code:

```shell
	$ git clone https://gitlab.com/mecatronyx/opencnc/opencn.git opencn
	$ git submodule init
	$ git submodule update
```

Project related links:

* [OpenCN generated documentation](https://mecatronyx.gitlab.io/opencnc/opencn/)
* [OpenCN discussion forum](https://discourse.heig-vd.ch/c/opencn-framework/)
* [OpenCN Youtube channel](https://www.youtube.com/channel/UC8FQCu_fKYfK7QRDN0j_dBw)
* [OpenCN wiki](https://gitlab.com/mecatronyx/opencnc/opencn/-/wikis/home)

A Virtualbox VM can be downloaded [here](http://reds-data.heig-vd.ch/OpenCN/OpenCN_VM12_02.ova)
* usr: opencn
* pwd: opencn


###
HAL
###

************
Introduction
************

HAL concept comes from *linuxCNC* project. This documentation is also derivated from
`linuxCNC documentation <http://linuxcnc.org/docs/html/hal/intro.html>`_.

HAL stands for *Hardware Abstraction Layer*. At the highest level, it is simply a
way to allow a number of *building blocks* (called components) to be loaded and
interconnected to assemble a complex system.

HAL is based on the same principles that are used to design hardware circuits and
systems, so it is useful to examine those principles first.

Any system (including a CNC machine), consists of interconnected components. For
the CNC machine, those components might be the drives, motors, encoders, etc...
The machine builder must select, mount and wire these pieces together to make a
complete system.

HAL Concepts
============

This section defines key HAL terms

**Component**

    A HAL component is a piece of software with well-defined inputs, outputs, and
    behavior, that can be loaded and interconnected as needed.

**Pin**

	Pins are the inputs and the outputs of the *components*.

**Parameter**

	Parameters are used to configure *components* or . They can be seen as trim pots

**Signal**

	Signals are the equivalent of a wire. They are used to connect pins together

**Type**

	A pin can be of one of the following types:

        bit - a single TRUE/FALSE or ON/OFF value

        float - a 64 bit floating point value, with approximately 53 bits of resolution
        and over 1000 bits of dynamic range.

        u32 - a 32 bit unsigned integer, legal values are 0 to 4,294,967,295

        s32 - a 32 bit signed integer, legal values are -2,147,483,647 to +2,147,483,647

**Function**

	Components export or more functions. These functions perform components specific
	actions. Functions should be added to a thread to be executed.

**Thread**

    A thread is a list of functions that runs at specific intervals. When a thread
    is first created, it has a specific time interval (period), but no functions.
    Functions can be added to the thread, and will be executed in order every time
    the thread runs.

Usage
=====

The `halcmd` tools is available on openCN target to perform action to the HAL.

here are some example on how to use it:

* Module loading:
	 ``./halcmd load <MODULE_NAME> [PARAMS]``
* Thread creation:
	``./halcmd load threads name1=<THREAD_NAME> period1=<period>``
* Add function to a thread:
	``./halcmd addf <FUNC_NAME> <THREAD_NAME>``
* Start / stop hal execution:
	``./halcmd start``

	``./halcmd stop``
* Set PIN value:
	``./halcmd setp <PIN NAME> VALUE``
* Read PIN value:
	``./halcmd show pin``


It is possible to create a script with HAL commands and to call with the ``halcmd``
script. By convention, a HAL script use ``hal`` as extension.

* call a HAL script:
	``./halcmd -f <HAL_SCRIPT>``

* HAL script example :

.. code-block:: shell

	load loopback cfg=fsub
	load threads name1=loopback_thread period1=20000
	addf loopback.0 loopback_thread
	setp loopback.0.f_in.0 12.3
	show pin loopback

**********
Components
**********

OpenCN supports the following components:

* :ref:`loopback`
* :ref:`streamer`
* :ref:`sampler`
* :ref:`threads`
* :ref:`LinuxCNC EtherCAT (lcec)`
* :ref:`Trivial kinematics (trivkins)`
* :ref:`feedopt`
* :ref:`LinuxCNC Control Toolkit (lcct)`
* :ref:`OpenCN companion`

Architecture
============

A component is composted in three parts:

* **user space**: The main task of the user-space part is to parse the component
  parameters.
* **kernel**: It is in the kernel that the components PINs and  HAL functions
  (function exported by the component) are defined.
* **kernel real-time**: The HAL functions are RT functions.

The :numref:`_fig-component_arch` shows the different parts of a component.


.. figure:: pictures/opencn_component_architecture.png
   :name: _fig-component_arch
   :alt: Components architecture
   :align: center

   Components architecture


.. _loopback:

Loopback
========

The loopback HAL component has been designed for debug purpose. It contains
pairs of HAL pins. A pair of pins is composed of one input pin and one output
pins of the same type. The purpose of this component is to copy any data
written on an input pin to its paired output pin. The function that copies
the data from input to output pins is exported to be run in a real time thread.
The number of pins is provided dynamically as a string argument of the program.
Each char of the string describes the type of a pair of pins:

* 'f' -> float
* 's' -> 32-bit signed integer
* 'u' -> 32-bit unsigned integer
* 'b' -> bit

For instance, to load a loopback component with 2 pairs of float pins and
one pair of 32-bit signed pins, the configuration can be specified as follow:
**cfg=ffs**

.. note::

	The maximum number of pairs of pins allowed for this component is 25.

.. note::

	It is possible to generated up to 8 loopback components, however this feature
	has not been tested though.

Example
-------

This section provides an example of use of the loopback component using
the HAL command to load the component, add its real time function to a
thread and modify input pins.

Load a loopback component with one pair of float pins, one pair of 32-bit
signed pins, one pair of 32-bit unsigned pins and one pair of bit pins:

:code:`./halcmd load loopback cfg=fsub`

Load a thread component:

:code:`./halcmd load threads name1=loopback_thread period1=20000`

Add the real time function of the loopback component to the thread:

:code:`./halcmd addf loopback.0 loopback_thread`

Show pins of the loopback component:

:code:`./halcmd show pin loopback`

This command should show this information::

    [   68.064071] Component Pins:
    [   68.064397] Owner    Type  Dir         Value  Name
    [   68.064913]     14     bit   IN          FALSE  loopback.0.b_in.3
    [   68.065753]     14     bit   OUT         FALSE  loopback.0.b_out.3
    [   68.065753]
    [   68.066683]     14     float IN              0  loopback.0.f_in.0
    [   68.067594]     14     float OUT             0  loopback.0.f_out.0
    [   68.067594]
    [   68.068423]     14     s32   IN              0  loopback.0.s32_in.1
    [   68.069284]     14     s32   OUT             0  loopback.0.s32_out.1
    [   68.069284]
    [   68.070128]     14     s32   OUT            94  loopback.0.time
    [   68.070128]
    [   68.070938]     14     u32   IN     0x00000000  loopback.0.u32_in.2
    [   68.071753]     14     u32   OUT    0x00000000  loopback.0.u32_out.2
    [   68.071753]
    [   68.072620]     16     s32   OUT            94  loopback_thread.time

Modify the float input pin:

:code:`./halcmd setp loopback.0.f_in.0 12.3`

Show pins of the loopback component

:code:`./halcmd show pin loopback`

The value written to loopback.0.f_in.0 has been copied to loopback.0.f_out.0::

    [  137.240362]     14     float IN           12.3  loopback.0.f_in.0
    [  137.241265]     14     float OUT          12.3  loopback.0.f_out.0

.. _streamer:

streamer
========

Under Construction

.. _sampler:

sampler
=======

Under Construction

.. _threads:

threads
=======

The *threads* component is currently able to manage up to three independent
periodic threads (or tasks). Depending on the arguments passed to the application,
the threads are initialized during the component loading.

Each thread can have a different period.

Functions are then attached (exported) to these threads.

.. _LinuxCNC EtherCAT (lcec):

LinuxCNC EtherCAT (lcec)
========================

The *lcec* component provides an interface between the EtherCAT master and OpenCN
components. It is a port of the project `linuxcnc-ethercat <https://github.com/sittner/linuxcnc-ethercat>`_
made by Sascha Ittner

Overview
--------

lcec enables EtherCAT Distributed Clock feature (DC) in mode B (a slave is master
of the clock).

It does not export any function. Instead, it creates HAL threads, one per master.
These threads:

* Implement the EtherCAT DC in mode B
* Provide HAL thread features: adding/removing functions, start/stop thread execution
* The hread period depends of :mod:appTimePeriod master parameter sets in the
  *EtherCAT* configuration file (see :ref:`etherCAt configuration file`)
* Are named :mod:lcec_thread.N where N is the master index in the EtherCAT bus
  topology.

.. note::

 The current version of lcec only supports 1 master in the EtherCAT topology.

.. _etherCAt configuration file:

*EtherCAT* Configuration file
-----------------------------

The *EtherCAT* configuration file is a XML file that describes the *EtherCAT* bus
topology. The supported tags are:

* **masters**: top level tag
* **master**: master description
* **slave**: slave description
* **dcConf**: Distributed clock description
* **initCmds**: initial commands sent to initialize a slave (CoE or IDN commands)

The following paragraphs provides details about the parameters supported by each
tags.

Master tag parameters
^^^^^^^^^^^^^^^^^^^^^

* **idx**: Index of the master in EtherCAT topology
* **appTimePeriod**: Period of the EtherCAT cycle, in ns
* **name**: (optional) Name of the master. If not set, the master name is set to
  the ‘idx’ value


Salve tag parameters
^^^^^^^^^^^^^^^^^^^^

* **type**: Type of the slave
* **idx**: Index of the slave in EtherCAT topology
* **name**: (optional) name of the slave. If not set, the slave name is set to the
  ‘idx’ value

dcConfig tag parameters
^^^^^^^^^^^^^^^^^^^^^^^
* **assignActivate**: AssignActivate  word. It is vendor-specific and can be taken
  from the XML device description file.
* **sync0Cycle**: (optional) SYNC0 cycle time, in ns
* **sync0Shift**: (optional) SYNC0 shift time, in ns
* **sync1Cycle**: (optional) SYNC1 cycle time, in ns
* **sync1Shift**: (optional) SYNC1 shift time, in ns

initCmds tag parameters
^^^^^^^^^^^^^^^^^^^^^^^

* **filename**: name of the file with the CoE or SoE commands

EtherCAT configuration file example
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: xml

	<masters>
		<master idx="0" appTimePeriod="100000">
			<slave idx="0" type="EK1100"/>
			<slave idx="1" type="EL1252"/>
			<slave idx="2" type="EL2252">
				<dcConf assignActivate="300" sync0Cycle="*1" sync0Shift="50000"/>
			</slave>
		</master>
	</masters>

Debug mode
----------

The lcec debug mode provides a standalone mode: In this mode, lece does not interface
with the *EtherCAT* master and so there are no EtherCAT activities at all. It only
creates HAL pins and parameters and call the init(),read() and write() functions
of the slaves.

It means that it is possible to test/debug the system without *EtherCAT* interface
(no slaves needed). The behavior of the pins/parameters exported are slave dependent.

Usage
-----

the lcec component takes 2 parameters when loaded:

* **Cfg**: XML file EtherCAT providing the topology of the bus. The content of this
  file is described in :ref:`etherCAt configuration file` paragraph.
* **Debug** (-d): enable the debug mode.

Example:

.. code-block:: shell

	$ ./halcmd load lcec cfg=<ETHECAT_TOPOLOGY> [-d]

Supported slaves
----------------

*  :ref:`Beckhoff AX5106`

.. _Beckhoff AX5106:

Beckhoff AX5106
^^^^^^^^^^^^^^^

* Assign Activate: 0x730

PINs list:

================= ========= =========
Name              Type      direction
================= ========= =========
enable-X          HAL_BIT   HAL_IN
enabled-X         HAL_BIT   HAL_OUT
fault-X           HAL_BIT   HAL_OUT
target-position-X HAL_FLOAT HAL_IN
status-X          HAL_U32   HAL_IN
current-position  HAL_FLOAT HAL_IN
================= ========= =========

The XML files below are an example to start ax5106 drive in ``position control``
mode with an EtherCAT frequency of 1 KHz.

* EtherCAT topology: ax5106_ec_test.xml
* IDN configuration: ax5106_startup.xml

New slave integration
---------------------

This paragraph gives an overview how a new slave type can be added. The slave
behavior is implemented in the kernel.

1. Add the new type in ``lcec_slave_type_t`` enum defined in ``<OPENCN_HOME>/include/opencn/uapi/lcec_conf.h``
   file Example:  ``lcecSlaveTypeEL12522``.
2. Add	the	new slave type in the ``slaveTypes[]`` array in ``usr/components/lcec/lcec_xml_parser.c``
   file. The  first  parameter  is  the  salve  name  (a  string,  this  string  is  then  used  in  the  EtherCAT configuration XML file) and the second parameter is the value added in previous
   step Example: ``{ "EL1252", lcecSlaveTypeEL1252 }``
3. In the kernel
	a. Create the slave source files:
		* ``my_slave.h``: VID, PID PDO number and the prototype of the init function
		* ``my_slave.c``: implementation of the feature behavior: initialization read
		  and write functions. The PDOs and Sync  managers should also be defined here.
	b. In ``lcec_conf.c`` file, ``types[]`` array: add the new salve definitions
	   ``{lcecSlaveTypeEL1252,     LCEC_EL1252_VID,     LCEC_EL1252_PID,     LCEC_EL1252_PDOS,
	   lcec_el1252_init}``

.. _Trivial kinematics (trivkins):

Trivial kinematics (trivkins)
=============================

The *trivkins* component exports the following three  functions for a trivial
kinematics: :func:kinematicsType, :func:kinematicsForward and :func:kinematicsInverse()

Usage
-----

The trivkins has two parameters:

* Type: type of kinematic used. Supported values:
	* **‘B’** or **‘b’**: Both, the forward and inverse kinematics are defined.
	* **‘F’** or **‘f’**: Forward, only the forward kinematics exists.
	* **‘I’** or **‘I’**: Inverse only the inverse kinematics exists.
	* **‘1’**:  Identity. joints and world coordinates are the same, as for s
	  lideway  machines (XYZ milling machines).
* Coordinates: the coordinates used by the machine (standard coordinate name,
  [xyzabcuvw]). For example, for a XYZ milling machine, the coordinates parameter
  is XYZ

Invocation:

.. code-block:: shell

	./halcmd load trivkins type=<TYPE> coordinates=<COORDINATES>

.. _feedopt:

feedopt
=======

Under Construction

.. _LinuxCNC Control Toolkit (lcct):

LinuxCNC Control Toolkit (lcct)
===============================

Under Construction


.. _OpenCN companion:

OpenCN companion
================

OpenCN companion is *simple* *EtherCAT* slave. The doc of this slave can be found
`here <companion.html>`_.

In OpenCN, the support of the OpenCN Companion EtherCAT slave is part of the lcec
component. The following code shows an example of an EtherCAT topology file with
one Companion slave.

.. code-block:: xml

	<masters>
		<master idx="0" appTimePeriod="1000000">
			<slave idx="0" type="COMPANION" name="COMP">
				<dcConf assignActivate="300" sync0Cycle="*1" sync0Shift="500000"/>
			</slave>
		</master>
	</masters>


The companion export the following PINs:

================= ========= =========
Name              Type      direction
================= ========= =========
in-0                bit     OUT
in-1                bit     OUT
in-2                bit     OUT
in-3                bit     OUT
out-0               bit     IN
out-1               bit     IN
out-2               bit     IN
out-3               bit     IN
pwm-duty-cycle-0    u32     IN
pwm-duty-cycle-1    u32     IN
================= ========= =========

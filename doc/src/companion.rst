
.. _OpenCN companion slave:

################
OpenCN Companion
################

OpenCN companion is a **simple**,  **cheap** and **versatile** *EtherCAT* slave.
It is based on a *Raspberry PI 4* and `easyCAT <https://www.bausano.net/en/hardware/ethercat-e-arduino/ethercat-and-raspberry.html>`_
HAT

*OpenCN Companion* exposes an interface to:
- 4 inputs & 4 outputs
- 2 PWM
- 1 I2C bus (not exposed to PDO interface)

*******************
Companion GPIO pins
*******************

The companion offers 4 GPIOs as input and 4 GPIOs as output

===== ==== ========
name  GPIO Pin RPi4
===== ==== ========
in0    16    36
in1    20    38
in2    21    40
in3    26    37
out0   24    18
out1   27    13
out2   22    15
out3   23    16
===== ==== ========

*********
PDOs list
*********

*Companion* provides 32 inputs and 32 output PDOs bytes. The following tables shows
how they are mapped:

====== ===========================
Inputs       Usage
====== ===========================
0      Input GPIOs
1      Reserved (GPIOs extension)
====== ===========================

======= ===========================
Outputs      Usage
======= ===========================
0       output GPIOs
1       Reserved (GPIOs extension)
2       duty_cycle_offs 0
3       duty_cycle_offs 1
======= ===========================

***
PWM
***

The PWM implementation in the companion uses the *Linux kernel* PWM `framework <https://www.kernel.org/doc/html/latest/driver-api/pwm.html>`_.
It acts as a wrapper around the low level PWM driver of the RPi4 to allow a high-level configuration. It allows configuring the frequency, duty-cycle and the range of our PWM easily.

For now, only the duty-cycle is accessible from the EC interface using PINs but the frequency and range should be configured by some PARAMETERS in the future.


***
I2C
***

In the current Companion state, there is an example using I2C with the *SI7021* sensor.
For now, it is only an usage example with no PDOs exported. However it opens the discussion for future work.

===========
Future work
===========
This explore different angle to the usage and export of the I2C.

--------------------------
Exporting the I2C controls
--------------------------
One way would be to export all the controls/protocol elements of I2C:

* Address
* Read/Write bit
* Length
* etc...

This approach offers the most freedom to the user. However, it leaves the responsability to the user to
know the I2C implementation and to reimplement the access correctly from its application.

It also means we have to take in account all the I2C timing requirements on top of the EC ones.

-----------------------------
Exporting preprocessed values
-----------------------------
This approach encapsulate all I2C implementation in the companion itself. It would only export processed values
(sensors, registers, configs, ...). It restrict the user possibilities but ease the application
development by hiding the low-level I2C implementation.

From the companion side, it would only means a small processing after the I2C transactions, which
needs to be done in both approaches. However, it means that for each device, the PDOs need to be adapted.


******
RT IRQ
******
To time the EC transfers/PDOs processing, the IRQ coming from the EC master is used. The idea is to have
a RT thread waiting on a completion, which is then completed in the RT IRQ handler. We can the process the EC buffers and update
the companion inputs/outputs.

=============================
Issues with the current state
=============================
Multiple issues arised when implementing this approach:

* Xenomai/Linux was not patched to allow GPIOs based RT IRQ.
* SPI and I2C BCM (used by RPi4) drivers are not patched to support RT.


The first one means that the current *Xenomai/Cobalt* does not support multi-level *irqchip* in an IRQ path. It only allows
RT IRQ to be processed directly by the GIC, which rise an issue when working with GPIOs having a *pinctrl* acting as a first *irqchip*.

Bellow is a quick overview of the path used when requesting the IRQ 17 as an RT IRQ.

--------------------
`__ipipe_init_early`
--------------------
Found in: *agency/linux/kernel/ipipe/core.c*

Called at the initialization phase early in the boot process. Initializes all RT irqdescs to have
the GIC as an irqchip.

------------
`gpiod_get`
------------
Found in *agency/linux/drivers/gpio/gpiolib.c*

Request our GPIO from the device tree and get its descriptor. Called in the requester.

--------------
`gpiod_to_irq`
--------------
Found in *agency/linux/drivers/gpio/gpiolib.c*

Retrieve the **Linux** irq_desc associated to our gpio. In our example, GPIO17 has the hwirq 17 and
the irq 61 (only useful for Linux). Its GIC irqnr is the 145 (SGI 113 + 32 (PPI)).

------------------
`rtdm_irq_request`
------------------
Found in : *agency/linux/kernel/xenomai/rtdm/drvlib.c*

This function:

* Bind our irq handler to the irqdescs[hwirq].
* Calls `irqchip->irq_enable()` which will enable the irq at the GIC level.
* Sets the IRQ affinity to the RT CPU, also done in the GIC.

It does not do any translation from the irq number retrieved from the DT.
For now, it only lookup in its previously allocated ipipe irqdescs, do a one level initialization
(GIC side) and sets its RT handle

============
IRQ 17 patch
============
This section shows the patch developed to at least have the EC interrupt (17) triggering an IRQ and calling its handler. It will
explain what was setup and what needs to be done to have a fully GPIOs based RT irq framework. It is important to note that in *OPENCN* only one of the 4
CPUs (CPU1) will execute the RT domain. It will only focus on the RPi4 case. Most of the patch can be found at
the commit `6168e389 <https://gitlab.com/mecatronyx/opencnc/opencn/-/commit/6168e3899ecddc3c7910d73fec3ebea12e669fc2>`_.


-----------------------
Get the correct irqchip
-----------------------

As said before, the GPIOs irq are not managed by the GIC directly, but by the BCM pin controller (agency/linux/drivers/pinctrl/bcm/pinctrl-bcm2835.c) instead.
It is then chained to the GIC to completely setup/clean the IRQ.

*xnintr_attach* (agency/linux/kernel/xenomai/intr.c): When requesting the IRQ 17, we have to first initialize the GIC part (affinity + enable) using the GIC irqno
145 as our hwirq in the ipipe irqdescs array. After this, set it back to 17.

-----------------------
Correct the dispatching
-----------------------
When processing an IRQ, the GIC will call *__ipipe_grab_irq* (agency/linux/arch/arm/kernel/ipipe.c) passing the GIC irqno (145).
We then need to translate it back to our hwirq 17 and then continue with the dispatching.

After this, we also have to add the IRQ ACK to the pinctrl.

-------------------
Always EOI from GIC
-------------------
We also have to call the EOI callback from the GIC in *gic_handle_irq* (agency/linux/drivers/irqchip/irq-gic.c) to fully terminate the IRQ processing and be able to receive the next one.


==================================
SPI/UART drivers not supporting RT
==================================
The other main issue, is that the SPI and I2C BCM drivers are not RT ready. They uses locks that are owned by CPU0 which causes a conflict when
used from CPU1.
`Here <https://source.denx.de/Xenomai/xenomai/-/blob/master/kernel/drivers/spi/spi-bcm2835.c>`_ is the official Xenomai patch to be able to use the SPI in the RT context.

The I2C however has not been `officially ported <https://xenomai.org/pipermail/xenomai/2020-May/042931.html>`_ to support the RTDM. `Here <https://github.com/ahmedius2/Simple-PL011-RTDM-driver>`_ is an adaptation of the bcm2835 library which seems
to implement it.

==========
Next steps
==========
The next step would be to make this adaptation available for every GPIOs without having to hand-pick the right one. A patch for the 4.19 kernel is proposed `here <http://www.simplerobot.net/2019/12/xenomai-3-for-raspberry-pi-4.html>`_.
It patches Xenomai/ipipe and the `bcm2835-pinctrl` driver. It would need some adaptations to include it in our heavily modified Xenomai.

For the SPI, an official patch is already existing. The I2C one will need some more work as no official patch is available.

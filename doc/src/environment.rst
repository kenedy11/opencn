###########
Environment
###########

OpenCN environment can be used in two *modes*:

*  **Native** mode: The compilation of the different components (kernel, rootfs,
   ...) and deployment are done directly on the host machine.
*  **Docker** mode: The framework provides a docker environment which provides all
   the tools needed by this environment.

.. warning::

	OpenCN framework is expected to be used in a Linux host. It is used/tested
	with Ubuntu and Debian distributions


Below are the commands to retrieve the source code:


.. code-block:: shell

	$ git clone https://gitlab.com/mecatronyx/opencnc/opencn.git opencn
	$ git submodule init
	$ git submodule update

*************
*Native* mode
*************

Tools installation
==================

This section all the

* Add i386 architecture

.. code-block:: shell

	$ dpkg --add-architecture i386

* Required packages

.. code-block:: shell

	$ sudo apt update & sudo apt upgrade
	$ sudo apt install bc bison bridge-utils ca-certificates  capnproto cpio \
	  cmake device-tree-compiler elfutils fakeroot file flex gcc git g++ lib32z1-dev \
	  libc6:i386 libcapnp-dev libcapstone3 libepoxy0 libgbm1 libgtk2.0-dev libjpeg62 \
	  libssl-dev libstdc++6:i386 libusb-1.0-0 make nano openssh-server patch pkg-config \
	  python3 python3-crypto python3-pyelftools rsync sudo wget unzip u-boot-tools \
	  xz-utils zlib1g:i386

* Add the ARM toolchain

.. code-block:: shell

	$ sudo mkdir -p /opt/toolchains && cd /opt/toolchains
  	# Download and extract arm-none-linux-gnueabihf toolchain (gcc v9.2.1).
	$ wget -q https://developer.arm.com/-/media/Files/downloads/gnu-a/9.2-2019.12/binrel/gcc-arm-9.2-2019.12-x86_64-arm-none-linux-gnueabihf.tar.xz
	$ sudo tar -xf gcc-arm-9.2-2019.12-x86_64-arm-none-linux-gnueabihf.tar.xz
	$ sudo rm gcc-arm-9.2-2019.12-x86_64-arm-none-linux-gnueabihf.tar.xz
	$ sudo mv gcc-arm-9.2-2019.12-x86_64-arm-none-linux-gnueabihf arm-linux-gnueabihf_9.2.1
	$ sudo echo 'export PATH="${PATH}:/opt/toolchains/arm-linux-gnueabihf_9.2.1/bin"' > /etc/profile.d/02-toolchains.sh

Compilation
===========

The framework provides a generic build scripts, ``<OPENCN_HOME>/agency/build.sh``.

It provides options to build each OpenCN components independently; ``-k`` for the
kernel, ``-r`` for the rootfs and ``-u`` for the user application; or all in one
call (``-a``). The target selection is done the ``-t`` option. It only has to be
call once. The ``-b`` option build the dependences, like U-Boot for ARM targets
or qemu for the emulation ones.

.. code-block:: shell

	Usage: ./build.sh [OPTIONS]

	Where OPTIONS are:
	  -a    build all components
	  -b    build u-boot and/or qemu depending on the target
	  -c    clean the selected components
	  -k    build linux kernel
	  -r    build rootfs (secondary)
	  -u    build user apps
	  -t    select the wanted target (rpi4, vexpress, x86, x86-qemu)
	        It has to be done at least once (creation of build.conf file)

.. note::

	This script only focus on the build of target components (Components running
	in the selected target). It does not propose an option to build the host
	application, like the ``user_gui``.

Examples
--------

* Select rpi4 target: ``./build.sh -t rpi4``
* Clean the kernel build: ``./build.sh -kc``

Deployment
==========

As for the compilation, the framework provides a generic build scripts, ``<OPENCN_HOME>/agency/deploy.sh``.

Before to perform the deployment, the SD card or USB key has to be formated. It
can be done with the ``-f`` option.

As for the ``build.sh`` script, ``deploy.sh`` provides options to deploy each OpenCN
components independently; ``-b`` for the boot elements (U-Boot, kernel, ...), ``-r``
for the rootfs and ``-u`` for the user application; or all in one call (``-a``).

.. code-block:: shell

	Usage: ./deploy.sh [OPTIONS] [DEVICE]

	Where OPTIONS are:
	  -a    Deploy all (no SD card formating)
	  -b    Deploy boot (kernel, U-boot, etc.)
	  -f    format the SD card / USB key
	  -r    Deploy rootfs (secondary)
	  -u    Deploy usr apps
	At least one OPTION should be set

	DEVICE is the device name of the SD Card or USB key (ex: sdb or mmcblk0 or other...)

.. note::

	The target is the one selected by the -t option of the ``build.sh`` script.

Emulation
=========

To start the qemu emulation for the `vexpress` or `x86-quemu` targets, is simply
done by running ``<OPENCN_HOME>/agency/st`` script.

.. code-block:: shell

	$ cd <OPENCN_HOME>/agency
	$ ./st

*************
*Docker* mode
*************

OpenCN framework provides a docker environment which provides all the needed tools.
The main advantages of a container solution are:

* Simplification of the deployment of the framework: No need to install a bunch
  of packets on the development PC. Only docker is needed
* Avoids compatibility issues with tool version, after an update for example
* End-User can use any Linux distribution

Before using the docker environment, the OpenCN image has to be built. It has do
be done only once.

.. code-block:: shell

	$ cd <OPENCN_HOME>
	$ docker build -t opencn/build-env docker/

Usage
=====

In the ``<OPENCN_HOME>/docker`` folder, there is scripts to build (``<OPENCN_HOME>/docker/build.sh``),
deploy (``<OPENCN_HOME>/docker/deploy.sh``) or launch (``<OPENCN_HOME>/docker/st.sh``)
the emulation. There usage are exactly the same the one in *Native* mode.

GCode Interpreter (RS274)
-------------------------

Overview
~~~~~~~~

Here we will look at the integration of the gcode intepreter into OpenCN. This one is more complicated
because the interpreter is a class imported from LinuxCNC. 

The steps for using the interpreter are:

- Create an instance of the ``Interp`` class (then use its member functions)
- Open a gcode source file
- Call read (reads a line from the file)
- Call exec (executes any applicable thing that was read)

The call to ``exec`` will end up in callbacks that we have to implement ourselves. They are defined in
``agency/usr/rs274/cannon.hh`` and must be available on link-time when building the interpreter. They have
been implemented in ``agency/usr/rs274ngc/ocn.cc``. Theses functions must somehow be able to interact with the matlab
scripts that called the interpreter functions in the first place. 

One of these callbacks is ``ARC_FEED``, a function that describes a helix movement while milling. 

.. code-block:: cpp

    extern void ARC_FEED(int lineno, double first_end, double second_end, double first_axis, double second_axis,
                     int rotation, double axis_end_point, double a, double b, double c, double u, double v, double w, double pitch);

These arguments have to be converted into a ``CurvStruct`` and returned to the initial caller of ``exec``. Since the coder does not support
``coder.ceval`` with classes, a wrapper file has been created for this purpose: ``agency/usr/matlab/common/src/cpp_interp.cpp``. Historicaly,
this wrapper uses a static instance of ``Interp`` since only one is ever used, but it could be reworked to handle an opaque pointer to it as 
in the class-wrapping :ref:`example<Coder External Lib Example>`.

.. code-block:: cpp

    // File: agency/usr/matlab/common/src/cpp_interp.cpp
    #include "cpp_interp.hpp"
    #include "rs274ngc_interp.hh"
    #include "interp_return.hh"
    #include <unistd.h>

    namespace {
        // This is the static instance of Interp
        Interp interp;

        // Flag for tracking whether the instance has been initialized
        bool was_init{false};

        // Temporary CurvStruct
        ocn::CurvStruct curv;

        // Character buffer for string manipulation
        char buffer[1024];
    }

    // Makes the flag available to users of this wrapper
    int cpp_interp_loaded() {
        return was_init;
    }

    // Function to initialize the static instance and open a gcode source file
    int cpp_interp_init(const char* filename) {
        // Get current path for debugging purposes, there were some issues with the path
        // previously
        char* path = getcwd(buffer, sizeof(buffer));
        if (!path) {
            fprintf(stderr, "cpp_interp_init: Failed to get current path\n");
            return 0;
        }

        fprintf(stderr, "cpp_interp_init, pwd: '%s', trying to open '%s'\n", buffer, filename);
        if (interp.init() != INTERP_OK) {
            fprintf(stderr, "cpp_interp_init: Failed to open\n");
            return 0;
        }
        was_init = (interp.open(filename) == INTERP_OK);

        // If everything went OK, set the flag to 1 and return it
        
        return was_init;
    }

    // This function is available to ARC_FEED in ocn.cc in order to copy
    // a CurvStruct into the temporary variable
    void push_curv_struct(const ocn::CurvStruct* curv_struct) {
        ocn::CopyCurvStruct(curv_struct, &curv);
    }

    // This function uses the static instance to read one line and then execute it
    int cpp_interp_read(ocn::CurvStruct* curv_struct) {
        // Set the temporary variable as invalid
        curv.Type = ocn::CurveType_None;
        int status = 0;
        status = interp.read();

        // Check if the end of file was reached or the exit gcode command was found
        if (status == INTERP_ENDFILE || status == INTERP_EXIT) {
            return 0;
        }
        // or if an error occured
        else if (status > INTERP_MIN_ERROR) {
            fprintf(stderr, "ERROR(read): %s\n", interp.getSavedError());
            return 0;
        }

        // During the next call, ARC_FEED can end up being called by the interpreter,
        // and thus push_curv_struct as well
        status = interp.execute();
        // Here the temporary CurvStruct "curv" may contain a new value
        
        // Check error again
        if (status > INTERP_MIN_ERROR) {
            fprintf(stderr, "ERROR(execute): %s\n", interp.getSavedError());
            return 0;
        }

        // Copy the temporary variable into the output parameter (returning it to the caller)
        ocn::CopyCurvStruct(&curv, curv_struct);
        
        return 1;
    }

The matlab function using this is ``ReadGCode``. Here is a part of it. Notice how it uses a ``cmd`` argument instead of using
several different functions. This is because of the static ``Interp`` instance: the mexed library has to be the same file for
both functions or the static instance will not be the same one.

.. code-block:: matlab

    % File: ReadGCode.m
    % This function returns 1 while there is still something to do next time and if the CurvStruct
    % can be valid
    function [status, CurvStruct] = ReadGCode(cmd, filename)
        if coder.target('mex')
            % Satisfy the coder
            CurvStruct = ConstrLineStruct([1,2,3]', [4,5,6]', 0.2, ZSpdMode.NN);

            % The interpreter needs this define
            coder.updateBuildInfo('addDefines', '_POSIX_C_SOURCE=199309L')

            % Enables detailed printing from the interpreter
            % coder.updateBuildInfo('addDefines', 'DEBUG_RS274')

            % Indicate that we are currently mexing to the cpp code
            coder.updateBuildInfo('addDefines', 'MEX_READGCODE')

            % Colored build output
            coder.updateBuildInfo('addCompileFlags', '-fdiagnostics-color=always')

            % Add all the interpreter files to the mex build as well as our callback implementation
            % and wrapper
            coder.updateBuildInfo('addSourceFiles','cpp_interp.cpp', '$(START_DIR)/../common/src');
            coder.updateBuildInfo('addSourceFiles','interp_arc.cc', '$(START_DIR)/../../rs274ngc');
            coder.updateBuildInfo('addSourceFiles','interp_array.cc', '$(START_DIR)/../../rs274ngc');
            coder.updateBuildInfo('addSourceFiles','interp_base.cc', '$(START_DIR)/../../rs274ngc');
            coder.updateBuildInfo('addSourceFiles','interp_check.cc', '$(START_DIR)/../../rs274ngc');
            coder.updateBuildInfo('addSourceFiles','interp_convert.cc', '$(START_DIR)/../../rs274ngc');
            coder.updateBuildInfo('addSourceFiles','interp_cycles.cc', '$(START_DIR)/../../rs274ngc');
            coder.updateBuildInfo('addSourceFiles','interp_execute.cc', '$(START_DIR)/../../rs274ngc');
            coder.updateBuildInfo('addSourceFiles','interp_find.cc', '$(START_DIR)/../../rs274ngc');
            coder.updateBuildInfo('addSourceFiles','interp_internal.cc', '$(START_DIR)/../../rs274ngc');
            coder.updateBuildInfo('addSourceFiles','interp_inverse.cc', '$(START_DIR)/../../rs274ngc');
            coder.updateBuildInfo('addSourceFiles','interp_namedparams.cc', '$(START_DIR)/../../rs274ngc');
            coder.updateBuildInfo('addSourceFiles','interp_o_word.cc', '$(START_DIR)/../../rs274ngc');
            coder.updateBuildInfo('addSourceFiles','interp_queue.cc', '$(START_DIR)/../../rs274ngc');
            coder.updateBuildInfo('addSourceFiles','interp_read.cc', '$(START_DIR)/../../rs274ngc');
            coder.updateBuildInfo('addSourceFiles','interp_remap.cc', '$(START_DIR)/../../rs274ngc');
            coder.updateBuildInfo('addSourceFiles','interp_setup.cc', '$(START_DIR)/../../rs274ngc');
            coder.updateBuildInfo('addSourceFiles','interp_write.cc', '$(START_DIR)/../../rs274ngc');
            coder.updateBuildInfo('addSourceFiles','ocn.cc', '$(START_DIR)/../../rs274ngc');
            coder.updateBuildInfo('addSourceFiles','rs274ngc_pre.cc', '$(START_DIR)/../../rs274ngc');
            coder.updateBuildInfo('addSourceFiles','inifile.cc', '$(START_DIR)/../../rs274ngc');
            coder.updateBuildInfo('addLinkFlags', '-ldl');
            coder.updateBuildInfo('addIncludePaths', '$(START_DIR)/gen_mex/readgcode');
            coder.cinclude('cpp_interp.hpp');
            
            status = int32(0);
            switch cmd
                case ReadGCodeCmd.Load
                    % [filename 0]: We can only pass 0-terminated strings with coder.ceval
                    status = coder.ceval('cpp_interp_init', [filename 0]);
                case ReadGCodeCmd.Read
                    is_loaded = int32(0);
                    is_loaded = coder.ceval('cpp_interp_loaded');
                    if is_loaded
                        status = coder.ceval('cpp_interp_read', coder.ref(CurvStruct));
                    else
                        status = int32(0);
                    end
            end
        else
            % ...
        end

.. uml::

    skinparam defaultFontName hack
    |Matlab|
    |#AntiqueWhite|cpp_interp.cpp|
    |#LightRed|Interp|
    |#LightGreen|ocn.cc|

    |Matlab|
    start
    :ReadGCode(init);
    |cpp_interp.cpp|
    :cpp_interp_init;
    |Interp|
    :init;
    :open;
    |Matlab|
    :ReadGCode(read);
    |cpp_interp.cpp|
    :cpp_interp_loaded;
    :cpp_interp_read;
    |Interp|
    :read;
    :execute;
    |ocn.cc|
    :ARC_FEED;
    |cpp_interp.cpp|
    :push_curv_struct;
    |Matlab|
    stop
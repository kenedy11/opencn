GSL Bspline
-----------

Overview
~~~~~~~~

At the time of writing, Matlab does not support generating code for b-splines.
An external library was thus used for this purpose: the GNU Scientific Library (GSL).
It has a C API and requires the creation and maintenance of a "workspace".

The Matlab functions responsible for managing this are the ``bspline_foo.m`` files.
The wrapper file is ``src/c_spline.c``. It provides helper functions to limit the number of
wrapper functions to implement as well as the possibility of an error.

We will go over the implementation of ``bspline_create``, ``bspline_eval`` and ``bspline_base_eval``.

Function: bspline_create
~~~~~~~~~~~~~~~~~~~~~~~~

This function creates a workspace with a certain spline degree and breakpoints. In other words, 
it prepares a bspline basis ready for evaluation.

.. code-block:: matlab

    % File: bspline_create.m
    function Bl = bspline_create(degree, breakpoints) %#codegen
        nbreak = length(breakpoints);
        ncoeff = nbreak + degree - 2;
        
        h = uint64(0);
        
        % The same code is used when generating for native or mex
        if coder.target('rtw') || coder.target('mex')
            % Add the wrapper to the build
            coder.updateBuildInfo('addSourceFiles','c_spline.c', '$(START_DIR)/src');

            % Use a special class listed below to get the system's pkg-config
            % information for compiling and linking.
            coder.updateBuildInfo('addLinkFlags', LibInfo.gsl.lflags);
            coder.cinclude('src/c_spline.h');

            % "breakpoints" here is a vector, it will be automaticaly passed as a "const double*"
            coder.ceval('c_bspline_create_with_breakpoints', coder.wref(h), degree, breakpoints, int32(nbreak));

            % This needs to be reworked a bit, but Bl is the resulting structure containing all necessary information
            % about the b-spline workspace
            Bl.ncoeff = ncoeff;
            Bl.breakpoints = breakpoints;
            Bl.handle = h;
            Bl.degree = int32(degree);

            % Make sure the coder interprets "breakpoints" as a row vector of any size
            % [1, Inf]: Maximal size of first dimension: 1, second dimension: Infinite
            % [0, 1]: First dimension fixed, second dimension flexible
            coder.varsize('Bl.breakpoints', [1, Inf], [0, 1]);
        else
            Bl = bspline_create_mex(int32(degree), breakpoints);
        end
    end


    % File: LibInfo.m
    % This class wraps the call the find_libcflags/find_liblflags with a constant property
    % to convince the coder it is a constant value during generation
    classdef LibInfo
        properties( Constant = true )
            gsl = struct('cflags', find_libcflags('gsl'), 'lflags', find_liblflags('gsl'));
            clp = struct('cflags', find_libcflags('clp'), 'lflags', find_liblflags('clp'));
        end
    end

    % File: find_libcflags.m
    % Function used to fetch cflags for a system library that uses pkg-config
    function cflags = find_libcflags(name)

    [status, cflags] = system(sprintf("pkg-config --cflags %s", name));
    assert(status == 0);

    % Remove extra whitespace
    cflags = strip(cflags);

    end

.. code-block:: cpp

    // File: Excerpt from c_spline.h
    typedef struct bspline {
        // The workspace
        gsl_bspline_workspace *ws;

        // Some temporary matrices for computing things
        gsl_matrix *dB;
        gsl_matrix *dBNonZero;
    } bspline_t;

    // File: Excerpt from c_spline.c
    void c_bspline_create_with_breakpoints(void *handle_, int32_t degree, double* breakpoints, int N)
    {
        // This function has a pointer to a pointer as the first argument to return a bspline_t*
        // The rest of the functions keep this same convention
        size_t* handle = handle_;
        size_t ncoeffs;
        // Disable the gsl error handler, we don't want to crash matlab.
        // (The default handler calls terminate() on error)
        gsl_set_error_handler_off();
        
        // Allocate a new bspline_t
        bspline_t *bs = (bspline_t *)malloc(sizeof(bspline_t));
       
        // Copy the breakpoints into a gsl_vector
        // (Another function that interprets a double* as a gsl_vector could be used)
        gsl_vector* brkpnts = gsl_vector_alloc(N);
        for(int i = 0; i < N; i++) {
            gsl_vector_set(brkpnts, i, breakpoints[i]);
        }
       
        // Create workspace and setup knots
        bs->ws = gsl_bspline_alloc(degree, N);
        gsl_bspline_knots(brkpnts, bs->ws);
        
        ncoeffs = gsl_bspline_ncoeffs(bs->ws);
       
        // Prepare the temporary matrices
        // the number of coeffs will not change so we can allocate dB once and forget about it
        bs->dB = gsl_matrix_alloc(ncoeffs, nderiv + 1);
        bs->dBNonZero = gsl_matrix_alloc(degree, nderiv + 1);
        
        *handle = (size_t)bs;
    }

For mexing, the same configuration introduced in the queue wrapper is used.

.. code-block:: matlab
    
    % File: Excerpt from generate_mex.m
    codegen('-config', cfg,'-d', 'gen_mex/bspline_create', ...
    'bspline_create', '-args', ...
    ... % Once again the vector size specification for "breakpoints"
    {fcfg.SplineDegree, coder.typeof(0.0, [1, Inf], [0, 1])},...
    '-o', 'bspline_create_mex');

    % The type of the returned struct is used later for mexing other functions
    BlType = coder.OutputType('bspline_create');



Function: bspline_eval
~~~~~~~~~~~~~~~~~~~~~~

This function evaluates a function using the workspace basis weighted by ``coeffs`` at point ``x``.

.. code-block:: matlab

    % File: bspline_eval.m
    function [x, xd, xdd, xddd] = bspline_eval(Bl, coeffs, x) %#codegen
    % C wrapper function signature for reference:
    % void c_bspline_eval(uint64_t *handle, const double *c, double x, double X[4]);
        % Prepare the output vector containing the result and its 3 derivatives
        X = zeros(1, 4);
        if coder.target('matlab')
            assert(numel(coeffs) == Bl.ncoeff);
        end

        if coder.target('rtw') || coder.target('mex')
            % Catch mistakes or double precision edge cases
            if x < 0
                fprintf('ERROR: C_BSPLINE_EVAL: X < 0 (%f)\n', x);
                x = 0;
            elseif x > 1
                fprintf('ERROR: C_BSPLINE_EVAL: X > 1 (%f)\n', x);
                x = 1;
            end

            coder.updateBuildInfo('addSourceFiles','c_spline.c', '$(START_DIR)/src');
            coder.updateBuildInfo('addLinkFlags', LibInfo.gsl.lflags);
            coder.cinclude('src/c_spline.h');

            % Pass 'X' as a wref, a writable ref -> double*
            coder.ceval('c_bspline_eval', coder.rref(Bl.handle), coder.rref(coeffs),...
                x, coder.wref(X));
            x = X(1);
            xd = X(2);
            xdd = X(3);
            xddd = X(4);
        else
            [x, xd, xdd, xddd] = bspline_eval_mex('bspline_eval', Bl, coeffs, x);
        end
    end

.. code-block:: cpp

    // File: Excerpt from c_bspline.c
    // We use a void* here so compilers don't complain when we pass in a uint64*
    void c_bspline_eval(const void *handle_, const double *c, double x, double X[4])
    {
        const size_t* handle = (const size_t*)handle_;
        size_t k, i, istart, iend;
        bspline_t *bs = (bspline_t *)*handle;
       
        // Check for invalid values again
        if (x < 0.0) {
            fprintf(stderr, "c_bspline_eval: x < 0\n");
            x = 0.0;
        }
        if (x > 1.0) {
            fprintf(stderr, "c_bspline_eval: x > 1\n");
            x = 1.0;
        }
        // Check for NaN
        if (x != x) {
            fprintf(stderr, "c_bspline_eval: x = %f\n", x);
        }
      
        gsl_bspline_deriv_eval_nonzero(x, nderiv, bs->dBNonZero, &istart, &iend, bs->ws);
        /* for each deriv degree */
        for (k = 0; k < nderiv + 1; k++) {
            X[k] = 0.0;
            /* for each coeff */
            for (i = istart; i <= iend; i++) {
                X[k] += gsl_matrix_get(bs->dBNonZero, i - istart, k) * c[i];
            }
        }
    }


Function: bspline_base_eval
~~~~~~~~~~~~~~~~~~~~~~~~~~~

This function is used for evaluating the individual basis splines at a list of points, 
writing everything into a matrix. This matrix is used by the Matlab scripts for setting up the
optimization problem.

The evaluation happens for the three derivatives and one integral. Only the Matlab code listing will be
shown here, the C code is not that interesting.

.. code-block:: matlab
    
    % File: bspline_base_eval.m 
    function [BasisVal, BasisValD, BasisValDD, BasisValDDD, BasisIntegr] = bspline_base_eval(Bl, xvec) %#codegen
        if coder.target('rtw') || coder.target('mex')
            % n, bspline_n
            samples = int32(numel(xvec));

            % Prepare output matrices
            BasisVal = zeros(samples, Bl.ncoeff);
            BasisValD = BasisVal;
            BasisValDD = BasisVal; 
            BasisValDDD = BasisVal;
            BasisIntegr = BasisVal(1, :)';
            
            coder.updateBuildInfo('addSourceFiles','c_spline.c', '$(START_DIR)/src');
            coder.updateBuildInfo('addLinkFlags', LibInfo.gsl.lflags);
            coder.cinclude('src/c_spline.h');
            coder.ceval('c_bspline_base_eval', coder.rref(Bl.handle), samples, coder.rref(xvec), ...,
                coder.ref(BasisVal), coder.ref(BasisValD), coder.ref(BasisValDD),coder.ref(BasisValDDD),...
                coder.ref(BasisIntegr));
        else
            [BasisVal, BasisValD, BasisValDD, BasisValDDD, BasisIntegr] = bspline_base_eval_mex('bspline_base_eval', Bl, xvec);
        end
    end


Every coder directive used for this wrapper should be familier from the queue wrapper.

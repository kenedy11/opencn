Coder documentation
*******************

OpenCN code structure overview
==============================

Intro
-----

OpenCN is a complete linux-based operating system with the rootfs built
using **buildroot** and a heavily customized kernel based on the
**Xenomai** extension for hard realtime. This realtime enables OpenCN to
use a ``10kHz`` EtherCAT master, which controls all the CNC axes.

OpenCN borrows the concept of **components** from **LinuxCNC**, enabling
the communication between them using **pins**. Connecting two or more
pins creates a **signal**.

The position values are generated with a trajectory planner implemented
in Matlab and then converted into ``C++`` code using the Matlab
**coder**. This trajectory planner, called **Feedopt** (Feedrate
Optimizer), accepts G-code (RS274) as input, using the interpreter from
LinuxCNC, called **RS274**.

.. uml:: 
    
    skinparam defaultFontName hack
    package Kernel {
        object lcct
        object "feedopt" as feedoptK
        object lcec
    }

    package Userspace {
        object "feedopt" as feedoptU
        object server
        object RS274
    }
     
    package Remote {
        object GUI
        object client 
    }
    
    lcct -- feedoptK: pins
    lcct -- lcec: pins
    feedoptU -- RS274
    GUI -- client
    server -- client: capnp
    server -- feedoptU: pins
    server -- lcct: pins
    feedoptK -- feedoptU: pins + sharedbuf

Several steps are required to go from g-code to position values and CNC
motion: 

- Create ``CurvStruct``\ s from g-code, which are matlab
  structures that describe geometric primitives (Line, Helix), and push
  them to a queue 
- Analyze the queue and make geometric optimization on
  the path 
- Optimize the path into a minimal-time, jerk-limited
  trajectory using b-splines for the velocity profile 
- Resample the
  resulting trajectory segments at 10Khz and send the resulting position
  values to **lcct** through the kernel-side feedopt 
- When certain
  conditions are met, lcct sends feedopt positions to **lcec**, the EtherCAT master


The goal when using Matlab is to have the same codebase for easy
algorithm testing and then just exporting it to native code. External
libraries, source code, or even Matlab functions themselves can be
compiled into shared libraries that Matlab can call as normal functions.
This process is called **Mex**-ing. Some issues arise from these steps:

- Matlab has no b-spline implementation and thus we use the GSL (GNU
  Scientific Library) one. This means that we need to write Matlab
  functions that can selectively use a Mex library that calls GSL
  functions or direct calls to GSL when using native code. 
- Matlab has
  poor dynamic resing of arrays. The performance is unacceptable and thus
  custom queues are used 
- RS274 from LinuxCNC is used, which is a big C++
  codebase, and it needs to be compiled into a Mex library as well 
- All of the code needs to eventualy be callable from pure C++

Matlab coder introduction
=========================

Overview
--------

The matlab coder is a tool for generating C/C++ code from matlab
functions as well as for making Mex libraries from them. It can be
configured in several modes, depending on the generation target. The
modes in use in OpenCN are: 

- Embedded Library: Generates code that 100%
  compatible with standard C/C++ without any matlab runtime dependencies,
  therefore suitable for an embedded environment. Here it is a library
  because it will only be called from other code, not a standlone
  executable. 
- Mex: Generates code for creating libraries that can be
  used by Matlab as normal functions, either for speeding up execution of
  existing scripts or bringing in external libraries. It also allows
  directly compiling C++ files without any generation when using the Mex
  headers and API.

Limitations
-----------

Unfortunately, there are a lot of limitations when using this tool that
have to accounted for. Here are some of them: 

- Return arguments always
  have to be assigned on all call paths 
- The coder does not infer that
  assigning 0 to an int32 variable is in fact an integer, have to use
  ``variable=int32(0)`` 
- Only free C++ functions can be called, classes
  are impossible to manipulate directly 
- No handle classes or classes
  *containing* handle classes can be used as arguments to entry-point
  functions
- When creating new structures, the fields cannot re-use the ones of the 
  structure that is being created
 
  .. code-block:: matlab
     
    function S = CreateStruct()
        S.a = 42;
        S.b = S.a + 10; % This line is not accepted by the coder
    end
  
  I would say this is bad practice with complicated structures anyway, it is
  much easier to navigate source code when classes clearly define a list of 
  properties, their dimensions, types and default values.
  

Let’s do two examples of using this tool before going to how OpenCN
libraries work.

.. _Coder External Lib Example:

Calling external libraries from Matlab
--------------------------------------

Imagine we have a library that we want to use in Matlab but we either
don’t have access to its source code, or it is too complex anyway and
not at all worth it re-implementing it. This library has a class
``ExampleClass`` with a method ``getValue`` returning an int.

Using code generation
~~~~~~~~~~~~~~~~~~~~~

When using code generation, an intermediate C++ file is required for
wrapping the library since we cannot handle classes directly. We can
then call C++ functions using
``coder.ceval('functionName', arg1, arg2)``.

.. uml::

    skinparam defaultFontName hack
    left to right direction
    object "Matlab Script" as matlabScript
    object Wrapper
    object ExampleClass
    matlabScript-->Wrapper: coder.ceval
    Wrapper-->ExampleClass

We need a way to store a reference to the class instance in Matlab.
Since a pointer type does not exist, a ``uint64`` will be used instead.
An ``CreateExampleClass`` function will be responsible of creating this
instance through the wrapper and then returing the ``uint64``-cast
pointer.

**Matlab-side functions**:

.. code-block:: matlab

   % File: CreateExampleClass.m
   function handle = CreateExampleClass()
   if coder.target('mex')
       % If we are generating code for Mex

       % This line, even if the value is unusued, declares
       % the type of 'handle' because Matlab has no way of knowing it
       % purely from coder.ceval
       handle = uint64(0);

       % insert a call to initLibrary into the generated code
       handle = coder.ceval('createExampleClass');

       % Add the wrapper file to the compilation
       coder.updateBuildInfo('addSourceFiles','wrapper.cpp');

       % -------
       % Add link flags to the precompiled library
       coder.updateBuildInfo('addLinkFlags', '-lthelibrary');
       % OR
       % Add library source
       coder.updateBuildInfo('addSourceFiles', 'library_source.cpp');
       % -------

       % Include the wrapper header file
       % This will be inserted at the beginning of the generated code
       coder.cinclude('wrapper.h');
   else
       % If not, just call the mexed library
       handle = CreateExampleClass_mex()
   end
   end

   % File: ExampleClassGetValue.m
   function value = ExampleClassGetValue(handle)
   if coder.target('mex')
       % Insert call to getValue into generated code
       value = int32(0);

       % Insert a call to getValue with 'handle' as argument
       value = coder.ceval('getValue', handle);

       coder.updateBuildInfo('addSourceFiles','wrapper.cpp');
       coder.updateBuildInfo('addSourceFiles', 'library_source.cpp');
       coder.cinclude('wrapper.h');
   else
       value = ExampleClassGetValue_mex(handle);
   end
   end

**Wrapper functions**:

.. code-block:: cpp

   // File: wrapper.h
   #include <cstdint>
   uint64_t createExampleClass();
   int getValue(uint64_t handle);

   // File: wrapper.cpp
   #include "library_header.h"
   uint64_t createExampleClass() {
       return reinterpret_cast<uint64_t>(new ExampleClass);
   }

   int getValue(uint64_t handle) {
       return reinterpret_cast<ExampleClass*>(handle)->getValue();
   }

Now we have to configure code generation.

.. code-block:: matlab

   % File: generate_mex.m
   cfg = coder.config('mex');
   % We want the library to be built as well
   cfg.GenCodeOnly = false;

   % Generate C++
   cfg.TargetLang = 'C++';

   % Call code generation with this configuration for CreateExampleClass
   codegen('-config', cfg, ...
       ...% Set the entry-point function
       'CreateExampleClass',...
       % Set the resulting file name
       '-o', 'CreateExampleClass_mex'
   )

   % Call code generation with this configuration for ExampleClassGetValue
   codegen('-config', cfg, ...
       ...% Set the entry-point function with an argument example
       'ExampleClassGetValue', '-args', uint64(0),...
       ...% Set the resulting file name
       '-o', 'CreateExampleClass_mex'
   )

All that is left to do is to run ``generate_mex.m``. The library can
then be used this way from Matlab:

.. code-block:: matlab

   % File: TestExampleClass.m
   handle = CreateExampleClass;
   value = ExampleClassGetValue(handle);

To destroy the class instance, another function in the same spirit that
handles destruction should be created.

Using Mex API
~~~~~~~~~~~~~

When using the Mex API, no code generation takes place, we write the
wrapper as well as the Matlab function itself. Using the same
``ExampleClass`` from before, the C++ code would look like this:

.. code-block:: cpp

   // File: example_class_create.cpp
   #include "library_header.h"

   using matlab::mex::ArgumentList;

   // Matlab expects a class named 'MexFunction' that extends
   // matlab::mex::Function
   class MexFunction : public matlab::mex::Function
   {
   public:
       // It has to implement the call operator, ()
       void operator()(ArgumentList outputs, ArgumentList inputs)
       {
           // Object for creating Matlab-understood types
           matlab::data::ArrayFactory factory;

           // Create a new instance of the ExampleClass
           ExampleClass* instance = new ExampleClass;

           // And then create a generic 'scalar' with createScalar,
           // returning it to Matlab in the first output argument
           outputs[0] = factory.createScalar(
               reinterpret_cast<uint64_t>(instance)
           );
       }
   };

   // File: example_class_getvalue.cpp
   #include "library_header.h"

   using matlab::mex::ArgumentList;
   class MexFunction : public matlab::mex::Function
   {
   public:
       void operator()(ArgumentList outputs, ArgumentList inputs)
       {
           // Check if the number of inputs is correct
           if (inputs.size() != 1) {
               // Get a pointer to the Matlab runtime
               std::shared_ptr<matlab::engine::MATLABEngine> matlabPtr = getEngine();

               // And call the 'error' function, with an error id and 
               // a list of format arguments (printf-like)
               matlabPtr->feval(u"error", 0,
                   std::vector<matlab::data::Array>({
                       factory.createScalar("Wrong number of inputs: %d"),
                       factory.createScalar(inputs.size())}));

           }

           matlab::data::ArrayFactory factory;

           // Get the handle as in uint64_t from the generic inputs
           // (Probably implemented with a conversion operator)
           uint64_t handle = inputs[0][0];

           // Convert it to our class instance
           ExampleClass* instance = reinterpret_cast<ExampleClass*>(handle);

           // As before, return a value
           outputs[0] = factory.createScalar(
               instance->getValue()
           );
       }
   };

These functions can now be built using the ``mex`` command.

.. code-block:: matlab

   % File: build_mex.m
   mex example_class_create.cpp -output ExampleClassCreate_mex
   mex example_class_getvalue.cpp -output ExampleClassGetValue_mex

These functions can be called directly, or we can create a Matlab class
for convenience.

.. code-block:: matlab

   % File: ExampleClass.m
   classdef ExampleClass

   properties (SetAccess=private)
       % uint64 scalar modifyable only from within the class
       handle(1,1) uint64
   end

   methods
       function obj = ExampleClass()
           obj.handle = ExampleClassCreate_mex()
       end

       function value = get(obj) {
           value = ExampleClassGetValue_mex(obj.handle);
       }
   end
   end

   % File: TestExampleClass.m
   exampleClass = ExampleClass;
   value = exampleClass.get;

Calling external libraries in generated code
--------------------------------------------

The final goal of OpenCN is for code to be executed in a native,
embedded C++ environment, so these functions have to work seamlessly for
native code generation as well.

``ExampleClass`` requires little change from the mex codegen example.

.. code-block:: matlab

   % File: CreateExampleClass.m
   function handle = CreateExampleClass()
   if coder.target('mex')
       % ...
   elseif coder.target('rtw')
       % If we are generating code for embedded

       handle = uint64(0);

       % insert a call to createExampleClass into the generated code
       handle = coder.ceval('createExampleClass');

       % Include the wrapper header file
       % This will be inserted at the beginning of the generated code
       coder.cinclude('wrapper.h');
   else
       % If not, just call the mexed library
       handle = CreateExampleClass_mex()
   end
   end

   % File: ExampleClassGetValue.m
   function value = ExampleClassGetValue(handle)
   if coder.target('mex')
       % ...
   elseif coder.target('rtw')
       % If generating for embedded
       value = int32(0);

       % Insert a call to getValue with 'handle' as argument
       value = coder.ceval('getValue', handle);
       coder.cinclude('wrapper.h');
   else
       value = ExampleClassGetValue_mex(handle);
   end
   end

Notice that there are no source files or library links added with
updateBuildInfo; this is the job of the native build system.

Let’s configure code generation. More parameters than needed for this
example are introduced here.

.. code-block:: matlab

   % File: generate_c.m
   % This time we want an embedded library, without Matlab dependencies
   cfg = coder.config('lib', 'ecoder', true);

   % Some code formatting, we need to read it sometimes
   cfg.IndentSize = 4;
   cfg.ColumnLimit = 100;

   % Variables will have the same name in the generated code as in Matlab
   % This is for easier debugging
   cfg.PreserveVariableNames = 'All';

   % This time we _only_ want code generation, without compilation by Matlab
   cfg.GenCodeOnly = true;

   % Generate all code into a single source file, easier to integrate into
   % the native build system
   cfg.FilePartitionMethod = 'SingleFile';

   % Choose language and standard
   cfg.TargetLang = 'C++';
   cfg.TargetLangStandard = 'C++11 (ISO)';

   % Choose a target device architecture
   cfg.HardwareImplementation.TargetHWDeviceType = 'Intel->x86-64 (Linux 64)';
   cfg.HardwareImplementation.ProdHWDeviceType = 'Intel->x86-64 (Linux 64)';

   % Enable Intel SSE extensions
   cfg.CodeReplacementLibrary = 'Intel SSE (Linux)';

   % Put all generated code into the namespace 'ocn', this reduces the
   % potential for name conflicts and is overall cleaner
   cfg.CppNamespace = 'ocn';

   % We do not need to generate an example, nor a Makefile
   cfg.GenerateExampleMain = 'DoNotGenerate';
   cfg.GenerateMakefile = false;

   % Allow variables to change their size, this makes it much more flexible
   % when matrices need to change their size depending on some parameters
   cfg.EnableVariableSizing = true;

   codegen('-config', cfg,...
       ...% Set output directory
       '-d', 'generate_example', ...
       ...% Add first function to generation
       'ExampleClassCreate',...
       ...% Add second function with example value
       'ExampleClassGetValue', '-args', uint64(0)
   );

The first function in the codegen list will define the generated
filename. This means that after running ``generate_c.m``,
``generate_example`` will contain ``ExampleClassCreate.cpp`` and
``ExampleClassCreate.h``.

The coder will generate all the needed auxiliary functions, but only the
ones specified in the codegen list will be directly callable.

An example native code using this would look like this

.. code-block:: cpp

   // File: TestExampleClass.cpp
   #include "generate_example/ExampleClassCreate.h"
   int main() {
       uint64_t handle = ocn::ExampleClassCreate();
       int value = ocn::ExampleClassGetValue(handle);
       return 0;
   }

Now we just need to build it, using CMake for example.

.. code-block:: cmake

   # File: CMakeLists.txt
   project(example)
   add_executable(example 
       TestExampleClass.cpp
       library_source.cpp
       generate_example/ExampleClassCreate.cpp
   )

This may seem like a complicated way to just call ``new ExampleClass``
and then using it directly, but remember, the entry point functions may
be very complicated and call wrapped libraries themselves. They also
have to work in both the Mex-ed environement and the native one, while
conserving the same Matlab-level interface for easy testing and
integration.

Wrappers in OpenCN
==================

After this introduction to how code generation works, we can now look at
the different wrappers that exist in OpenCN, how they are implemented,
and introduce some more details that are needed to make them work.

.. include:: wrapper/queue.rst
.. include:: wrapper/bspline.rst
.. include:: wrapper/rs274.rst
.. include:: wrapper/clp.rst

